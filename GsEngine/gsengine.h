#pragma once


#include "gsengine_global.h"

#include "../GsRenderer/gsrenderer.h"
#include "../GsGraphics/gsmodel3d.h"
#include "../GsScene/gsscene.h"
#include "../GsInputProvider/gsinputprovider.h"


class GSENGINE_EXPORT GsEngine
{
	friend DWORD WINAPI showFramesPerSecond(
		void* frames);

    public:
		GsEngine(
			HINSTANCE instance, 
			HINSTANCE previousInstance, 
			std::string commandLine, 
			int commandShow, 
			GsDisplayParameters displayParameters, 
			std::wstring programName = L"Demo");
        ~GsEngine();

		int main();

		void loadScript(
			std::string fileName);

    private:
		GsResult initialize(
			GsDisplayParameters displayParameters, 
			std::string graphicsApiName = "Direct3D 9", 
			std::string soundApiName = "DirectSound",
			std::string networkApiName = "DirectPlay");

		static LRESULT WINAPI processMessages(
			HWND hWnd,
			unsigned int msg, 
			WPARAM wParam,
			LPARAM lParam);

		void release();

		void processCommandLineAndCommandShow(
			std::string line, 
			int commandShow);

		GsResult registerMainWindowClass();

		GsResult initializeWindow(
			GsDisplayParameters displayParameters);
		GsResult initializeGraphics(
			std::string graphicsApiName, 
			GsDisplayParameters displayParameters);
		GsResult initializeSound(
			std::string soundApiName);
		GsResult initializeInput();
		GsResult initializeNetwork(
			std::string networkApiName);

		GsResult initializeScene();
		GsResult initializeLight();

		void start();

		void loadShader(
			GsShaderType type,
			std::string fileName);

		void processScriptCommand(
			std::vector<std::string>& command);

		static bool stop;
		static UINT framesPerSecond;
		static std::string labelFrames;


		std::wstring programName;

		HINSTANCE mainInstance;
		HINSTANCE previousInstance;
		WNDCLASSEX	windowClass;
		
		HWND window;

		GsRenderer* renderer;
		static GsRenderDevice* renderDevice;

		GsInputProvider* inputProvider;
		GsInputDevice* inputDevice;



		GsScene* scene;

		static bool active;
		
		bool fail;

		static GsLog log;


		UINT pixelShaders[100];
		UINT vertexShaders[100];
		static UINT activePixelShader;
		static UINT activeVertexShader;
		static UINT pixelShadersNumber;
		static UINT vertexShadersNumber;
		
		static std::string shadersModeLabel;
		static bool shadersEnabled;

		static bool rotateModels;

		
		HANDLE framesCounterHandle;
};

DWORD WINAPI showFramesPerSecond(
	void* framesPerSecond);