#include "gsengine.h"

#include "qapplication.h"
#include "..\GsMathematics\gsmatrix.h"
#include <iosfwd>


bool GsEngine::active = false;
bool GsEngine::stop = false;

UINT GsEngine::activePixelShader = -1;
UINT GsEngine::activeVertexShader = -1;
UINT GsEngine::pixelShadersNumber = 0;
UINT GsEngine::vertexShadersNumber = 0;

bool GsEngine::shadersEnabled = false;
std::string GsEngine::shadersModeLabel = "shaders off";

bool GsEngine::rotateModels = false;

GsRenderDevice* GsEngine::renderDevice = nullptr;

UINT GsEngine::framesPerSecond = 0;
std::string GsEngine::labelFrames = "";

GsLog GsEngine::log = GsLog();


GsEngine::GsEngine(
    HINSTANCE newMainInstance,
    HINSTANCE newPreviousInstance,
    std::string commandLine,
    int commandShow,
    GsDisplayParameters displayParameters,
    std::wstring newProgramName)
{
    log << "-------------> Construction started. <-------------";
    mainInstance = newMainInstance;
    previousInstance = newPreviousInstance;

    processCommandLineAndCommandShow(commandLine, commandShow);

    active = false;
    stop = false;

    programName = newProgramName;

    if(fail = failed(initialize(displayParameters)))
        log << "-------------> Construction failed. <--------------";
    else
        log << "------------> Construction succeeded. <------------";
}

GsEngine::~GsEngine()
{
    if(active)
        release();
}


int GsEngine::main()
{
    log.addSeparator();
    log << "-------------> Main process started. <-------------";


    if(fail)
        log << "Something failed.";
    else
    {
        active = true;

        MSG message;

        while(!stop)
        {
            if(PeekMessage(&message, nullptr, 0, 0, PM_REMOVE))
            {
                TranslateMessage(&message);
                DispatchMessage(&message);
            }
            else
                start();

            Sleep(1);
        }
    }

    log << "-------------> Main process finished. <------------";

    release();

    return fail;
}


void GsEngine::start()
{
    static float angle = 0.0, time = 0.0;
    GsColor colorWhite = { 1.0, 1.0, 1.0, 1.0 };

    if(!fail)
        //       if(active)
    {
        if(failed(renderDevice->beginRendering(GS_CLEAR__PIXEL | GS_CLEAR__DEPTH | GS_CLEAR__STENCIL)))
        {
            log << "FAILED: GsRenderDevice::beginRendering() in GsEngine::start().";

            fail = true;
        }
        else
        {
            GsMousePositionDelta mousePositionDelta;

            if(inputDevice->mouseDown(GS_MOUSE_BUTTON__LEFT))
                if(inputDevice->mouseMoved(&mousePositionDelta))
                {
					if (failed(scene->rotateCamera(-mousePositionDelta.deltaY * 0.005, -mousePositionDelta.deltaX * 0.003)))
                        log << "Failed to rotate camera.";
                }

			int scrollDelta;
			if (inputDevice->mouseScrolled(&scrollDelta))
				if (failed(scene->scrollCamera(scrollDelta)))
					log << "Failed to scroll camera.";

            if(rotateModels)
            {
                angle += 0.001;

                if(angle > GS_2PI)
                    angle = 0;
            }

            if(shadersEnabled)
                time += 0.01;
            else
                time = 0.0;


            if(shadersEnabled)
            {
                if(failed(renderDevice->activatePixelShader(activePixelShader)))
                    stop = true;

                if(failed(renderDevice->activateVertexShader(activeVertexShader, GS_VERTEX__UNTRANSFORMED_UNLIT)))
                    stop = true;

                renderDevice->registerConstantFloatForShader(GS_SHADER__PIXEL, "time", time, 0);
            }
            else
                renderDevice->setUseShaders(false);


            if(failed(scene->renderAll()))
                log << "Failed to render scene.";

            if(failed(renderDevice->drawText(0, 10, 10, colorWhite, shadersModeLabel.data())))
                log << "Failed to draw mode label.";

            if(failed(renderDevice->drawText(0, 10, 50, colorWhite, labelFrames.data())))
                log << "Failed to draw frames per second label.";


            renderDevice->endRendering();
            framesPerSecond++;
        }
    }
    //        else
    //           Sleep(100);
    else
        stop = true;
}


GsResult GsEngine::initialize(
    GsDisplayParameters displayParameters,
    std::string graphicsApiName,
    std::string soundApiName,
    std::string networkApiName)
{
    GsResult result;

    log << "Initialization started.";


    if(failed(result = initializeWindow(displayParameters)))
        return result;

    if(failed(result = initializeGraphics(graphicsApiName, displayParameters)))
        return result;

    if(failed(result = initializeInput()))
        return result;

    if(failed(result = initializeSound(soundApiName)))
        return result;

    if(failed(result = initializeNetwork(networkApiName)))
        return result;

    if(failed(result = initializeScene()))
        return result;

    log << "Initialization succeeded.";
    return GS_OK;
}

void GsEngine::release()
{
    log.addSeparator();
    log << "----------------> Release started. <---------------";

    if(renderer)
        renderer->releaseDevice();

    if(inputProvider)
        inputProvider->releaseDevice();


    UnregisterClass((LPCWSTR)programName.c_str(), mainInstance);

    active = false;

    CloseHandle(framesCounterHandle);

    log << "---------------> Release succeeded. <--------------";
}


void GsEngine::processCommandLineAndCommandShow(
    std::string line,
    int commandShow)
{
    log << "Command line and command show procession started.";

    log << "There is no command procession just now, motherfucker!";

    log << "Command line and command show procession succeeded.";
    return;
}


GsResult GsEngine::initializeWindow(
    GsDisplayParameters displayParameters)
{
    log << "Window initialization started.";

    GsResult result;

    if(failed(result = registerMainWindowClass()))
        return result;


    if(!(window = CreateWindowEx(0, (LPCWSTR)programName.c_str(), (LPCWSTR)programName.c_str(), WS_OVERLAPPEDWINDOW | WS_VISIBLE, displayParameters.x, displayParameters.y,
                                 displayParameters.width, displayParameters.height, nullptr, nullptr, mainInstance, nullptr)))
    {
        log << "Window creation failed.";
        return GS_FAIL__CREATE_WINDOW;
    }
    else
        log << "Window creation succeeded.";



    mainInstance = GetModuleHandle(nullptr);

    log << "Window initialization succeeded.";
    return GS_OK;
}

GsResult GsEngine::initializeGraphics(
    std::string graphicsApiName,
    GsDisplayParameters displayParameters)
{
    log << "Graphics initialization started.";

    GsResult result;

    renderer = new GsRenderer(log);

    if(failed(renderer->createDevice(graphicsApiName)))
        return GS_FAIL__RENDERER_CREATE_DEVICE;

    renderDevice = renderer->getDevice();

    if(!renderDevice)
        return GS_FAIL__RENDER_DEVICE_IS_NULL;


    if(failed(result = renderDevice->init(window, displayParameters)))
        return result;

    if(failed(result = renderDevice->createFont("Arial", 10, false, false, false, 30, nullptr)))
        return result;

    if(failed(result = renderDevice->initStage(0.8, nullptr)))
        return result;

    framesPerSecond = 0;
    framesCounterHandle = CreateThread(nullptr, 0, showFramesPerSecond, (void*)&framesPerSecond, 0, nullptr);

    //     if(failed(result = initializeLight()))
    //         return result;

    log << "Graphics initialization succeeded.";
    return GS_OK;
}

GsResult GsEngine::initializeSound(
    std::string soundApiName)
{
    log << "Sound initialization started.";

    log << "There is no sound just now, motherfucker!";

    log << "Sound initialization succeeded.";
    return GS_OK;
}

GsResult GsEngine::initializeInput()
{
    log << "Input devices initialization started.";


    inputProvider = new GsInputProvider(log);

    if(failed(inputProvider->createDevice("DirectInput 8")))
        return GS_FAIL;

    inputDevice = inputProvider->getDevice();

    if(failed(inputDevice->init(mainInstance, window)))
    {
        log << "Input devices initialization failed";
        return GS_FAIL;
    }


    log << "Input devices initialization succeeded.";
    return GS_OK;
}

GsResult GsEngine::initializeNetwork(
    std::string networkApiName)
{
    log << "Network initialization started.";

    log << "There is no network just now, motherfucker!";

    log << "Network initialization succeeded.";
    return GS_OK;
}

LRESULT WINAPI GsEngine::processMessages(
    HWND hWnd,
    unsigned int msg,
    WPARAM wParam,
    LPARAM lParam)
{
    switch(msg)
    {
        case WM_ACTIVATE:
        {
            active = (BOOL)wParam;
        }
        break;

        case WM_KEYDOWN:
        {
            switch(wParam)
            {
                case VK_ESCAPE:
                {
                    stop = true;
                    PostMessage(hWnd, WM_CLOSE, 0, 0);
                    return 0;
                }

                case VK_RETURN:
                {
                    shadersEnabled = !shadersEnabled;

                    if(shadersEnabled)
                        shadersModeLabel = "shaders on";
                    else
                        shadersModeLabel = "shaders off";

                    log << shadersModeLabel;

                    return 0;
                }

                case VK_SPACE:
                {
                    rotateModels = !rotateModels;
                    return 0;
                }
            }
        }
        break;

        case WM_DESTROY:
        {
            stop = true;
            PostQuitMessage(0);
            return 1;
        }

        default:
            break;
    }

    return DefWindowProc(hWnd, msg, wParam, lParam);
}

GsResult GsEngine::registerMainWindowClass()
{
    windowClass.cbSize = sizeof(windowClass);
    windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC | CS_DBLCLKS;
    windowClass.lpfnWndProc = processMessages;
    windowClass.cbClsExtra = 0;
    windowClass.cbWndExtra = 0;
    windowClass.hInstance = mainInstance;
    windowClass.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
    windowClass.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    windowClass.lpszMenuName = nullptr;
    windowClass.lpszClassName = (LPCWSTR)programName.data();
    windowClass.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);

    if(RegisterClassEx(&windowClass) == 0)
    {
        log << "Main window class registration failed.";
        return GS_FAIL__REGISTER_WINDOW_CLASS;
    }

    log << "Main window class registration succeeded.";
    return GS_OK;
}

GsResult GsEngine::initializeScene()
{
    log << "Scene initialization started.";

    scene = new GsScene(log, renderDevice);

    GsVector right(1.0, 0.0, 0.0), up(0.0, 1.0, 0.0), direction(0.0, 0.0, 1.0), position(0.0, 0.0, -5.0);

    if(failed(scene->setView(right, up, direction, position)))
    {
        log << "Scene default view setting failed.";
        return GS_FAIL;
    }

    log << "Scene default view setting succeeded.";

    if(failed(scene->setProjectionMode(GS_PROJECTION__PERSPECTIVE)))
    {
        log << "Scene projection mode setting failed.";
        return GS_FAIL;
    }

    log << "Scene projection mode setting succeeded.";

    GsMatrix world;
    world.setIdentity();

    if(failed(scene->setWorldTransform(world)))
    {
        log << "Scene world transform setting failed.";
        return GS_FAIL;
    }

    log << "Scene world transform setting succeeded.";

    log << "Scene initialization succeeded.";
    return GS_OK;
}

void GsEngine::loadShader(
    GsShaderType type,
    std::string fileName)
{
    switch(type)
    {
        case GS_SHADER__VERTEX:
            if(failed(renderDevice->createVertexShader(fileName.data(), &vertexShaders[vertexShadersNumber])))
            {
                log << "Failed to load vertex shader '" + fileName + "'.";
                return;
            }

            vertexShadersNumber++;
            break;

        case GS_SHADER__PIXEL:
            if(failed(renderDevice->createPixelShader(fileName.data(), &pixelShaders[pixelShadersNumber])))
            {
                log << "Failed to load pixel shader '" + fileName + "'.";
                return;
            }

            pixelShadersNumber++;
            break;

        case GS_SHADER__VIDEO_SHADER_MOTHERFUCKERRRR:
            log << "Video shader is not provided just now. But who knows...";
            break;

        default:
            break;
    }

    return;
}

GsResult GsEngine::initializeLight()
{
    GsColor colorBlack = { 0.0, 0.0, 0.0, 1.0 };
    renderDevice->setAmbientLightColor(colorBlack);

    GsLight newLight;
    GsColor colorWhite = { 1.0, 1.0, 1.0, 1.0 };

    newLight.type = GS_LIGHT__DIRECTIONAL;
    newLight.direction = GsVector(-1.0, 0.0, 0.0);
    newLight.range = 1000.0;
    newLight.diffuse = colorBlack;
    newLight.ambient = colorBlack;
    newLight.specular = colorWhite;

    GsResult result;

    if(failed(result = renderDevice->setLight(&newLight, 0)))
        log << "Light initialization failed.";
    else
        log << "Light initialization succeeded.";

    return result;
}

void GsEngine::loadScript(
    std::string fileName)
{
    log << "Script file '" + fileName + "' execution started.";
    std::ifstream file;

    file.open(fileName);

    if(!file.is_open())
    {
        log << "Failed to open script file '" + fileName + "'.";
        file.close();

        log << "Script file execution failed.";

        return;
    }

    std::vector<std::string> command;
    std::string line, subline;
    int newWordPosition;

    while(!file.eof())
    {
        std::getline(file, line);
        command.clear();

        while((newWordPosition = line.find_first_of(' ')) != -1)
        {
            subline = line.substr(0, newWordPosition);
            command.push_back(subline);

            newWordPosition = line.find_first_not_of(' ', newWordPosition);

            if(newWordPosition != -1)
                line = line.substr(newWordPosition);
        }

        if(line.length())
            command.push_back(line);

        processScriptCommand(command);
    }

    file.close();

    log << "Script file '" + fileName + "' execution succeeded.";
}

void GsEngine::processScriptCommand(
    std::vector<std::string>& command)
{
    if(command.empty())
        return;

    if(command[0] == "load")
    {
        if(command[1] == "model")
        {
            scene->loadModel(command[2]);
            return;
        }

        if(command[1] == "shader")
        {
            if(command[2] == "pixel")
            {
                loadShader(GS_SHADER__PIXEL, command[3]);
                return;
            }

            if(command[2] == "vertex")
            {
                loadShader(GS_SHADER__VERTEX, command[3]);
                return;
            }

            {
                log << "Unknown vertex type '" + command[2] + "' in script.";
                return;
            }
        }
    }

    if(command[0] == "use")
    {
        if(command[1] == "shaders")
        {
            if(command[2] == "1")
            {
                shadersEnabled = true;
                log << "Set engine to use shaders for first frame.";
                return;
            }

            if(command[2] == "0")
            {
                shadersEnabled = false;
                log << "Set engine not to use shaders for first frame.";
                return;
            }
        }

        if(command[1] == "shader")
        {
            if(command[2] == "pixel")
            {
                activePixelShader = atoi(command[3].data());
                log << "Set use pixel shader #" + command[3] + ".";
                return;
            }

            if(command[2] == "vertex")
            {
                activeVertexShader = atoi(command[3].data());
                log << "Set use vertex shader #" + command[3] + ".";
                return;
            }
        }

        if(command[1] == "clearcolor")
        {
            float red = atof(command[2].data());
            float green = atof(command[3].data());
            float blue = atof(command[4].data());
            float alpha = atof(command[5].data());

            GsColor newClearColor = { red, green, blue, alpha };

            renderDevice->setClearColor(newClearColor);
        }
    }

	if (command[0] == "set")
	{
		if (command[1] == "world")
		{
			if (command[2] == "identity")
			{
				GsMatrix world;
				world.setIdentity();
				scene->setWorldTransform(world);
				log << "Set world transform to identity matrix.";
				return;
			}
		}
	}

    log << "Unknown script command.";
}

DWORD WINAPI showFramesPerSecond(
    void* frames)
{
    while(GsEngine::active)
    {
        GsEngine::labelFrames = std::to_string(GsEngine::framesPerSecond) + " fps";
        GsEngine::log << GsEngine::labelFrames;
        GsEngine::framesPerSecond = 0;
        Sleep(1000);
    }

    return 0;
}