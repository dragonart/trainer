#pragma once


#include <QtCore/qglobal.h>


#ifdef GSENGINE_LIB
# define GSENGINE_EXPORT Q_DECL_EXPORT
#else
# define GSENGINE_EXPORT Q_DECL_IMPORT
#endif