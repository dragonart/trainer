#pragma once


typedef unsigned long GsResult;

const GsResult GS_OK = 0x00000000;

const GsResult GS_CANCELED = 0x88888888;

const GsResult GS_FAIL = 0xffff0000;
const GsResult GS_FAIL__INVALID_PARAMETER = 0xffff0000;
const GsResult GS_FAIL__FUNCTION_NOT_FOUND_IN_DLL = 0xffff0001;
const GsResult GS_FAIL__LOAD_LIBRARY = 0xffff0002;
const GsResult GS_FAIL__CREATE_RENDER_DEVICE_FROM_DLL = 0xffff0003;
const GsResult GS_FAIL__API_NOT_SUPPORTED = 0xffff0003;
const GsResult GS_FAIL__API_NOT_CREATED = 0xffff0004;
const GsResult GS_FAIL__DIRECTX9_ENUMERATE_ADAPTERS_FAILED = 0xffff0005;
const GsResult GS_FAIL__DIRECTX9_DIRECT3D_IS_NULL = 0xffff0006;
const GsResult GS_FAIL__DIRECTX9_NO_SUCH_WINDOW = 0xffff0007;
const GsResult GS_FAIL__DIRECTX9_GET_BACK_BUFFER = 0xffff0008;
const GsResult GS_FAIL__DIRECTX9_CLEAR = 0xffff0009;
const GsResult GS_FAIL__DIRECTX9_BEGIN_SCENE = 0xffff000a;
const GsResult GS_FAIL__DIRECTX9_GSRENDERDEVICE_IS_NULL = 0xffff000b;
const GsResult GS_FAIL__DIRECTX9_LOG_CREATE_LOG = 0xffff000c;
const GsResult GS_FAIL__DIRECT3D_CREATE_DEVICE = 0xffff000d;
const GsResult GS_FAIL__RENDERER_CREATE_DEVICE = 0xffff000e;
const GsResult GS_FAIL__RENDER_DEVICE_IS_NULL = 0xffff000f;
const GsResult GS_FAIL__REGISTER_WINDOW_CLASS = 0xffff0010;
const GsResult GS_FAIL__CREATE_WINDOW = 0xffff0011;
const GsResult GS_FAIL__NO_WINDOW_WITH_SUCH_INDEX = 0xffff0012;
const GsResult GS_FAIL__DIRECT3DDEVICE9_CLEAR = 0xffff0013;
const GsResult GS_FAIL__DIRECT3DDEVICE_BEGIN_SCENE = 0xffff0014;
const GsResult GS_FAIL__DIRECT3D_CREATE9 = 0xffff0015;
const GsResult GS_FAIL__DIRECT3D_GET_DEVICE_CAPS = 0xffff0016;
const GsResult GS_FAIL__DIRECT3D_GET_ADAPTER_DISPLAY_MODE = 0xffff0017;
const GsResult GS_FAIL__DIRECT3DDEVICE9_RESET = 0xffff0018;
const GsResult GS_FAIL__INVALID_SKIN_ID = 0xffff0019;
const GsResult GS_FAIL__CREATE_TEXTURE = 0xffff001a;
const GsResult GS_FAIL__FILE_NOT_FOUND = 0xffff001b;
const GsResult GS_FAIL__TEXTURE_FILE_NOT_24_BIT = 0xffff001c;
const GsResult GS_FAIL__DIRECT3DDEVICE9_CREATE_TEXTURE = 0xffff001d;
const GsResult GS_FAIL__DIRECT3DTEXTURE9_LOCK_RECT = 0xffff001e;
const GsResult GS_FAIL__INVALID_TEXTURE_FORMAT = 0xffff001f;
const GsResult GS_FAIL__SCENE_NOT_RUNNING = 0xffff0020;
const GsResult GS_FAIL__DIRECT3DDEVICE9__SET_TRANSFORM = 0xffff0021;
const GsResult GS_FAIL__DIRECT3D9_VIEW_LOOK_AT__VECTOR_IS_TOO_SMALL = 0xffff0022;
const GsResult GS_FAIL__DIRECT3D9__CLIPPING_DISTANCES_ARE_TOO_CLOSE = 0xffff0023;
const GsResult GS_FAIL__DIRECT3D9__FOV_IS_TOO_SMALL = 0xffff0024;
const GsResult GS_FAIL__DIRECT3D9__SCENE_IS_NOT_RUNNING = 0xffff0025;
const GsResult GS_FAIL__DIRECT3DDEVICE9__SET_VIEWPORT = 0xffff0026;
const GsResult GS_FAIL__DIRECT3D9__CALCULATE_PERSPECTIVE_PROJECTION_MATRIX = 0xffff0027;
const GsResult GS_FAIL__DIRECT3DDEVICE9__SET_MATERIAL = 0xffff0028;
const GsResult GS_FAIL__DIRECT3D9__CREATE_VERTEX_SHADER = 0xffff0029;
const GsResult GS_FAIL__DIRECT3D9__INIT_STAGE = 0xffff002a;
const GsResult GS_FAIL_DIRECT3D9__SET_MODE = 0xffff002b;
const GsResult GS_FAIL__DIRECT3D9__CREATE_VERTEX_SHADER__FILE_NOT_FOUND = 0xffff002c;
const GsResult GS_FAIL__DIRECT3D9__CREATE_VERTEX_SHADER__ASSEMBLE_VERTEX_SHADER = 0xffff002d;
const GsResult GS_FAIL__DIRECT3DDEVICE9__CREATE_VERTEX_SHADER = 0xffff002e;
const GsResult GS_FAIL__DIRECT3D9__ACTIVATE_VERTEX_SHADER__NO_SHADER_SUPPORT = 0xffff002f;
const GsResult GS_FAIL__DIRECT3D9_ACTIVATE_VERTEX_SHADER__INVALID_SHADER_ID = 0xffff0030;
const GsResult GS_FAIL__DIRECT3DDEVICE9__SET_VERTEX_DECLARATION = 0xffff0031;
const GsResult GS_FAIL__DIRECT3D9__ACTIVATE_VERTEX_SHADER__INVALID_VERTEX_TYPE = 0xffff0032;
const GsResult GS_FAIL__DIRECT3DDEVICE9__SET_VERTEX_SHADER = 0xffff0033;
const GsResult GS_FAIL__DIRECT3D9__CREATE_PIXEL_SHADER__FILE_NOT_FOUND = 0xffff0034;
const GsResult GS_FAIL__DIRECT3D9__CREATE_PIXEL_SHADER__ASSEMBLE_SHADER = 0xffff0035;
const GsResult GS_FAIL__DIRECT3DDEVICE9__CREATE_PIXEL_SHADER = 0xffff0036;
const GsResult GS_FAIL__DIRECT3D9__ACRTIVATE_PIXEL_SHADER__NO_SHADER_SUPPORT = 0xffff0037;
const GsResult GS_FAIL__DIRECT3D9__ACTIVATE_PIXEL_SHADER__INVALID_ID = 0xffff0038;
const GsResult GS_FAIL__DIRECT3DDEVICE9__SET_PIXEL_SHADER = 0xffff0039;
const GsResult GS_FAIL__VERTEXCACHE__CANT_ADD_SO_MUCH = 0xffff003a;
const GsResult GS_FAIL__VERTEXCACHE__FLUSH = 0xffff003b;
const GsResult GS_FAIL__DIRECT3DVERTEXBUFFER9__LOCK = 0xffff003c;
const GsResult GS_FAIL__DIRECT3DINDEXBUFFER9__LOCK = 0xffff003d;
const GsResult GS_FAIL__INVALID_ID = 0xffff003e;
const GsResult GS_FAIL__OUT_OF_MEMORY = 0xffff003f;
const GsResult GS_FAIL__DIRECT3DDEVICE9__CREATE_INDEX_BUFFER = 0xffff0040;
const GsResult GS_FAIL__DIRECT3DDEVICE9__CREATE_VERTEX_BUFFER = 0xffff0041;



inline bool failed(
	GsResult result)
{
	return (result >= GS_FAIL);
}


const auto GS_MAX_ID = 65535;