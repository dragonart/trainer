#include "gsmatrix.h"

#include "gsvector.h"

#include <vector>


GsMatrix::GsMatrix()
{
	std::array<float, GS_DIMENSIONS_NUMBER + 1> line;
	line.fill(0.0);

	elements.fill(line);
}

GsMatrix::~GsMatrix()
{

}

void GsMatrix::setIdentity()
{
	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
			if (i == j)
				elements[i][j] = 1.0;
			else
				elements[i][j] = 0.0;
}


void GsMatrix::setRotationX(
    float angle)
{
     float sine = sin(angle);
     float cosine = cos(angle);

	 setIdentity();

	 elements[1][1] = cosine;
	 elements[1][2] = -sine;
	 elements[2][1] = sine;
	 elements[2][2] = cosine;
}

void GsMatrix::setRotationY(
    float angle)
{
     float sine = sin(angle);
     float cosine = cos(angle);

	 setIdentity();

	 elements[0][0] = cosine;
	 elements[0][2] = sine;
	 elements[2][0] = -sine;
	 elements[2][2] = cosine;
}

void GsMatrix::setRotationZ(
    float angle)
{
     float sine = sin(angle);
     float cosine = cos(angle);

	 setIdentity();

	 elements[0][0] = cosine;
	 elements[0][1] = -sine;
	 elements[1][0] = sine;
	 elements[1][1] = cosine;
}

void GsMatrix::setRotation(
	const GsVector& axis,
	float angle)
{
	GsVector changebleAxis = axis;
	float cosine = cos(angle);
	float sine = sin(angle);
	float temp = 1.0 - cosine;

	if (!changebleAxis.isUnit())
		changebleAxis.normalize();


	setIdentity();

	elements[0][0] = cosine + temp * changebleAxis.x * changebleAxis.x;
	elements[0][1] = temp * changebleAxis.x * changebleAxis.y - sine * changebleAxis.z;
	elements[0][2] = temp * changebleAxis.x * changebleAxis.z + sine * changebleAxis.y;
	
	elements[1][0] = temp * changebleAxis.x * changebleAxis.y + sine * changebleAxis.z;
	elements[1][1] = cosine + temp * changebleAxis.y * changebleAxis.y;
	elements[1][2] = temp * changebleAxis.y * changebleAxis.z - sine * changebleAxis.x;

	elements[2][0] = temp * changebleAxis.x * changebleAxis.z - sine * changebleAxis.y;
	elements[2][1] = temp * changebleAxis.y * changebleAxis.z + sine * changebleAxis.x;
	elements[2][2] = cosine + temp * changebleAxis.z * changebleAxis.z;
}


void GsMatrix::setTranslation(
	GsVector& vector, 
	GsMatrixSetTranslationMode mode)
{
	if (mode == GS_MATRIX_SET_TRANSLATION__ERASE_CONTENT)
		setIdentity();

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
		elements[i][3] = vector[i];
}

GsVector GsMatrix::getTranslation() const
{
	return GsVector(elements[3][0], elements[3][1], elements[3][2]);
}

float GsMatrix::calculateDeterminant() const
{
	float result;
	
	result = elements[0][0] * (elements[1][1] * elements[2][2] * elements[3][3] + elements[1][2] * elements[2][3] * elements[3][1] + elements[1][3] * elements[2][1] * elements[3][2]
		- elements[1][3] * elements[2][2] * elements[3][1] - elements[1][2] * elements[2][1] * elements[3][3] - elements[1][1] * elements[2][3] * elements[3][2]);
	result -= elements[0][1] * (elements[1][0] * elements[2][2] * elements[3][3] + elements[1][2] * elements[2][3] * elements[3][0] + elements[1][3] * elements[2][0] * elements[3][2]
		- elements[1][3] * elements[2][2] * elements[3][0] - elements[1][2] * elements[2][0] * elements[3][3] - elements[1][0] * elements[2][3] * elements[3][2]);
	result += elements[0][2] * (elements[1][0] * elements[2][1] * elements[3][3] + elements[1][1] * elements[2][3] * elements[3][0] + elements[1][3] * elements[2][0] * elements[3][1]
		- elements[1][3] * elements[2][1] * elements[3][0] - elements[1][1] * elements[2][0] * elements[3][3] - elements[1][0] * elements[2][3] * elements[3][1]);
	result -= elements[0][3] * (elements[1][0] * elements[2][1] * elements[3][2] + elements[1][1] * elements[2][2] * elements[3][0] + elements[1][2] * elements[2][0] * elements[3][1]
		- elements[1][2] * elements[2][1] * elements[3][0] - elements[1][1] * elements[2][0] * elements[3][2] - elements[1][0] * elements[2][2] * elements[3][1]);

	return result;
}

GsMatrix GsMatrix::operator*(
	const GsMatrix& other) const
{
 	GsMatrix result;
	
	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
			for (auto k = 0; k < GS_DIMENSIONS_NUMBER + 1; k++)
				result.elements[i][j] += elements[i][k] * other.elements[k][j];

 	return result;
}

GsVector GsMatrix::operator*(
	GsVector& vector) const
{
	GsVector result;

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
	{
		result[i] = 0.0;

		for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
			result[i] += elements[i][j] * vector[j];
	}

	return result;
}

std::array<float, GS_DIMENSIONS_NUMBER + 1>& GsMatrix::operator[](
	unsigned short index)
{
	if (index > GS_DIMENSIONS_NUMBER)
		return elements[0];

	return elements[index];
}


GsMatrix GsMatrix::transposeOf(
	const GsMatrix& matrix)
{
	GsMatrix result;

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
			result.elements[i][j] = matrix.elements[j][i];

	return result;
}

GsMatrix GsMatrix::inverseOf(
	const GsMatrix& other)
{
	GsMatrix matrix = other;
	GsMatrix result;

	result.setIdentity();

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
	{
		float divider = matrix.elements[i][i];

		for (auto j = i; j < GS_DIMENSIONS_NUMBER + 1; j++)
			matrix.elements[i][j] /= divider;
		for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
			result.elements[i][j] /= divider;

		for (auto j = i + 1; j < GS_DIMENSIONS_NUMBER + 1; j++)
		{
			float multiplier = matrix.elements[j][i];

			for (auto k = i; k < GS_DIMENSIONS_NUMBER + 1; k++)
				matrix.elements[j][k] -= matrix.elements[i][k] * multiplier;
			for (auto k = 0; k < GS_DIMENSIONS_NUMBER + 1; k++)
				result.elements[j][k] -= result.elements[i][k] * multiplier;
		}
	}

	for (auto i = GS_DIMENSIONS_NUMBER; i >= 1; i--)
		for (auto j = i - 1; j >= 0; j--)
		{
			float multiplier = matrix.elements[j][i];

			for (auto k = i; k < GS_DIMENSIONS_NUMBER + 1; k++)
				matrix.elements[j][k] -= matrix.elements[i][k] * multiplier;
			for (auto k = 0; k < GS_DIMENSIONS_NUMBER + 1; k++)
				result.elements[j][k] -= result.elements[i][k] * multiplier;
		}

	return result;
}