#pragma once


#include "gsmathematics_global.h"

#include "gsmathematics.h"

#include "gsplane.h"
#include "gsaxisalignedboundingbox.h"


class GSMATHEMATICS_EXPORT GsPolygon
{
        friend class GsPlane;


    public:
		GsVector* points;
		unsigned int* indices;

		
		GsPolygon();
        ~GsPolygon();

        void set(
			const GsVector* points, 
			int pointsNumber,
            const unsigned int* indices, 
			int indicesNumber);

        void clip(
			const GsPlane& plane,
            GsPolygon* front,
            GsPolygon* back);
        void clip(
			const GsAxisAlignedBoundingBox& box);
        int cull(
			const GsAxisAlignedBoundingBox& box);

        void copyOf(
			const GsPolygon& other);

        void swapFaces();

        bool intersects(
			const GsRay& ray, 
			bool, 
			float* t);
        bool intersects(
			const GsRay& ray, 
			bool, 
			float fL, 
			float* t);

		int getPointsNumber() const;
		int getIndicesNumber() const;

		GsVector* getPoints();
		unsigned int* getIndices();
		GsPlane getPlane();

		GsAxisAlignedBoundingBox getBox();
        
		unsigned int getFlag();
		void setFlag(
			unsigned int n);

        void printToFile(
			FILE* file);


    private:
        GsPlane plane;

        int pointsNumber;
        int indicesNumber;
        GsAxisAlignedBoundingBox box;
        unsigned int   m_Flag;

        void calculateBoundingBox();
};