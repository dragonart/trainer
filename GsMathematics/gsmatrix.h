#pragma once


#include "gsmathematics_global.h"

#include "gsmathematics.h"

#include "array"


enum GsMatrixSetTranslationMode
{
	GS_MATRIX_SET_TRANSLATION__SAVE_DATA = 0,
	GS_MATRIX_SET_TRANSLATION__ERASE_CONTENT = 1
};


class GSMATHEMATICS_EXPORT GsMatrix
{
    public:
		GsMatrix();
        ~GsMatrix();

        void setIdentity();

        void setRotationX(
            float angle);
        void setRotationY(
            float angle);
        void setRotationZ(
            float angle);

        void setRotation(
            const GsVector& axis,
            float angle);

		void setTranslation(
			GsVector& vector, 
			GsMatrixSetTranslationMode mode = GS_MATRIX_SET_TRANSLATION__SAVE_DATA);
		GsVector getTranslation() const;

		float calculateDeterminant() const;

		std::array<float, GS_DIMENSIONS_NUMBER + 1>& operator[](
			unsigned short index);

        GsMatrix operator*(
            const GsMatrix& other) const;
		GsVector operator*(
			GsVector& vector) const;

		static GsMatrix transposeOf(
			const GsMatrix& other);
		static GsMatrix inverseOf(
			const GsMatrix& other);

	
	private:
		std::array<std::array<float, GS_DIMENSIONS_NUMBER + 1>, GS_DIMENSIONS_NUMBER + 1> elements;
};