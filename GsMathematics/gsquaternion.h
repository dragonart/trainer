#pragma once


#include "gsmathematics_global.h"

#include "gsmathematics.h"

#include "gsvector.h"

#include <array>


class GSMATHEMATICS_EXPORT GsQuaternion
{
    public:
		float& x;
		float& y;
		float& z;
		float& w;

        GsQuaternion();
        GsQuaternion(
            float x,
            float y,
            float z,
			float w);
		GsQuaternion(
			const GsQuaternion& other);
		GsQuaternion(
			GsVector vector,
			float angle);
        ~GsQuaternion();

		void createFromEulers(
			float rotationX, 
			float rotationY, 
			float rotationZ);
		void createFromEulers(
			GsEulerRotationAngles angles);

        void normalize();
        
		GsEulerRotationAngles getEulers() const;
		GsMatrix getMatrix() const;
		float getMagnitude() const;
		float getSquaredMagnitude() const;

		GsQuaternion operator=(
			const GsQuaternion& other);
		GsQuaternion operator=(
			const GsVector& vector);
		
		float& operator[](
			unsigned short index);

		GsQuaternion operator+(
			const GsQuaternion& other) const;
		void operator+=(
			const GsQuaternion& other);

		GsQuaternion operator-() const;
		GsQuaternion operator-(
			const GsQuaternion& other) const;
		void operator-=(
			const GsQuaternion& other);

		GsQuaternion operator/(
			float divider) const;
		void operator/=(
            float divider);

       
		GsQuaternion operator*(
			float multiplier) const;
		void operator*=(
			float multiplier);

        GsQuaternion operator*(
            GsVector& vector);

        GsQuaternion operator*(
            const GsQuaternion& other) const;
        void operator*=(
            const GsQuaternion& other);

        void rotate(
            const GsQuaternion& a,
            const GsQuaternion& b);
        GsVector rotate(
			const GsVector& vector);

		void getAxisAndAngle(
			GsVector& axis,
			float& angle);

		static GsQuaternion conjugateTo(
			const GsQuaternion& quaternion);
		static GsQuaternion inverseTo(
			const GsQuaternion& quaternion);
		static GsQuaternion slerp(
			const GsVector& from,
			const GsVector& to,
			float localTime);


	private:
		std::array<float, GS_DIMENSIONS_NUMBER + 1> elements;
};