#pragma once


#include "gsmathematics_global.h"

#include "gsmathematics.h"

#include "gsvector.h"


class GSMATHEMATICS_EXPORT GsOrientedBoundingBox
{
    public:
        float halfLengthX, halfLengthY, halfLengthZ;
        GsVector boxAxisX, boxAxisY, boxAxisZ;
        GsVector center;


		GsOrientedBoundingBox();
		~GsOrientedBoundingBox();

         void deTransform(
			const GsOrientedBoundingBox& other,
            const GsMatrix& matrix);

        bool intersects(
			const GsRay& ray, 
			float* t);
        bool intersects(
			const GsRay& ray, 
			float fL, 
			float* t);
        bool intersects(
			const GsOrientedBoundingBox& other);
        bool intersects(
			const GsVector& a,
            const GsVector& b,
            const GsVector& c);

        int cull(
			const GsPlane* planes, 
			int planesNumber);


    private:
        void ObbProj(
			const GsOrientedBoundingBox& other, 
			const GsVector& vector,
            float* min, 
			float* max);
        void TriProj(
			const GsVector& v0, 
			const GsVector& v1,
            const GsVector& v2, 
			const GsVector& vcV,
            float* min, 
			float* max);
};