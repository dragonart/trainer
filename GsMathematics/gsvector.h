#pragma once


#include "gsmathematics_global.h"

#include "gsmathematics.h"

#include "array"


class GSMATHEMATICS_EXPORT GsVector
{
    public:
        float& x;
        float& y;
        float& z;
        float& w;

        GsVector();
        GsVector(
            float x,
            float y,
            float z,
            float w = 1.0);
		GsVector::GsVector(
			const GsVector& other);
        ~GsVector();

        void set(
            float x,
            float y,
            float z,
            float w = 1.0);

        float getLength() const;
        float getSquaredLength() const;

        void negate();
        void normalize();

        float getAngleWithVector(
            const GsVector& other) const;

        void rotateWithMatrix(
            GsMatrix& matrix);
        void rotateInverseWithMatrix(
            GsMatrix& matrix);

        void difference(
            const GsVector& a,
            const GsVector& b);

        bool isUnit() const;

		GsVector operator=(
			const GsVector& other);
		GsVector operator=(
			const GsQuaternion& quaternion);

		float& operator[](
			unsigned short index);

        GsVector operator+(
            const GsVector& other) const;
        void operator+=(
            const GsVector& other);
        GsVector operator+(
            float scalar) const;
        void operator+=(
            float scalar);

        GsVector operator-() const;
        GsVector operator-(
            const GsVector& other) const;
        void operator-=(
            const GsVector& other);
        GsVector operator-(
            float scalar) const;
        void operator-=(
            float scalar);

        float operator*(
            const GsVector& other) const;
        GsVector operator*(
            float multiplier) const;
        void operator*=(
            float multiplier);

        GsVector operator/(
            float divider) const;
        void operator/=(
            float divider);

        static GsVector cross(
            const GsVector a,
            const GsVector b);


    private:
        std::array < float, GS_DIMENSIONS_NUMBER + 1 > elements;
};