#pragma once


#define GS_REAL__EPSILON FLT_EPSILON

template <class T>
inline bool arePracticallyEqual(T a, T b)
{
	return (abs(a - b) < 10.0 * GS_REAL__EPSILON);
}


template <class T>
inline T abs(T x)
{
	return (x > 0) ? x : -x;
}


const auto GS_DIMENSIONS_NUMBER = 3;


class GsAxisAlignedBoundingBox;
class GsMatrix;
class GsOrientedBoundingBox;
class GsPlane;
class GsPolygon;
class GsQuaternion;
class GsRay;
class GsVector;


const float GS_PI8 = 0.39269908169872415480783042290994;
const float GS_PI6 = 0.52359877559829887307710723054658;
const float GS_PI4 = 0.78539816339744830961566084581988;
const float GS_PI3 = 1.0471975511965977461542144610932;
const float GS_PI2 = 1.5707963267948966192313216916398;
const float GS_PI	= 3.1415926535897932384626433832795;
const float GS_2PI = 6.283185307179586476925286766559;
const float GS_3PI = 9.4247779607693797153879301498385;


struct GsEulerRotationAngles
{
	float rotationX;
	float rotationY;
	float rotationZ;
};

struct GsPoint
{
	int x;
	int y;
};

enum GsAxis
{
	GS_AXIS__X,
	GS_AXIS__Y,
	GS_AXIS__Z
};