#pragma once


#include "gsmathematics_global.h"

#include "gsmathematics.h"

#include "gsvector.h"


class GSMATHEMATICS_EXPORT GsPlane
{
    public:
        GsVector normal;
        GsVector point;
        float distanceToOrigin;


        GsPlane();
        ~GsPlane();

         void set(
			const GsVector& normal, 
			const GsVector& point);
         void set(
			const GsVector& normal, 
			const GsVector& point, 
			float distance);
         void set(
			const GsVector& a, 
			const GsVector& b, 
			const GsVector& c);

         float calculateDistanceToPoint(
			const GsVector& point);
        
		 int classify(
			const GsVector& point);
        int classify(
			const GsPolygon& polygon);

        bool clip(
			const GsRay*, 
			float, 
			GsRay*, 
			GsRay*);

        bool intersects(
			const GsVector& vc0, 
			const GsVector& vc1,
            const GsVector& vc2);
        bool intersects(
			const GsPlane& plane, 
			GsRay* intersection);
        bool intersects(
			const GsAxisAlignedBoundingBox& box);
        bool intersects(
			const GsOrientedBoundingBox& box);
};