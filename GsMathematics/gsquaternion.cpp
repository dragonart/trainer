#include "gsquaternion.h"

#include "gsmatrix.h"


GsQuaternion::GsQuaternion() : x(elements[0]), y(elements[1]), z(elements[2]), w(elements[3])
{
	x = 0.0;
	y = 0.0;
	z = 0.0;
	w = 1.0;
}

GsQuaternion::GsQuaternion(
	float newX,
	float newY, 
	float newZ, 
	float newW) : x(elements[0]), y(elements[1]), z(elements[2]), w(elements[3])
{
	x = newX;
	y = newY;
	z = newZ;
	w = newW;
}

GsQuaternion::GsQuaternion(
	GsVector vector,
	float angle) : x(elements[0]), y(elements[1]), z(elements[2]), w(elements[3])
{
	float sine = sin(angle / 2.0);
	
	vector.normalize();

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
		elements[i] = sine * vector[i];

	w = cos(angle / 2.0);
}

GsQuaternion::GsQuaternion(
	const GsQuaternion& other) : x(elements[0]), y(elements[1]), z(elements[2]), w(elements[3])
{
	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		elements[i] = other.elements[i];
}

GsQuaternion::~GsQuaternion()
{

}

void GsQuaternion::createFromEulers(
	float angleRotationX,
	float angleRotationY,
	float angleRotationZ)
{
	GsEulerRotationAngles angles;

	angles.rotationX = angleRotationX;
	angles.rotationY = angleRotationY;
	angles.rotationZ = angleRotationZ;

	createFromEulers(angles);
}

void GsQuaternion::createFromEulers(
	GsEulerRotationAngles angles)
{
	float cosX = cos(angles.rotationX / 2.0);
	float cosY = cos(angles.rotationY / 2.0);
	float cosZ = cos(angles.rotationZ / 2.0);

	float sinX = sin(angles.rotationX / 2.0);
	float sinY = sin(angles.rotationY / 2.0);
	float sinZ = sin(angles.rotationZ / 2.0);

	x = sinX * cosY * cosZ - cosX * sinY * sinZ;
	y = cosX * sinY * cosZ + sinX * cosY * sinZ;
	z = cosX * cosY * sinZ - sinX * sinY * cosZ;
	w = cosX * cosY * cosZ + sinX * sinY * sinZ;
}

void GsQuaternion::normalize()
{
	float magnitude = getMagnitude();

	if (magnitude != 0.0)
	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
			elements[i] /= magnitude;
}

GsEulerRotationAngles GsQuaternion::getEulers() const
{
	GsEulerRotationAngles angles;



	return angles;
}

GsMatrix GsQuaternion::getMatrix() const
{
	GsMatrix matrix;

	float xx = 2.0 * x * x;
	float xy = 2.0 * x * y;
	float xz = 2.0 * x * z;

	float yy = 2.0 * y * y;
	float yz = 2.0 * y * z;

	float zz = 2.0 * z * z;

	float wx = 2.0 * w * x;
	float wy = 2.0 * w * y;
	float wz = 2.0 * w * z;


	matrix[0][0] = 1.0 - (yy + zz);
	matrix[0][1] = xy - wz;
	matrix[0][2] = xz + wy;

	matrix[1][0] = xy + wz;
	matrix[1][1] = 1.0 - (xx + zz);
	matrix[1][2] = yz - wx;

	matrix[2][0] = xz - wy;
	matrix[2][1] = yz + wx;
	matrix[2][2] = 1.0 - (xx + yy);

	matrix[3][3] = 1.0;

	return matrix;
}

float GsQuaternion::getMagnitude() const
{
	return sqrt(getSquaredMagnitude());
}

float GsQuaternion::getSquaredMagnitude() const
{
	float SquaredMagnitude = 0.0;

	for (auto element : elements)
		SquaredMagnitude += element * element;

	return SquaredMagnitude;
}

GsQuaternion GsQuaternion::operator/(
	float divider) const
{
	GsQuaternion result = *this;

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		result.elements[i] /= divider;

	return result;
}

void GsQuaternion::operator/=(
	float divider)
{
	*this = *this / divider;
}

GsQuaternion GsQuaternion::operator*(
	float multiplier) const
{
	GsQuaternion result = *this;

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		result.elements[i] *= multiplier;

	return result;
}

void GsQuaternion::operator*=(
	float multiplier)
{
	*this = *this * multiplier;
}

GsQuaternion GsQuaternion::operator*(
	GsVector& vector)
{
	GsQuaternion result;

	result.x = w * vector.x + y * vector.z - z * vector.y;
	result.y = w * vector.y + z * vector.x - x * vector.z;
	result.z = w * vector.z + x * vector.y - y * vector.x;
	result.w = -(x * vector.x + y * vector.y + z * vector.z);

	return result;
}

GsQuaternion GsQuaternion::operator*(
	const GsQuaternion& other) const
{
	GsQuaternion result;

	result.w = w * other.w - x * other.x - y * other.y - z * other.z;
	
	result.x = x * other.w + other.x * w + y * other.z - other.y * z;
	result.y = y * other.w + other.y * w + z * other.x - other.z * x;
	result.z = z * other.w + other.z * w + x * other.y - other.x * y;

	return result;
}

void GsQuaternion::operator*=(
	const GsQuaternion& other)
{
	*this = *this * other;
}

GsQuaternion GsQuaternion::operator+(
	const GsQuaternion& other) const
{
	GsQuaternion result;

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		result[i] = elements[i] + other.elements[i];

	return result;
}

void GsQuaternion::operator+=(
	const GsQuaternion& other)
{
	*this = *this + other;
}

GsQuaternion GsQuaternion::operator-() const
{
	return GsQuaternion(-x, -y, -z, -w);
}

GsQuaternion GsQuaternion::operator-(
	const GsQuaternion& other) const
{
	return *this + (-other);
}

void GsQuaternion::operator-=(
	const GsQuaternion& other)
{
	*this = *this - other;
}

void GsQuaternion::rotate(
	const GsQuaternion& a,
	const GsQuaternion& b)
{

}

GsVector GsQuaternion::rotate(
	const GsVector& vector)
{
	return GsVector(0.0, 0.0, 0.0);
}

float& GsQuaternion::operator[](
	unsigned short index)
{
	if (index > GS_DIMENSIONS_NUMBER)
		return elements[0];

	return elements[index];
}

GsQuaternion GsQuaternion::operator=(
	const GsQuaternion& other)
{
	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		elements[i] = other.elements[i];

	return *this;
}

GsQuaternion GsQuaternion::operator=(
	const GsVector& vector)
{
	x = vector.x;
	y = vector.y;
	z = vector.z;
	w = 0.0;
	
	return *this;
}


GsQuaternion GsQuaternion::conjugateTo(
	const GsQuaternion& quaternion)
{
	GsQuaternion result;

	result.x = -quaternion.x;
	result.y = -quaternion.y;
	result.z = -quaternion.z;
	result.w = quaternion.w;

	return result;
}

GsQuaternion GsQuaternion::inverseTo(
	const GsQuaternion& quaternion)
{
	GsQuaternion result = conjugateTo(quaternion);

	result /= quaternion.getSquaredMagnitude();

	return result;
}

GsQuaternion GsQuaternion::slerp(
	const GsVector& from,
	const GsVector& to,
	float localTime)
{
	float angle = from.getAngleWithVector(to);
	GsQuaternion result;

	result = (from * sin((1.0 - localTime) * angle) + to * sin(localTime * angle)) / sin(angle);

	return result;
}

void GsQuaternion::getAxisAndAngle(
	GsVector& axis, 
	float& angle)
{
	float length = sqrt(x * x + y * y + z * z);
	if (!arePracticallyEqual(length, (float)0.0))
	{
		axis.set(x / length, y / length, z / length);
		if (w < 0.0)
			angle = 2.0 * atan2(-length, -w);
		else
			angle = 2.0 * atan2(length, w); 
	}
	else
	{
		axis = GsVector(0.0, 0.0, 0.0);
		angle = 0.0;
	}
}
