#include "gsvector.h"

#include "gsmatrix.h"
#include "gsquaternion.h"


GsVector::GsVector() : x(elements[0]), y(elements[1]), z(elements[2]), w(elements[3])
{
	elements.fill(0.0);

	w = 1.0;
}

GsVector::GsVector(
    float newX,
    float newY,
    float newZ,
	float newW) : x(elements[0]), y(elements[1]), z(elements[2]), w(elements[3])
{
    x = newX;
	y = newY;
    z = newZ;
    w = newW;
}

GsVector::GsVector(
	const GsVector& other) : x(elements[0]), y(elements[1]), z(elements[2]), w(elements[3])
{
	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		elements[i] = other.elements[i];
}

GsVector::~GsVector()
{

}

void GsVector::set(
    float newX,
    float newY,
    float newZ,
    float newW)
{
    x = newX;
    y = newY;
    z = newZ;
    w = newW;
}


float GsVector::getLength() const
{
    return sqrt(getSquaredLength());
}

float GsVector::getSquaredLength() const
{
    float result = 0;

    for(auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
        result += elements[i] * elements[i];

    return result;
}


void GsVector::negate()
{
    for(auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
		elements[i] = -elements[i];
}

void GsVector::normalize()
{
    float length = getLength();

    if(length != 0.0)
        for(auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
			elements[i] /= length;
}


float GsVector::getAngleWithVector(
	const GsVector& other) const
{
    float cosine = (*this) * other / (getLength() * other.getLength());
    return acos(cosine);
}


void GsVector::rotateWithMatrix(
	GsMatrix& matrix)
{
    std::array<float, GS_DIMENSIONS_NUMBER> newelements;
    newelements.fill(0.0);

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
		for (auto j = 0; j < GS_DIMENSIONS_NUMBER; j++)
			newelements[i] += matrix[i][j] * elements[j];

    for(auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
        elements[i] = newelements[i];
}

void GsVector::rotateInverseWithMatrix(
	GsMatrix& matrix)
{
    std::array<float, GS_DIMENSIONS_NUMBER> newelements;
    newelements.fill(0.0);

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
		for (auto j = 0; j < GS_DIMENSIONS_NUMBER; j++)
			newelements[i] += matrix[j][i] * elements[j];

    for(auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
        elements[i] = newelements[i];
}


void GsVector::difference(
    const GsVector& a,
    const GsVector& b)
{
    for(auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
        elements[i] = b.elements[i] - a.elements[i];

    elements[GS_DIMENSIONS_NUMBER] = 1.0;
}

GsVector GsVector::cross(
	const GsVector a, 
	const GsVector b)
{
	GsVector result;

	result.elements[0] = a.elements[1] * b.elements[2] - a.elements[2] * b.elements[1];
	result.elements[1] = a.elements[2] * b.elements[0] - a.elements[0] * b.elements[2];
	result.elements[2] = a.elements[0] * b.elements[1] - a.elements[1] * b.elements[0];

	result.elements[3] = 1.0;

	return result;
}


bool GsVector::isUnit() const
{
	return arePracticallyEqual(getSquaredLength(), (float)1.0);
}


GsVector GsVector::operator+(
	const GsVector& other) const
{
	return GsVector(elements[0] + other.elements[0], elements[1] + other.elements[1], elements[2] + other.elements[2]);
}

void GsVector::operator+=(
	const GsVector& other)
{
	*this = *this + other;
}

GsVector GsVector::operator+(
	float scalar) const
{
	return GsVector(elements[0] + scalar, elements[1] + scalar, elements[2] + scalar);
}

void GsVector::operator+=(
	float scalar)
{
	*this = *this + scalar;
}


GsVector GsVector::operator-() const
{
	return GsVector(-elements[0], -elements[1], -elements[2], elements[3]);
}

GsVector GsVector::operator-(
	const GsVector& other) const
{
	float newX = elements[0] - other.elements[0];
	float newY = elements[1] - other.elements[1];
	float newZ = elements[2] - other.elements[2];

	return GsVector(newX, newY, newZ);
}

void GsVector::operator-=(
    const GsVector& other)
{
	*this = *this - other;
}

GsVector GsVector::operator-(
	float scalar) const
{
	return GsVector(elements[0] - scalar, elements[1] - scalar, elements[2] - scalar);
}

void GsVector::operator-=(
	float scalar)
{
	*this = *this - scalar;
}

float GsVector::operator*(
	const GsVector& other) const
{
	float result = 0.0;

	for (auto i = 0; i < GS_DIMENSIONS_NUMBER; i++)
		result += elements[i] * other.elements[i];

	return result;
}

GsVector GsVector::operator*(
	float multiplier) const
{
	return GsVector(x *multiplier, y * multiplier, z * multiplier, w);
}

void GsVector::operator*=(
    float multiplier)
{
	*this = *this * multiplier;
}

GsVector GsVector::operator/(
	float divider) const
{
	return GsVector(x / divider, y / divider, z / divider, w);
}

void GsVector::operator/=(
    float divider)
{
	*this = *this / divider;
}

float& GsVector::operator[](
	unsigned short index)
{
	if (index > GS_DIMENSIONS_NUMBER)
		return elements[0];

	return elements[index];
}

GsVector GsVector::operator=(
	const GsVector& other)
{
	for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
		elements[i] = other.elements[i];

	return *this;
}

GsVector GsVector::operator=(
	const GsQuaternion& quaternion)
{
	x = quaternion.x;
	y = quaternion.y;
	z = quaternion.z;
	w = 1.0;

	return *this;
}
