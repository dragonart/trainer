#pragma once


#include "gsmathematics_global.h"

#include "gsmathematics.h"

#include "gsvector.h"


class GSMATHEMATICS_EXPORT GsAxisAlignedBoundingBox
{
    public:
        GsVector pointMin, pointMax;
        GsVector center;     


		GsAxisAlignedBoundingBox();
		GsAxisAlignedBoundingBox(
			GsVector pointMin, 
			GsVector pointMax);
		~GsAxisAlignedBoundingBox();

		void buildFromOrientedBoundingBox(
			const GsOrientedBoundingBox& box);

        int cull(
			const GsPlane* planes, 
			int planesNumber);

        void getPlanes(
			GsPlane* planes);

        bool contains(
			const GsRay& ray, 
			float fL);
        
		bool intersects(
			const GsRay& ray, 
			float* t);
        bool intersects(
			const GsRay& Ray, 
			float fL, 
			float* t);
        bool intersects(
			const GsAxisAlignedBoundingBox& other);
        bool intersects(
			const GsVector& vector);
};