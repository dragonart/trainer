#include "gsaxisalignedboundingbox.h"


GsAxisAlignedBoundingBox::GsAxisAlignedBoundingBox()
{

}

GsAxisAlignedBoundingBox::GsAxisAlignedBoundingBox(GsVector pointMin, GsVector pointMax)
{

}

GsAxisAlignedBoundingBox::~GsAxisAlignedBoundingBox()
{

}

void GsAxisAlignedBoundingBox::buildFromOrientedBoundingBox(const GsOrientedBoundingBox& box)
{

}

int GsAxisAlignedBoundingBox::cull(const GsPlane* planes, int planesNumber)
{
	return 0;
}

void GsAxisAlignedBoundingBox::getPlanes(GsPlane* planes)
{

}

bool GsAxisAlignedBoundingBox::contains(const GsRay& ray, float fL)
{
	return false;
}

bool GsAxisAlignedBoundingBox::intersects(const GsRay& ray, float* t)
{
	return false;
}

bool GsAxisAlignedBoundingBox::intersects(const GsRay& Ray, float fL, float* t)
{
	return false;
}

bool GsAxisAlignedBoundingBox::intersects(const GsAxisAlignedBoundingBox& other)
{
	return false;
}

bool GsAxisAlignedBoundingBox::intersects(const GsVector& vector)
{
	return false;
}
