#pragma once


#include "gsmathematics_global.h"

#include "gsmathematics.h"

#include "gsvector.h"


class GSMATHEMATICS_EXPORT GsRay
{
    public:
        GsVector origin;
        GsVector direction;


        GsRay();
        ~GsRay();

        void set(
            GsVector origin,
            GsVector direction);
        void deTransform(
            const GsMatrix& matrix);

        bool intersects(
            const GsVector& a,
            const GsVector& b,
            const GsVector& c,
            bool cull,
            float* t);
        bool intersects(
            const GsVector& a,
            const GsVector& b,
            const GsVector& c,
            bool cull,
            float fL,
            float* t);

        bool intersects(
            const GsPlane& plane,
            bool cull,
            float* t,
            GsVector* vcHit);
        bool intersects(
            const GsPlane& plane,
            bool cull,
            float fL,
            float* t,
            GsVector* vcHit);

        bool intersects(
            const GsAxisAlignedBoundingBox& box,
            float* t);
        bool intersects(
            const GsAxisAlignedBoundingBox& box,
            float fL,
            float* t);

        bool intersects(
            const GsOrientedBoundingBox& box,
            float* t);
        bool intersects(
            const GsOrientedBoundingBox& box,
            float fL,
            float* t);
};