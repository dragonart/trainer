#pragma once


#include "gscolor.h"


struct GsMaterial
{
	GsColor ambient;
	GsColor diffuse;
	GsColor emissive;
	GsColor specular;

	float power;
};
