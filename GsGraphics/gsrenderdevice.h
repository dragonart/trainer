#pragma once


#include "../gs.h"
#include "../GsTools/gslog.h"
#include "gsgraphics.h"
#include "gscolor.h"
#include "gsskinmanager.h"
#include "gsvertexcachemanager.h"
#include "gsviewport.h"
#include "gslight.h"

#include <windows.h>
#include <string>
#include <vector>


class GsRenderDevice
{
    public:
		GsSkinManager* skinManager;
		GsVertexCacheManager* vertexCacheManager;

		GsRenderDevice(
			GsLog& newLog) : log(newLog) {};
        virtual ~GsRenderDevice() {};

		virtual GsResult init(
			HWND window, 
			GsDisplayParameters displayParameters) = 0;
		virtual void release() = 0;

		virtual bool isWindowed() = 0;
		virtual GsResolution getResolution() = 0;

        virtual bool isInitialized() = 0;
        virtual bool isSceneRunning() = 0;

		virtual bool isUsingShaders() = 0;
		virtual void setUseShaders(
			bool use) = 0;
		virtual bool isThatCanUseShaders() = 0;
		virtual bool isThatCanDoShaders() = 0;
		virtual GsResult setShaderConstant(
			GsShaderType shaderType,
			GsDataType dataType,
			UINT, UINT, const void*) = 0;

		virtual void setUseAdditiveBlending(
			bool use) = 0;
		virtual bool isUsingAdditiveBlending() = 0;

		virtual GsResult switchWindowMode() = 0;

		virtual GsResult setView3D(
			GsVector& right,
			GsVector& up,
			GsVector& direction,
			GsVector& position) = 0;

		virtual GsResult setViewLookAt(
			GsVector& from,
			GsVector& to,
			GsVector& worldUp) = 0;

		virtual void setClippingPlanes(
			float nearDistance,
			float farDistance) = 0;

		virtual GsResult setMode(
			GsEngineMode mode) = 0;

		virtual void setOrthoScale(
			float scale) = 0;

		virtual GsResult initStage(
			float fov, 
			GsViewPort* viewPort) = 0;

		virtual GsResult getFrustum(
			GsClippingPlanes& clippingPlanes) = 0;

		virtual void transform2Dto3D(
			const GsPoint& point,
			GsVector* position,
			GsVector* direction) = 0;
		virtual GsVector transform2Dto2D(
			float scale, 
			const GsPoint* point, 
			GsAxis axis) = 0;

		virtual GsPoint transform3Dto2D(
			const GsVector& position) = 0;

		virtual void setWorldTransform(
			const GsMatrix* matrix) = 0;

		virtual void setBackFaceCulling(
			GsRenderState state) = 0;
		virtual void setStencilBufferMode(
			GsRenderState state, DWORD) = 0;

		virtual void setUseStencilShadowSettings(
			bool use) = 0;

		virtual void setUseColorBuffer(
			bool use) = 0;
		virtual bool isUsingColorBuffer() = 0;

		virtual void setUseTextures(
			bool use) = 0;
		virtual bool isUsingTextures() = 0;

		virtual void setDepthBufferMode(
			GsRenderState mode) = 0;

		virtual void setShadeMode(
			GsRenderState mode,
			float pointSize,
			const GsColor* wireframeColor) = 0;
		virtual GsRenderState getShadeMode() = 0;

		virtual GsResult setTextureStage(
			UCHAR,
			GsRenderState) = 0;

		virtual GsResult setLight(
			const GsLight* light,
			UCHAR stage) = 0;

		virtual GsResult createVertexShader(
			const void* data,
			UINT size,
			bool loadFromFile,
			bool isCompiled,
			UINT* index) = 0;
		virtual GsResult createPixelShader(
			const void* data,
			UINT size,
			bool loadFromFile,
			bool isCompiled,
			UINT* index) = 0;

		virtual GsResult createVertexShader(
			const char* fileName,
			UINT* index) = 0;
		virtual GsResult createPixelShader(
			const char* fileName,
			UINT* index) = 0;

		virtual GsResult activateVertexShader(
			UINT index,
			GsVertexType vertexType) = 0;
		virtual GsResult activatePixelShader(
			UINT index) = 0;


		virtual GsResult beginRendering(
			GsClearTarget clearTarget) = 0;
		virtual void endRendering() = 0;

		virtual GsResult clear(
			GsClearTarget clearTarget) = 0;

		virtual void setClearColor(
			GsColor color) = 0;

		virtual void fadeScreen(
			GsColor color) = 0;

		virtual GsResult createFont(
			const char* type,
			int weight,
			bool italic,
			bool underine,
			bool striked,
			DWORD size,
			UINT* index) = 0;

		virtual GsResult drawText(
			UINT fontIndex,
			int x,
			int y,
			GsColor color,
			const char* text, ...) = 0;

		virtual void setAmbientLightColor(
			const GsColor& color) = 0;

		virtual GsResult registerConstantFloatForShader(
			GsShaderType shaderType,
			const char* name,
			float value, 
			UINT shaderIndex) = 0;
		virtual GsResult registerConstantFloat3ForShader(
			GsShaderType shaderType,
			const char* name,
			float value[3],
			UINT shaderIndex) = 0;
		virtual GsResult registerConstantFloat4ForShader(
			GsShaderType shaderType,
			const char* name,
			float value[4],
			UINT shaderIndex) = 0;


	protected:
		HWND window;

		unsigned int windowX;
		unsigned int windowY;

		unsigned int windowWidth;
		unsigned int windowHeight;

		unsigned int screenWidth;
		unsigned int screenHeight;

		bool windowed;

		std::string adapterName;

		bool initialized;
		bool sceneRunning;

		bool useStencil;
		bool useTextures;

		bool canUseShaders;
		bool canDoShaders;
		bool useShaders;
		bool useAdditiveBlending;
		bool useColorBuffer;

		GsRenderState shadeMode; // wireframe?
		GsColor wireframeColor;

		float clippingDistanceNear;
		float clippingDistanceFar;
		GsEngineMode mode; // perspective, orthogonal or 2D

		GsViewPort viewPort;

		
		GsLog& log;

		void setDisplayParameters(
			GsDisplayParameters displayParameters);
};