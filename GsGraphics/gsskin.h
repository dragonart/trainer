#pragma once


#include <wtypes.h>


const auto GS_TEXTURES_PER_SKIN = 8;


struct GsSkin
{
	bool alphaEnabled;
	UINT materialIndex;
	UINT textureIndices[GS_TEXTURES_PER_SKIN];
};