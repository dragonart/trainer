#pragma once


#include "gscolor.h"
#include <wtypes.h>


struct GsTexture
{
	float alpha;
	char* name;
	void* data;
	GsColor* colorKeys;
	DWORD colorKeysNumber;
};