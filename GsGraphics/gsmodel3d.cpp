#include "gsmodel3d.h"


GsModel3D::GsModel3D(
	GsLog& newLog, 
	GsRenderDevice* newDevice) : log(newLog)
{
    device = newDevice;

    skinsIndicesNumber = 0;
    verticesNumber = 0;
    indicesNumber = 0;

    skinsIndices = nullptr;
    indices = nullptr;
    vertices = nullptr;
    vertices3t = nullptr;
    verticesTangent = nullptr;
    indicesPerMaterialNumbers = nullptr;

    buffer3t = nullptr;
    staticBuffers = nullptr;
    buffersTangent = nullptr;

	isReady = false;
}

GsModel3D::~GsModel3D()
{
	release();
}


GsResult GsModel3D::render(
    bool useStatic,
    bool b3t,
    bool tangentModel)
{
    GsResult result = GS_OK;

    if(useStatic)
        for(int i = 0; i < skinsIndicesNumber; i++)
        {
            if(b3t)
            {
                if(failed(device->vertexCacheManager->render(buffer3t[i])))
                    result = GS_FAIL;
            }
            else
                if(tangentModel)
                {
                    if(failed(device->vertexCacheManager->render(buffersTangent[i])))
                        result = GS_FAIL;
                }
                else
                    if(failed(device->vertexCacheManager->render(staticBuffers[i])))
                        result = GS_FAIL;
        }
    else
    {
        WORD* pIndices = indices;

        for(int i = 0; i < skinsIndicesNumber; indices += indicesPerMaterialNumbers[i], i++)
        {
            if(b3t)
            {
                if(failed(device->vertexCacheManager->render(GS_VERTEX__THREE_TEXTURE_COORD_PAIRS, verticesNumber, indicesPerMaterialNumbers[i], vertices3t, indices, skinsIndices[i])))
                    result = GS_FAIL;
            }
            else
            {
                if(tangentModel)
                {
                    if(failed(device->vertexCacheManager->render(GS_VERTEX__UNTRANSFORMED_UNLIT_WITH_TANGENT_VECTOR, verticesNumber, indicesPerMaterialNumbers[i], verticesTangent, indices,
                              skinsIndices[i])))
                        result = GS_FAIL;
                }
                else
                    if(failed(device->vertexCacheManager->render(GS_VERTEX__UNTRANSFORMED_UNLIT, verticesNumber, indicesPerMaterialNumbers[i], vertices, indices, skinsIndices[i])))
                        result = GS_FAIL;
            }
        }
    }

    return result;
}


GsResult GsModel3D::loadModelFromFile(
	const std::string fileName)
{
	int fileFormatStartPosition = fileName.find_last_of('.');
	std::string fileFormat = fileName.substr(fileFormatStartPosition);

	GsResult result;
	if (fileFormat == ".s3d")
		result = loadFromS3d(fileName);
	else
		if (fileFormat == ".obj")
		result = loadFromObj(fileName);

	if (failed(result))
	{
		log << "Failed to load model from file '" + fileName + "'. Unknown format '" + fileFormat + "'.";
		isReady = false;
		return GS_FAIL;
	}

	log << "Loading model from file '" + fileName + "' succeeded.";
	isReady = true;
	return GS_OK;
}

GsResult GsModel3D::loadFromObj(
	std::string fileName)
{
	return GS_FAIL;
}

GsResult GsModel3D::loadFromS3d(
	std::string fileName)
{
	file = fopen(fileName.data(), "r");

	GsColor ambient, diffuse, emissive, specular;
	UINT facesNumber, trianglesNumber = 0;

	float power = 0;

	char line[100];
	char texture[100];

	GsTriangle* triangles = nullptr;


	SEEK(line, "BEGIN_SKINS");
	NEXT(line);
	sscanf(line, "%d;", &skinsIndicesNumber);
	skinsIndices = new UINT[skinsIndicesNumber];

	for (UINT i = 0; i < skinsIndicesNumber; i++)
	{
		NEXT(line);
		NEXT(line);

		sscanf(line, "%f, %f, %f, %f;", &ambient.red, &ambient.green, &ambient.blue, &ambient.alpha);
		NEXT(line);
		sscanf(line, "%f, %f, %f, %f;", &diffuse.red, &diffuse.green, &diffuse.blue, &diffuse.alpha);
		NEXT(line);
		sscanf(line, "%f, %f, %f, %f;", &emissive.red, &emissive.green, &emissive.blue, &emissive.alpha);
		NEXT(line);
		sscanf(line, "%f, %f, %f, %f, %f;", &specular.red, &specular.green, &specular.blue, &specular.alpha, &power);
		NEXT(line);
		sscanf(line, "%s", &texture);

		device->skinManager->addSkin(ambient, diffuse, emissive, specular, power, &skinsIndices[i]);
		device->skinManager->addTexture(skinsIndices[i], texture, false, 0, nullptr, 0);

		NEXT(line);
	}

	rewind(file);
	SEEK(line, "BEGIN_VERTICES");
	NEXT(line);
	sscanf(line, "%d;", &verticesNumber);

	vertices = new GsVertexPositionNormalTexture[verticesNumber];
	vertices3t = new GsVertexPositionNormal3Textures[verticesNumber];
	verticesTangent = new GsVertexPositionNormalTextureSomething[verticesNumber];

	for (UINT i = 0; i < verticesNumber; i++)
	{
		NEXT(line);
		sscanf(line, "%f,%f,%f;%f,%f;%f,%f,%f;", &vertices[i].x, &vertices[i].y, &vertices[i].z, &vertices[i].tu, &vertices[i].tv, &vertices[i].normal[0], &vertices[i].normal[1], &vertices[i].normal[2]);

		memset(&vertices3t[i], 0, sizeof(GsVertexPositionNormal3Textures));
		vertices3t[i].x = vertices[i].x;
		vertices3t[i].y = vertices[i].y;
		vertices3t[i].z = vertices[i].z;
		vertices3t[i].tu1 = vertices[i].tu;
		vertices3t[i].tv1 = vertices[i].tv;

		memset(&verticesTangent[i], 0, sizeof(GsVertexPositionNormalTextureSomething));
		verticesTangent[i].x = vertices[i].x;
		verticesTangent[i].y = vertices[i].y;
		verticesTangent[i].z = vertices[i].z;
		verticesTangent[i].tu = vertices[i].tu;
		verticesTangent[i].tv = verticesTangent[i].tv;
		verticesTangent[i].normal[0] = vertices[i].normal[0];
		verticesTangent[i].normal[1] = vertices[i].normal[1];
		verticesTangent[i].normal[2] = vertices[i].normal[2];
	}

	rewind(file);
	SEEK(line, "BEGIN_FACES");
	NEXT(line);
	sscanf(line, "%d;", &facesNumber);

	triangles = new GsTriangle[facesNumber];

	for (UINT i = 0; i < facesNumber; i++)
	{
		NEXT(line);
		sscanf(line, "%d,%d,%d;%d", &triangles[trianglesNumber].i0, &triangles[trianglesNumber].i1, &triangles[trianglesNumber].i2, &triangles[trianglesNumber].materialIndex);
		trianglesNumber++;
	}

	qsort((void*)triangles, trianglesNumber, sizeof(GsTriangle), (comparingFunc)sortTriangles);


	UINT oldMaterialsNumber = triangles[0].materialIndex;
	indicesPerMaterialNumbers = new UINT[skinsIndicesNumber];
	staticBuffers = new UINT[skinsIndicesNumber];
	buffer3t = new UINT[skinsIndicesNumber];
	buffersTangent = new UINT[skinsIndicesNumber];

	memset(indicesPerMaterialNumbers, 0, sizeof(UINT) * skinsIndicesNumber);
	memset(staticBuffers, 0, sizeof(UINT) * skinsIndicesNumber);
	memset(buffer3t, 0, sizeof(UINT) * skinsIndicesNumber);
	memset(buffersTangent, 0, sizeof(UINT) * skinsIndicesNumber);

	indices = new WORD[trianglesNumber * 3];
	indicesNumber = facesNumber * 3;

	for (UINT i = 0; i < trianglesNumber; i++)
	{
		indices[i * 3] = triangles[i].i0;
		indices[i * 3 + 1] = triangles[i].i1;
		indices[i * 3 + 2] = triangles[i].i2;

		if (triangles[i].materialIndex != oldMaterialsNumber)
			oldMaterialsNumber = triangles[i].materialIndex;
		else
			indicesPerMaterialNumbers[triangles[i].materialIndex] += 3;
	}


	WORD* pIndices = indices;

	for (UINT i = 0; i < skinsIndicesNumber; pIndices += indicesPerMaterialNumbers[i], i++)
	{
		if (failed(device->vertexCacheManager->createStaticBuffer(GS_VERTEX__UNTRANSFORMED_UNLIT, skinsIndices[i], verticesNumber, indicesPerMaterialNumbers[i], vertices,
			pIndices, &staticBuffers[i])))
			log << "Error: createStaticBuffer() failed.";

		if (failed(device->vertexCacheManager->createStaticBuffer(GS_VERTEX__THREE_TEXTURE_COORD_PAIRS, skinsIndices[i], verticesNumber, indicesPerMaterialNumbers[i], vertices3t,
			pIndices, &buffer3t[i])))
			log << "Error: createStaticBuffer() for 3T failed.";

		if (failed(device->vertexCacheManager->createStaticBuffer(GS_VERTEX__UNTRANSFORMED_UNLIT_WITH_TANGENT_VECTOR, skinsIndices[i], verticesNumber,
			indicesPerMaterialNumbers[i], verticesTangent, pIndices, &buffersTangent[i])))
			log << "Error: createStaticBuffer() for tangent failed.";
	}

	delete[] triangles;
	fclose(file);

	return GS_OK;
}

void GsModel3D::release()
{
	if (indices)
	{
		delete[] indices;
		indices = nullptr;
	}

	if (vertices)
	{
		delete[] vertices;
		vertices = nullptr;
	}

	if (vertices3t)
	{
		delete[] vertices3t;
		vertices3t = nullptr;
	}

	if (verticesTangent)
	{
		delete[] verticesTangent;
		verticesTangent = nullptr;
	}

	if (skinsIndices)
	{
		delete[] skinsIndices;
		skinsIndices = nullptr;
	}

	if (indicesPerMaterialNumbers)
	{
		delete[] indicesPerMaterialNumbers;
		indicesPerMaterialNumbers = nullptr;
	}

	if (buffer3t)
	{
		delete[] buffer3t;
		buffer3t = nullptr;
	}

	if (staticBuffers)
	{
		delete[] staticBuffers;
		staticBuffers = nullptr;
	}

	if (buffersTangent)
	{
		delete[] buffersTangent;
		buffersTangent = nullptr;
	}

	isReady = false;
}

int sortTriangles(
    const GsTriangle* arg1,
    const GsTriangle* arg2)
{
    if(arg1->materialIndex > arg2->materialIndex)
        return 1;

    if(arg1->materialIndex < arg2->materialIndex)
        return -1;

    return 0;
}

int instr(
    const char* line,
    const char* subline)
{
    char a, c;
    int start;
    int lineLength = strlen(line);
    int sublineLength = strlen(subline);

    if((sublineLength <= 1) || (sublineLength > lineLength))
        return -1;

    memcpy(&a, &subline[0], sizeof(char));

    start = strcspn(line, &a);

    while(start < lineLength)
    {
        if(line[start] != a)
        {
            start++;
            continue;
        }

        int j;

        for(j = 1; j < sublineLength; j++)
        {
            memcpy(&c, &subline[j], sizeof(char));

            if(line[start + j] != c)
                break;
        }

        if(j == sublineLength)
            return start;
        else
            start++;
    }

    return -1;
}