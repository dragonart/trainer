#pragma once


#include "../GsTools/gslog.h"
#include "../gs.h"
#include "gsvertex.h"
#include "gsgraphics.h"


class GsVertexCacheManager
{
    public:
        GsVertexCacheManager(
            GsLog& newLog) : log(newLog) {};
		virtual ~GsVertexCacheManager() {};

        virtual GsResult createStaticBuffer(
            GsVertexType vertexType,
            UINT skinIndex,
            UINT verticesNumber,
            UINT indicesNumber,
            const void* vertices,
            const WORD* indices,
            UINT* index) = 0;

        virtual GsResult createIndexBuffer(
            UINT indicesNumber,
            const WORD* indices,
            UINT* index) = 0;

        virtual GsResult render(
            GsVertexType vertexType,
            UINT verticesNumber,
            UINT indicesNumber,
            const void* vertices,
            const WORD* indices,
            UINT skinIndex) = 0;

        virtual GsResult renderNaked(
            UINT verticesNumber,
            const void* vertices,
            bool textured) = 0;

        virtual GsResult render(
            UINT staticBufferIndex) = 0;

        virtual GsResult render(
            UINT staticBufferIndex,
            UINT indexBufferIndex,
            UINT skinIndex) = 0;

        virtual GsResult render(
            UINT staticBufferIndex,
            UINT skinIndex,
            UINT startIndex,
            UINT verticesNumber,
            UINT trianglesNumber) = 0;

        virtual GsResult renderPoints(
            GsVertexType vertexType,
            UINT verticesNumber,
            const void* vertices,
            const GsColor* color) = 0;

        virtual GsResult renderLines(
            GsVertexType vertexType,
            UINT verticesNumber,
            const void* vertices,
            const GsColor* color,
            bool stripLine) = 0;

        virtual GsResult renderLine(
            const float* start,
            const float* end,
            const GsColor* color) = 0;

        virtual GsResult forceFlushAll() = 0;

        virtual GsResult forceFlush(
            GsVertexType vertexType) = 0;

        virtual void invalidateStates() = 0;

        virtual GsRenderState getShadeMode() = 0;

    protected:
        GsLog& log;
};