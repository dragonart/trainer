#pragma once


#include "gsskin.h"
#include "gsmaterial.h"
#include "gstexture.h"

#include "..\gs.h"
#include "..\GsTools\gslog.h"


class GsSkinManager
{
    public:
        GsSkinManager(
            GsLog& newLog) : log(newLog) {};
        virtual ~GsSkinManager() {};

        virtual void reset() = 0;

        virtual GsResult addSkin(
            const GsColor& ambient,
            const GsColor& diffuse,
            const GsColor& emissive,
            const GsColor& specular,
            float specularPower,
			UINT* skinIndex) = 0;
        virtual GsResult addTexture(
			UINT skinIndex,
            const char* name,
            bool alphaEnabled,
            float alpha,
            GsColor* colorKeys,
            DWORD colorKeysNumber) = 0;

        virtual GsResult addTextureHeightmapAsBump(
            UINT skinIndex,
            const char* name) = 0;

        virtual GsResult exchangeTexture(
            UINT skinIndex,
            UINT textureStage,
			const char* name,
            bool alphaEnabled,
            float alpha,
            GsColor* colorKeys,
            DWORD colorKeysNumber) = 0;

        virtual GsResult exchangeMaterial(
            UINT skinIndex,
            const GsColor& ambient,
            const GsColor& diffuse,
            const GsColor& emissive,
            const GsColor& specular,
            float specularPower) = 0;

        virtual bool materialsAreEqual(
            const GsMaterial& material1,
            const GsMaterial& material2) = 0;
        virtual bool colorsAreEqual(
            const GsColor& color1,
            const GsColor& color2) = 0;

        virtual GsSkin& getSkin(
            UINT index) = 0;
        virtual GsMaterial& getMaterial(
            UINT index) = 0;
        virtual GsTexture& getTexture(
            UINT index) = 0;

        virtual const char* getTextureName(
            UINT textureIndex,
            float* alpha,
            GsColor* alphaKeys,
            UCHAR* alphaKeysNumber) = 0;

        virtual UINT getSkinsNumber() = 0;

        virtual void logCurrentStatus(
            bool detail) = 0;

    protected:
        GsSkin* skins;
        GsMaterial* materials;
        GsTexture* textures;

		UINT skinsNumber;
		UINT materialsNumber;
		UINT texturesNumber;

        GsLog& log;
};