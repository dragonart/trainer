#pragma once


#include "../GsMathematics/gsmathematics.h"


struct GsViewPort
{
	DWORD x;
	DWORD y;
	DWORD width;
	DWORD height;
};