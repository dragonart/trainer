#pragma once


#include "../GsMathematics/gsmathematics.h"
#include "gscolor.h"

#include <array>
#include <wtypes.h>


enum GsVertexType
{
	GS_VERTEX__UNTRANSFORMED_POSITION_ONLY,
	GS_VERTEX__UNTRANSFORMED_UNLIT,
	GS_VERTEX__UNTRANSFORMED_LIT,
	GS_VERTEX__CHARACTER_ANIMATION,
	GS_VERTEX__THREE_TEXTURE_COORD_PAIRS,
	GS_VERTEX__UNTRANSFORMED_UNLIT_WITH_TANGENT_VECTOR
};


struct GsVertexPositionOnly
{
	float x, y, z;
};

struct GsVertexPositionNormalTexture
{
	float x, y, z;
	float normal[GS_DIMENSIONS_NUMBER];
	float tu, tv;
};

struct GsVertexPositionColorTexture
{
	float x, y, z;
	DWORD color;
	float tu, tv;
};

struct GsVertexPositionNormalTexture2Bones
{
	float x, y, z;
	float normal[GS_DIMENSIONS_NUMBER];
	float tu, tv;
	float bone1, weight1;
	float bone2, weight2;
};

struct GsVertexPositionNormal3Textures
{
	float x, y, z;
	float normal[GS_DIMENSIONS_NUMBER];
	float tu1, tv1;
	float tu2, tv2;
	float tu3, tv3;
};

struct GsVertexPositionNormalTextureSomething
{
	float x, y, z;
	float normal[GS_DIMENSIONS_NUMBER];
	float tu, tv;
	float u[GS_DIMENSIONS_NUMBER];
};