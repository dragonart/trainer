#pragma once


#include "gsrenderdevice.h"


struct GsTriangle
{
	WORD i0, i1, i2;
	WORD n0, n1, n2;
	UINT materialIndex;
};


typedef int(*comparingFunc)(
	const void* arg1, 
	const void* arg2);
int sortTriangles(
	const GsTriangle* arg1,
	const GsTriangle* arg2);


#define SEEK(str, key) while(instr(str, key) == -1) fgets(str, 100, file);
#define NEXT(str) fgets(str, 100, file);
int instr(
	const char* line,
	const char* subline);


class GSMATHEMATICS_EXPORT GsModel3D
{
public:
	GsModel3D(GsLog& log, GsRenderDevice* device);
	~GsModel3D();

	GsResult render(
		bool useStatic, 
		bool b3t, 
		bool tangentModel);
	GsResult loadModelFromFile(
		const std::string fileName);

private:
	GsLog& log;
	GsRenderDevice* device;

	UINT* skinsIndices;
	UINT skinsIndicesNumber;

	GsVertexPositionNormalTexture* vertices;
	UINT verticesNumber;

	GsVertexPositionNormal3Textures* vertices3t;
	UINT* buffer3t;

	GsVertexPositionNormalTextureSomething* verticesTangent;
	UINT* buffersTangent;

	WORD* indices;
	UINT indicesNumber;

	UINT* indicesPerMaterialNumbers;
	UINT* staticBuffers;

	FILE* file;

	bool isReady;

	GsResult loadFromObj(
		std::string fileName);
	GsResult loadFromS3d(
		std::string fileName);

	void release();
};