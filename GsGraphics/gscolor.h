#pragma once


const auto GS_BASIC_COLORS_NUMBER = 3;


struct GsColor
{
	union
	{
		struct
		{
			float red;
			float green;
			float blue;
			float alpha;
		};

		float components[GS_BASIC_COLORS_NUMBER + 1];
	};
};

/*
const GsColor GS_COLOR_BLACK = GsColor(0.0, 0.0, 0.0);
const GsColor GS_COLOR_WHITE = GsColor(1.0, 1.0, 1.0);
const GsColor GS_COLOR_RED = GsColor(1.0, 0.0, 0.0);
const GsColor GS_COLOR_GREEN = GsColor(0.0, 1.0, 0.0);
const GsColor GS_COLOR_BLUE = GsColor(0.0, 0.0, 1.0);
*/