#pragma once


#include "gscolor.h"
#include "../GsMathematics/gsvector.h"


enum GsLightType
{
	GS_LIGHT__DIRECTIONAL = 0,
	GS_LIGHT__POINT = 1,
	GS_LIGHT__SPOT = 2
};

struct GsLight
{
	GsLightType type;
	
	GsColor ambient;
	GsColor diffuse;
	GsColor specular;

	GsVector position;
	GsVector direction;

	float range;
	float theta;
	float phi;

	float attenuation0;
	float attenuation1;
};