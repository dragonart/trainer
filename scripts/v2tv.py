import re
import numpy

data = open('..\Win32\Debug\data\models\g3.s3d')

vertices = []
vertices_number = None
faces = []
faces_number = None

head = []
middle = []
tail = []


for line in data:
	head.append(line)
	if re.search('BEGIN_VERTICES', line):
		next_line = data.readline()
		head.append(next_line)
		vertices_number = int(re.search('\d+', next_line).group(0))
		break

for i in range(0, vertices_number):
	vertices.append(data.readline())

for line in data:
	middle.append(line);
	if re.search('BEGIN_FACES', line):
		next_line = data.readline()
		middle.append(next_line)
		faces_number = int(re.search('\d+', next_line).group(0))
		break;

for i in range(0, faces_number):
	line = data.readline()
	faces.append(line)
	tail.append(line)

for line in data:
	tail.append(line)

data.close()

print('vertices:', vertices_number)
print('faces:', faces_number)


processed_vertices = []
for vertex in vertices:
	processed_vertex_line = re.split('[^0-9-\.]', vertex)
	for element in processed_vertex_line:
		if element != '':
			processed_vertices.append(float(element))
processed_vertices = numpy.reshape(processed_vertices, (vertices_number, 5))
vertices = processed_vertices

processed_faces = []
for face in faces:
	processed_faces_line = re.split('[^\d]', face)
	for element in processed_faces_line:
		if element != '':
			processed_faces.append(int(element))
processed_faces = numpy.reshape(processed_faces, (faces_number, 4))
faces = processed_faces


point0 = numpy.zeros(3)
point1 = numpy.zeros(3)
point2 = numpy.zeros(3)
new_vertices = numpy.zeros((vertices_number, 8))
for i in range(0, faces_number):
	for j in range(0, 3):
		point0[j] = vertices[faces[i, 0], j]
		point1[j] = vertices[faces[i, 1], j]
		point2[j] = vertices[faces[i, 2], j]
	vector01 = point1 - point0;
	vector02 = point2 - point0;
	normal = numpy.cross(vector01, vector02)
	for k in range(0, 3):
		for j in range(0, 5):
			new_vertices[faces[i, k], j] = vertices[faces[i, k], j]
		for j in range(0, 3):
			new_vertices[faces[i, k], j + 5] = normal[j]





result = open('..\Win32\Debug\data\models\g3n.s3d', 'w')

for line in head:
	result.write(line)
for vertex in new_vertices:
	result.write('{:10.6f},{:10.6f},{:10.6f};{:10.6f},{:10.6f};{:10.6f},{:10.6f},{:10.6f};\n'.format(vertex[0], vertex[1], vertex[2], vertex[3], vertex[4], vertex[5], vertex[6], vertex[7]))
result.write('\n')

for line in middle: 
	result.write(line)
for line in tail:
	result.write(line)

result.close()