import re
import numpy

data = open('sphere.obj')


vertices = []
normals = []
textures = []
faces = []

for line in data:
	if len(line) == 0:
		continue
	words = re.split('[ \n]', line)
	words = [x for x in words if x != '']
	if words[0] == 'v':
		del words[0]
		words = [float(x) for x in words]
		vertices.append(words)
	elif words[0] == 'vt':
		del words[0]
		words = [float(x) for x in words]
		textures.append(words)
	elif words[0] == 'vn':
		del words[0]
		words = [float(x) for x in words]
		normals.append(words)
	elif words[0] == 'f':
		del words[0]
		faces.append(words)
# print(vertices)
# print(textures)
# print(normals)
# print(faces)

print('vertices:', len(vertices))
print('textures:', len(textures))
print('normals:', len(normals))
print('faces:', len(faces))


resulting_faces = []
resulting_vertices = []
index = 0
for face in faces:
	for vertex in face:
		new_vertex = []
		vertex_coords = re.split('/', vertex)
		# 0 - position, 1 - texture, 2 - normal
		# in .s3d: position; texture; normal
		vertex_coords = [int(x) for x in vertex_coords]
		for element in vertices[int(vertex_coords[0]) - 1]:
			new_vertex.append(element)
		for element in textures[int(vertex_coords[1]) - 1]:
			new_vertex.append(element)
		for element in normals[int(vertex_coords[2]) - 1]:
			new_vertex.append(element)
		resulting_vertices.append(new_vertex)
	new_face = []
	for i in range(0, 3):
		new_face.append(index)
		index = index + 1
	resulting_faces.append(new_face)

print('resulting vertices:', len(resulting_vertices))
print('resulting faces:', len(resulting_faces))
print(resulting_vertices[0])



result = open('sphere.s3d', 'w')

result.write('\n\nBEGIN_VERTICES {\n');
result.write('{:5d};\n'.format(len(resulting_vertices)))
for vertex in resulting_vertices:
	result.write('{:10.6f},{:10.6f},{:10.6f};{:10.6f},{:10.6f};{:10.6f},{:10.6f},{:10.6f};\n'.format(vertex[0], vertex[1], vertex[2], vertex[3], vertex[4], vertex[5], vertex[6], vertex[7]))
result.write('}\n\n')

result.write('BEGIN_FACES {\n')
result.write('{:5d};\n'.format(len(resulting_faces)))
for face in resulting_faces:
	result.write('{:5d}, {:5d}, {:5d}; 0\n'.format(face[0], face[1], face[2]))
result.write('}')