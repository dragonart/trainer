import numpy
from numpy.random import normal
import struct

file_template = open('template1024.bmp', 'rb')

data = []
for i in range(54):
	quant = file_template.read(1)
	if data == '':
		break
	data.append(quant)

file_template.close()

#filename = input('enter filename: ')

filename = 'noize1024.bmp'

file_noize = open(filename, 'wb')
for quant in data:
	file_noize.write(quant)


surface = []
for i in range(1024):
	line = []
	for j in range(1024):
		pixel = int(normal(loc = 230, scale = 50))
		if pixel > 255:
			pixel = 255
		if pixel < 30:
			pixel = 30
		line.append(pixel)
	surface.append(line)

#print(pixel)


dots_size = 3

new_surface = surface
# for i in range(dots_size, 256 - dots_size):
# 	for j in range(dots_size, 256 - dots_size):
# 		summ = 0
# 		for k in range(i - dots_size, i + dots_size):
# 			for l in range(j - dots_size, j + dots_size):
# 				summ = summ + surface[k][l]

# 		new_surface[i][j] = int(summ / dots_size ) % 256;	



for line in new_surface:
	for pixel in line:
		data = struct.pack('BBB', pixel, pixel, pixel)
		file_noize.write(data)

file_noize.close()