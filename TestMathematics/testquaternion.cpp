#include <SDKDDKVer.h>
#include "CppUnitTest.h"

#include "../GsMathematics/gsquaternion.h"
#include "../GsMathematics/gsmatrix.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace TestMathematics
{
    TEST_CLASS(Quaternion)
    {
    public:
        TEST_METHOD(quaternionDefaultCunstuctor)
        {
			GsQuaternion quaternion;

			Assert::IsTrue(arePracticallyEqual(quaternion.x, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.y, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.z, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.w, (float)1.0));
        }

        TEST_METHOD(quaternionConstructor)
        {
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0);

			Assert::IsTrue(arePracticallyEqual(quaternion.x, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.y, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.z, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.w, (float)4.0));
        }

        TEST_METHOD(quaternionOperatorArray)
        {
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0);

            Assert::IsTrue(arePracticallyEqual(quaternion[0], (float)1.0));
			Assert::IsTrue(arePracticallyEqual(quaternion[1], (float)2.0));
			Assert::IsTrue(arePracticallyEqual(quaternion[2], (float)3.0));
			Assert::IsTrue(arePracticallyEqual(quaternion[3], (float)4.0));
			Assert::IsTrue(arePracticallyEqual(quaternion[9], (float)1.0));
        }

		TEST_METHOD(quaternionCreateFromEuler)
		{
			Assert::Fail();
		}

		TEST_METHOD(quaternionNormalize)
		{
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0);

			quaternion.normalize();

			Assert::IsTrue(arePracticallyEqual(quaternion.getMagnitude(), (float)1.0));
		}

		TEST_METHOD(quaternionConjugate)
		{
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0);

			GsQuaternion result = GsQuaternion::conjugateTo(quaternion);

			Assert::IsTrue(arePracticallyEqual(result.x, (float)-1.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)-2.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)4.0));
		}

		TEST_METHOD(quaternionGetEulers)
		{
			Assert::Fail();
		}

		TEST_METHOD(quaternionGetMatrix)
		{
			GsQuaternion quaternion(GsVector(1.0, 0.0, 0.0), GS_PI2);

			GsVector vector(1.0, 2.0, 3.0);

			vector = quaternion.getMatrix() * vector;

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(quaternionGetMagnitude)
		{
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0);

			float magnitude = quaternion.getMagnitude();

			Assert::IsTrue(arePracticallyEqual(magnitude, (float)5.477225575051661134569697828008));
		}

		TEST_METHOD(quaternionOperatorPlus)
		{
			GsQuaternion a(1.0, 2.0, 3.0, 4.0), b(5.0, 6.0, 7.0, 8.0), result;

			result = a + b;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)6.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)8.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)10.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)12.0));
		}

		TEST_METHOD(quaternionOperatorPlusEqual)
		{
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0), result(5.0, 6.0, 7.0, 8.0);

			result += quaternion;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)6.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)8.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)10.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)12.0));
		}

		TEST_METHOD(quaternionOperatorMinus)
		{
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0) , result;

			result = -quaternion;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)-1.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)-2.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)-4.0));
		}

		TEST_METHOD(quaternionOperatorMinusQuaternion)
		{
			GsQuaternion a(1.0, 2.0, 3.0, 4.0), b(5.0, 6.0, 7.0, 8.0), result;

			result = a - b;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)-4.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)-4.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)-4.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)-4.0));
		}

		TEST_METHOD(quaternionOperatorMinusEqualQuaternion)
		{
			GsQuaternion vector(1.0, 2.0, 3.0, 4.0), result(5.0, 6.0, 7.0, 8.0);

			result -= vector;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)4.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)4.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)4.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)4.0));
		}

		TEST_METHOD(quaternionOperatorDivide)
		{
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0), result;

			result = quaternion / 2.0;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)0.5));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)1.5));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)2.0));
		}

		TEST_METHOD(quaternionOperatorDivideEqual)
		{
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0);

			quaternion /= 2.0;

			Assert::IsTrue(arePracticallyEqual(quaternion.x, (float)0.5));
			Assert::IsTrue(arePracticallyEqual(quaternion.y, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.z, (float)1.5));
			Assert::IsTrue(arePracticallyEqual(quaternion.w, (float)2.0));
		}

		TEST_METHOD(quaternionOperatorMultiplyScalar)
		{
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0), result;

			result = quaternion * 2.0;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)4.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)6.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)8.0));
		}

		TEST_METHOD(quaternionOperatorMultiplyEqualScalar)
		{
			GsQuaternion quaternion(1.0, 2.0, 3.0, 4.0);

			quaternion *= 2.0;

			Assert::IsTrue(arePracticallyEqual(quaternion.x, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.y, (float)4.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.z, (float)6.0));
			Assert::IsTrue(arePracticallyEqual(quaternion.w, (float)8.0));
		}

		TEST_METHOD(quaternionOperatorMultiplyVector)
		{
			Assert::Fail();
		}

		TEST_METHOD(quaternionOperatorMultiplyQuaternion)
		{
			Assert::Fail();
		}

		TEST_METHOD(quaternionOperatorMultiplyEqualQuaternion)
		{
			Assert::Fail();
		}
	};
}