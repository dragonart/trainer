#include <SDKDDKVer.h>
#include "CppUnitTest.h"

#include "../GsMathematics/gsmatrix.h"
#include "../GsMathematics/gsvector.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace TestMathematics
{
	TEST_CLASS(Matrix)
	{
	public:
		TEST_METHOD(matrixDefaultConstructor)
		{
			GsMatrix matrix;

			for (unsigned int i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
				for (unsigned int j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
					Assert::IsTrue(arePracticallyEqual(matrix[i][j], (float)0.0));
		}

		TEST_METHOD(matrixCalculateDeterminant)
		{
			GsMatrix matrix;


			matrix[0][0] = 1.0;
			matrix[0][1] = 6.0;
			matrix[0][2] = 3.0;
			matrix[0][3] = 0.0;
			matrix[1][0] = 5.0;
			matrix[1][1] = 3.0;
			matrix[1][2] = 2.0;
			matrix[1][3] = 2.0;
			matrix[2][0] = 9.0;
			matrix[2][1] = 7.0;
			matrix[2][2] = 1.0;
			matrix[2][3] = 4.0;
			matrix[3][0] = 4.0;
			matrix[3][1] = 5.0;
			matrix[3][2] = 2.0;
			matrix[3][3] = 6.0;

			float result = matrix.calculateDeterminant();


			Assert::IsTrue(arePracticallyEqual(result, (float)406.0));
		}
		
		TEST_METHOD(matrixGetTranslation)
		{
			GsMatrix matrix;

			
			matrix[3][0] = 1.0;
			matrix[3][1] = 2.0;
			matrix[3][2] = 3.0;

			GsVector vector = matrix.getTranslation();


			Assert::IsTrue(arePracticallyEqual(vector.x, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)3.0));
		}

		TEST_METHOD(matrixInverseOf)
		{
			GsMatrix matrix;

			matrix[0][0] = 1.0;
			matrix[0][1] = 7.0;
			matrix[0][2] = 4.0;
			matrix[0][3] = 0.0;
			matrix[1][0] = 9.0;
			matrix[1][1] = 4.0;
			matrix[1][2] = 8.0;
			matrix[1][3] = 8.0;
			matrix[2][0] = 2.0;
			matrix[2][1] = 4.0;
			matrix[2][2] = 5.0;
			matrix[2][3] = 5.0;
			matrix[3][0] = 1.0;
			matrix[3][1] = 7.0;
			matrix[3][2] = 1.0;
			matrix[3][3] = 1.0;


			GsMatrix result = GsMatrix::inverseOf(matrix);

			Assert::IsTrue(arePracticallyEqual(result[0][0], (float)0.0));
			Assert::IsTrue(arePracticallyEqual(result[0][1], (float)0.165775));
			Assert::IsTrue(arePracticallyEqual(result[0][2], (float)-0.278075));
			Assert::IsTrue(arePracticallyEqual(result[0][3], (float)0.0641711));
			Assert::IsTrue(arePracticallyEqual(result[1][0], (float)0.0));
			Assert::IsTrue(arePracticallyEqual(result[1][1], (float)-0.0160428));
			Assert::IsTrue(arePracticallyEqual(result[1][2], (float)-0.00534758));
			Assert::IsTrue(arePracticallyEqual(result[1][3], (float)0.15508));
			Assert::IsTrue(arePracticallyEqual(result[2][0], (float)0.25));
			Assert::IsTrue(arePracticallyEqual(result[2][1], (float)-0.013369));
			Assert::IsTrue(arePracticallyEqual(result[2][2], (float)0.078877));
			Assert::IsTrue(arePracticallyEqual(result[2][3], (float)-0.287433));
			Assert::IsTrue(arePracticallyEqual(result[3][0], (float)-0.25));
			Assert::IsTrue(arePracticallyEqual(result[3][1], (float)-0.040107));
			Assert::IsTrue(arePracticallyEqual(result[3][2], (float)0.236631));
			Assert::IsTrue(arePracticallyEqual(result[3][3], (float)0.137701));
		}

		TEST_METHOD(matrixSetIdentity)
		{
			GsMatrix matrix;

			matrix.setIdentity();

			for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
				for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
					if (i == j)
						Assert::IsTrue(arePracticallyEqual(matrix[i][j], (float)1.0));
					else
						Assert::IsTrue(arePracticallyEqual(matrix[i][j], (float)0.0));
		}

		TEST_METHOD(matrixSetRotation)
		{
			GsMatrix matrix;
			GsVector axis(1.0, 1.0, 0.0);

			matrix.setRotation(axis, GS_PI);

			GsVector vector(1.0, 0.0, 0.0);
			vector = matrix * vector;

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(matrixSetRotationX)
		{
			GsMatrix matrix;

			matrix.setRotationX(GS_PI2);

			GsVector vector(1.0, 2.0, 3.0, 1.0);
			vector = matrix * vector;

			
			Assert::IsTrue(arePracticallyEqual(vector.x, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(matrixSetRotationY)
		{
			GsMatrix matrix;

			matrix.setRotationY(GS_PI2);

			GsVector vector(1.0, 2.0, 3.0, 1.0);
			vector = matrix * vector;

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)-1.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(matrixSetRotationZ)
		{
			GsMatrix matrix;

			matrix.setRotationZ(GS_PI2);

			GsVector vector(1.0, 2.0, 3.0, 1.0);
			vector = matrix * vector;

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)-2.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(matrixSetTranslation)
		{
			GsMatrix matrix;
			GsVector translation(4.0, 5.0, 6.0);

			matrix.setTranslation(translation, GS_MATRIX_SET_TRANSLATION__ERASE_CONTENT);
			
			GsVector result(1.0, 2.0, 3.0);
			result = matrix * result;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)5.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)7.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)9.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(matrixTransposeOf)
		{
			GsMatrix matrix;


			for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
				for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
					matrix[i][j] = i * 10.0 + j;

			GsMatrix result = GsMatrix::transposeOf(matrix);


			for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
				for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
					Assert::IsTrue(arePracticallyEqual(matrix[i][j], result[j][i]));
		}

		TEST_METHOD(matrixOperatorMultiplyMatrix)
		{
			GsMatrix a, b, result;

			for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
				for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
				{
					a[i][j] = i * 10.0 + j;
					b[i][j] = (i + 4) * 10.0 + j;
				}

			result = a * b;

			Assert::IsTrue(arePracticallyEqual(result[0][0], (float)380.0));
			Assert::IsTrue(arePracticallyEqual(result[0][1], (float)386.0));
			Assert::IsTrue(arePracticallyEqual(result[0][2], (float)392.0));
			Assert::IsTrue(arePracticallyEqual(result[0][3], (float)398.0));
			Assert::IsTrue(arePracticallyEqual(result[1][0], (float)2580.0));
			Assert::IsTrue(arePracticallyEqual(result[1][1], (float)2626.0));
			Assert::IsTrue(arePracticallyEqual(result[1][2], (float)2672.0));
			Assert::IsTrue(arePracticallyEqual(result[1][3], (float)2718.0));
			Assert::IsTrue(arePracticallyEqual(result[2][0], (float)4780.0));
			Assert::IsTrue(arePracticallyEqual(result[2][1], (float)4866.0));
			Assert::IsTrue(arePracticallyEqual(result[2][2], (float)4952.0));
			Assert::IsTrue(arePracticallyEqual(result[2][3], (float)5038.0));
			Assert::IsTrue(arePracticallyEqual(result[3][0], (float)6980.0));
			Assert::IsTrue(arePracticallyEqual(result[3][1], (float)7106.0));
			Assert::IsTrue(arePracticallyEqual(result[3][2], (float)7232.0));
			Assert::IsTrue(arePracticallyEqual(result[3][3], (float)7358.0));
		}

		TEST_METHOD(matrixOperatorMultiplyVector)
		{
			GsMatrix matrix;

			for (auto i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
				for (auto j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
					matrix[i][j] = i * 10.0 + j;

			
			GsVector vector(1.0, 2.0, 3.0, 1.0);
			vector = matrix * vector;


			Assert::IsTrue(arePracticallyEqual(vector.x, (float)11.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)81.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)151.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)221.0));
		}
	};
}