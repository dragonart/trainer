#include <SDKDDKVer.h>
#include "CppUnitTest.h"

#include "../gsmathematics/gsvector.h"
#include "../GsMathematics/gsmatrix.h"
#include "../GsMathematics/gsquaternion.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace TestMathematics
{
    TEST_CLASS(Vector)
    {
    public:
		TEST_METHOD(vectorDefaultCunstuctor)
		{
			GsVector vector;

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(vectorConstructor)
		{
			GsVector a(1.0, 2.0, 3.0), b(1.0, 2.0, 3.0, 4.0);


			Assert::IsTrue(arePracticallyEqual(a.x, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(a.y, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(a.z, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(a.w, (float)1.0));

			Assert::IsTrue(arePracticallyEqual(b.x, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(b.y, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(b.z, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(b.w, (float)4.0));
		}

		TEST_METHOD(vectorOperatorArray)
		{
			GsVector vector(0.0, 1.0, 2.0, 3.0);

			for (auto i = 0; i < 4; i++)
				Assert::IsTrue(arePracticallyEqual(vector[i], (float)i));

			Assert::IsTrue(arePracticallyEqual(vector[40], (float)0.0));
		}

		TEST_METHOD(vectorCross)
		{
			GsVector a(1.0, 2.0, 3.0), b(4.0, 5.0, 6.0), result;

			result = GsVector::cross(a, b);

			Assert::IsTrue(arePracticallyEqual(result.x, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)6.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(vectorDifference)
		{
			GsVector a(3.0, 2.0, 1.0), b(4.0, 5.0, 6.0), result;

			result.difference(a, b);

			Assert::IsTrue(arePracticallyEqual(result.x, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)5.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}
		
		TEST_METHOD(vectorGetAngleWithVector)
		{
			GsVector a(1.0, 0.0, 0.0), b(0.0, 1.0, 0.0);

			float angle = a.getAngleWithVector(b);

			Assert::IsTrue(arePracticallyEqual(angle, GS_PI2));
		}

		TEST_METHOD(vectorGetLength)
		{
			GsVector vector(3.0, 4.0, 0.0);

			float length = vector.getLength();

			Assert::IsTrue(arePracticallyEqual(length, (float)5.0));
		}

		TEST_METHOD(vectorGetSquaredLength)
		{
			GsVector vector(3.0, 4.0, 0.0);

			float squaredLength = vector.getSquaredLength();

			Assert::IsTrue(arePracticallyEqual(squaredLength, (float)25.0));
		}

		TEST_METHOD(vectorIsUnit)
		{
			GsVector vector(1.0, 0.0, 0.0);
			Assert::IsTrue(vector.isUnit());

			vector.set(0.0, 1.0, 0.0);
			Assert::IsTrue(vector.isUnit());

			vector.set(0.0, 0.0, 1.0);
			Assert::IsTrue(vector.isUnit());
		}

		TEST_METHOD(vectorNegate)
		{
			GsVector vector(1.0, 2.0, 3.0);

			vector.negate();

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)-1.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)-2.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(vectorNormalize)
		{
			GsVector vector(1.0, 2.0, 3.0);

			vector.normalize();

			Assert::IsTrue(arePracticallyEqual(vector.getLength(), (float)1.0));
		}

		TEST_METHOD(vectorRotateInverseWithMatrix)
		{
			GsVector vector(1.0, 0.0, 0.0);
			GsMatrix matrixRotation;
			
			matrixRotation.setRotationZ(GS_PI2);
			vector.rotateInverseWithMatrix(matrixRotation);

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)-1.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(vectorRotateWithMatrix)
		{
			GsVector vector(1.0, 0.0, 0.0);
			GsMatrix matrixRotation;

			matrixRotation.setRotationZ(GS_PI2);
			vector.rotateWithMatrix(matrixRotation);

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(vectorSet)
        {
			GsVector vector;
			
			vector.set(1.0, 2.0, 3.0, 4.0);

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)4.0));
        }

		TEST_METHOD(vectorOperatorMinusVector)
		{
			GsVector a(1.0, 2.0, 3.0), b(4.0, 5.0, 6.0), result;

			result = a - b;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorMinusScalar)
		{
			GsVector vector(1.0, 2.0, 3.0);
			float scalar = 0.5;

			GsVector result = vector - scalar;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)0.5));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)1.5));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)2.5));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorMultiplyVector)
		{
			GsVector a(1.0, 2.0, 3.0), b(4.0, 5.0, 6.0);

			float result = a * b;

			Assert::IsTrue(arePracticallyEqual(result, (float)32));
		}

		TEST_METHOD(vectorOperatorMultiplyScalar)
		{
			GsVector vector(1.0, 2.0, 3.0);
			float multiplier = 3.5;

			GsVector result = vector * multiplier;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)3.5));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)7.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)10.5));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorMultiplyAssignScalar)
		{
			GsVector vector(1.0, 2.0, 3.0);
			float multiplier = 3.5;

			vector *= multiplier;

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)3.5));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)7.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)10.5));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorDivideScalar)
		{
			GsVector vector(1.0, 2.0, 3.0);
			float divider = 2.0;

			GsVector result = vector / divider;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)0.5));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)1.5));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorDivideAssignScalar)
		{
			GsVector vector(1.0, 2.0, 3.0);
			float divider = 2.0;

			vector /= divider;

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)0.5));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)1.5));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorPlusVector)
		{
			GsVector a(1.0, 2.0, 3.0), b(4.0, 5.0, 6.0);

			GsVector result = a + b;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)5.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)7.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)9.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorPlusScalar)
		{
			GsVector vector(1.0, 2.0, 3.0);
			float scalar = 1.5;

			GsVector result = vector + scalar;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)2.5));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)3.5));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)4.5));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorPlusAssignVector)
		{
			GsVector vector(1.0, 2.0, 3.0);
			GsVector result(4.0, 5.0, 6.0);

			result += vector;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)5.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)7.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)9.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorPlusAssignScalar)
		{
			GsVector vector(1.0, 2.0, 3.0);
			float scalar = 1.0;

			vector += scalar;

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)4.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorMinus)
		{
			GsVector vector(1.0, 2.0, 3.0, 4.0);
			GsVector result;

			result = -vector;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)-1.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)-2.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)-3.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)4.0));
		}

		TEST_METHOD(vectorOperatorMinusAssignVector)
		{
			GsVector vector(1.0, 2.0, 3.0);
			GsVector result(4.0, 5.0, 6.0);

			result -= vector;

			Assert::IsTrue(arePracticallyEqual(result.x, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(result.y, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(result.z, (float)3.0));
			Assert::IsTrue(arePracticallyEqual(result.w, (float)1.0));
		}

		TEST_METHOD(vectorOperatorMinusAssignScalar)
		{
			GsVector vector(1.0, 2.0, 3.0);
			float scalar = 1.0;

			vector -= scalar;

			Assert::IsTrue(arePracticallyEqual(vector.x, (float)0.0));
			Assert::IsTrue(arePracticallyEqual(vector.y, (float)1.0));
			Assert::IsTrue(arePracticallyEqual(vector.z, (float)2.0));
			Assert::IsTrue(arePracticallyEqual(vector.w, (float)1.0));
		}
    };
}