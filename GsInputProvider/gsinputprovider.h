#pragma once


#include "gsinputprovider_global.h"

#include "../GsInput/gsinputdevice.h"


class GSINPUTPROVIDER_EXPORT GsInputProvider
{
    public:
        GsInputProvider(
			GsLog& log);
        ~GsInputProvider();

		GsResult createDevice(
			std::string apiName);
		void releaseDevice();

		GsInputDevice* getDevice();


    private:
		GsInputDevice* device;

		GsLog& log;
};