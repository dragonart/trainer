#include "gsinputprovider.h"

#include "../GsDirectInput8/gsdirectinput8.h"


GsInputProvider::GsInputProvider(
    GsLog& newLog) : log(newLog)
{
    device = nullptr;
}

GsInputProvider::~GsInputProvider()
{
    releaseDevice();
}

GsResult GsInputProvider::createDevice(
    std::string apiName)
{
    if(apiName == "DirectInput 8")
    {
        device = new GsDirectInput8(log);

        log << "Input API '" + apiName + "' is chosen.";
    }
    else
    {
        std::string line = "API '" + apiName + "' is not supported.";

        log << "FAIL: " + line;
        log << apiName + " creation failed.";

        return GS_FAIL__API_NOT_SUPPORTED;
    }

    return GS_OK;
}

void GsInputProvider::releaseDevice()
{
    log << "Input release started.";

    if(device)
    {
        device->release();
        device = nullptr;
    }

    log << "Input release succeeded.";
}

GsInputDevice* GsInputProvider::getDevice()
{
    return device;
}