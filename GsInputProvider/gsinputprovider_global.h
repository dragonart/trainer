#ifndef GSINPUTPROVIDER_GLOBAL_H
#define GSINPUTPROVIDER_GLOBAL_H

#include <QtCore/qglobal.h>

#ifdef GSINPUTPROVIDER_LIB
# define GSINPUTPROVIDER_EXPORT Q_DECL_EXPORT
#else
# define GSINPUTPROVIDER_EXPORT Q_DECL_IMPORT
#endif

#endif // GSINPUTPROVIDER_GLOBAL_H
