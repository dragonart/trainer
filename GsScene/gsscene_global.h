#ifndef GSSCENE_GLOBAL_H
#define GSSCENE_GLOBAL_H

#include <QtCore/qglobal.h>

#ifdef GSSCENE_LIB
# define GSSCENE_EXPORT Q_DECL_EXPORT
#else
# define GSSCENE_EXPORT Q_DECL_IMPORT
#endif

#endif // GSSCENE_GLOBAL_H
