#include "gsscene.h"

GsScene::GsScene(GsLog& newLog, 
	GsRenderDevice* newRenderDevice) : log(newLog)
{
	renderDevice = newRenderDevice;
	model = nullptr;

	world.setIdentity();

	cameraRight = cameraUp = cameraPosition = cameraDirection = GsVector(0.0, 0.0, 0.0);
}

GsScene::~GsScene()
{
	if (model)
	{
		delete model;
		model = nullptr;
	}
}

GsResult GsScene::renderAll()
{
	if (failed(model->render(true, false, false)))
	{
		log << "Failed to render model.";
		return GS_FAIL;
	}

	return GS_OK;
}

GsResult GsScene::loadModel(
	std::string fileName)
{
	model = new GsModel3D(log, renderDevice);
	
	return  model->loadModelFromFile(fileName);
}

GsResult GsScene::setView(
	GsVector right,
	GsVector up,
	GsVector direction,
	GsVector position)
{
	cameraRight = right;
	cameraUp = up;
	cameraDirection = direction;
	cameraPosition = position;

	if (failed(renderDevice->setView3D(right, up, direction, position)))
		return GS_FAIL;

	return GS_OK;
}

GsResult GsScene::setProjectionMode(
	GsEngineMode mode)
{
	if (failed(renderDevice->setMode(mode)))
		return GS_FAIL;

	return GS_OK;
}

GsResult GsScene::setWorldTransform(
	GsMatrix newWorld)
{
	world = newWorld;

	renderDevice->setWorldTransform(&world);

	return GS_OK;
}

GsResult GsScene::rotateCamera(
	float angleX, 
	float angleY)
{
	GsMatrix rotationX, rotationY;
	rotationX.setRotation(cameraRight, angleX);
	rotationY.setRotation(cameraUp, angleY);

	cameraRotationMatrix = rotationY * rotationX;

	cameraRight = cameraRotationMatrix * cameraRight;
	cameraUp = cameraRotationMatrix * cameraUp;
	cameraPosition = cameraRotationMatrix * cameraPosition;
	cameraDirection = cameraRotationMatrix * cameraDirection;

	if (failed(renderDevice->setView3D(cameraRight, cameraUp, cameraDirection, cameraPosition)))
		return GS_FAIL;

	return GS_OK;
}

GsResult GsScene::scrollCamera(
	int direction)
{
	if (direction < 0)
		cameraPosition /= 0.95;
	else
		cameraPosition *= 0.95;

	return failed(renderDevice->setView3D(cameraRight, cameraUp, cameraDirection, cameraPosition));
}
