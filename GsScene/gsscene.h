#pragma once


#include "gsscene_global.h"

#include "..\GsTools\gslog.h"
#include "..\GsMathematics\gsvector.h"
#include "..\gs.h"
#include "..\GsGraphics\gsmodel3d.h"
#include "..\GsMathematics\gsmatrix.h"


class GSSCENE_EXPORT GsScene
{
    public:
		GsScene(
			GsLog& log, 
			GsRenderDevice* renderDevice);
        ~GsScene();

		GsResult renderAll();
		
		GsResult loadModel(
			std::string fileName);

		GsResult setView(
			GsVector right,
			GsVector up,
			GsVector direction,
			GsVector position);

		GsResult rotateCamera(
			float angleX,
			float angleY);

		GsResult scrollCamera(
			int direction);




		GsResult setProjectionMode(
			GsEngineMode mode);

		GsResult setWorldTransform(
			GsMatrix world);


    private:
		GsLog& log;

		GsRenderDevice* renderDevice;

		GsVector cameraRight, cameraUp, cameraDirection, cameraPosition;
		GsModel3D* model;

		GsMatrix world;

		
		GsMatrix cameraRotationMatrix;
};