float4x4 matrixWorldViewProjection;//: WORLDVIEWPROJECTIONMATRIX;
float4x4 matrixWorld;

struct VS_INPUT
{
    float4 position: POSITION;
    float3 normal: TEXCOORD0;
    float2 tex: NORMAL;
};

struct VS_OUTPUT
{
     float4 position: POSITION;
     float3 normal: TEXCOORD0;
     float2 tex: TEXCOORD1;
};

VS_OUTPUT main(VS_INPUT original)
{
    VS_OUTPUT result;
    
    
    result.position = mul(original.position, matrixWorldViewProjection);   
    result.tex = original.tex; 
    result.normal = normalize(mul(original.normal, matrixWorld));


    return result;
}