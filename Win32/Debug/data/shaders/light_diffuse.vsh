float4x4 matrixWorldViewProjection;  //Произведение мировой, видовой и проекционной матриц.
float4x4 matrixWorld; //Мировая матрица.
float4   lightPosition; //Позиция источника света

struct VS_INPUT //Входящие данные
{
    float4 position: POSITION;
    float3 normal:   NORMAL;
    float2 tex: TEXCOORD0;
};

struct VS_OUTPUT //Исходящие данные
{
     float4 position: POSITION;
     float2 tex: TEXCOORD0;
     float3 normal: TEXCOORD1;
     float3 light: TEXCOORD2;
};

VS_OUTPUT main(in VS_INPUT original)
{
    VS_OUTPUT result;
    //Вычисляем позицию вершины.
    result.position = mul(original.position, matrixWorldViewProjection);
    result.tex = original.tex;
    //Сохраняем позицию источника для передачи во фрагментный шейдер в виде
    //3D текстурных координат.
    result.light = lightPosition;
    //Рассчитываем нормаль поверхности и сохраняем для фрагментного шейдера.
    result.normal = normalize(mul(original.normal, matrixWorld));
    //*под словом “сохраняем” имеется ввиду посылание данных в растеризатор,
    //а только потом в вершинный шейдер.
    return result;
}