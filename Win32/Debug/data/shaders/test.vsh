float4x4 matrixWorldViewProjection;

struct VS_INPUT_STRUCT
{
    float4 position: POSITION;
    float3 normal: NORMAL;
};

struct VS_OUTPUT_STRUCT
{
     float4 position: POSITION;
     float3 normal: TEXCOORD1;
};

VS_OUTPUT_STRUCT main(VS_INPUT_STRUCT original)
{
    VS_OUTPUT_STRUCT result;
    
    result.position = mul(original.position, matrixWorldViewProjection);
    
    return result;
}