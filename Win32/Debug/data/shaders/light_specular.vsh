float4x4 matrixWorldViewProjection;      //Произведение мировой, видовой и проекционной матриц.
float4x4 matrixWorld;    //Мировая матрица.
float4   lightPosition;    //Позиция источника света
float3   viewVector; //Видовой вектор
float4   cameraPosition;      //Позиция наблюдателя

struct VS_INPUT //Входящие данные
{
    float4 position: POSITION;
    float3 normal: NORMAL;
    float2 tex: TEXCOORD0;
};

struct VS_OUTPUT //Исходящие данные
{
    float4 position:  POSITION;
    float2 tex: TEXCOORD0;
    float3 normal: TEXCOORD1;
    float3 light: TEXCOORD2;
    float3 view: TEXCOORD3;
};

VS_OUTPUT main(VS_INPUT original)
{
    VS_OUTPUT result;
    //Трансформируем позицию вершины
    result.position = mul(original.position, matrixWorldViewProjection); 

    result.tex = original.tex;   
    
    //Рассчитываем нормаль поверхности и сохраняем для пиксельного шейдера.
    result.normal = normalize(mul(original.normal, matrixWorld));  
    //Сохраняем позицию источника для передачи в пиксельный шейдер в виде
    //3D текстурных координат.
    result.light = lightPosition; 
    
    //Вычисляем видовой вектор и сохраняем для пиксельного шейдера.
    result.view = cameraPosition - viewVector;

    return result;
}