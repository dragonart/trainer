﻿#include "gsdirectinput8.h"


GsDirectInput8::GsDirectInput8(
    GsLog& log) : GsInputDevice(log)
{
    device = nullptr;
    mouse = nullptr;
}

GsDirectInput8::~GsDirectInput8()
{
    release();
}


void GsDirectInput8::release()
{
    log << "DirectInput 8 release started.";

    if(mouse)
    {
        mouse->Unacquire();
        mouse->Release();
        mouse = nullptr;
        log << "Mouse release succeeded.";
    }

    if(device)
    {
        device->Release();
        device = nullptr;
    }

    log << "DirectInput 8 release succeeded.";
}

bool GsDirectInput8::mouseMoved(
    GsMousePositionDelta* delta)
{
    DIMOUSESTATE mouseStateCurrent;

    if(FAILED(mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&mouseStateCurrent)))
    {
        log << "Failed to get mouse state.";
        return false;
    }

    GsMousePositionDelta calculatedDelta;
    calculatedDelta.deltaX = mouseStateCurrent.lX - mouseState.lX;
    calculatedDelta.deltaY = mouseStateCurrent.lY - mouseState.lY;

    if((calculatedDelta.deltaX == 0) && (calculatedDelta.deltaY == 0))
    {
        delta = nullptr;
        return false;
    }

    if(delta)
        *delta = calculatedDelta;

    return true;
}

bool GsDirectInput8::mouseClicked(
    GsMouseButtonType button)
{
    DIMOUSESTATE mouseStateCurrent;

    if(FAILED(mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&mouseStateCurrent)))
    {
        log << "Failed to get mouse state.";
        return false;
    }

    bool result = !buttonIsDown(mouseState, button) && buttonIsDown(mouseStateCurrent, button);

    mouseState = mouseStateCurrent;

    return result;
}


bool GsDirectInput8::mouseUnClicked(
    GsMouseButtonType button)
{
    DIMOUSESTATE mouseStateCurrent;

    if(FAILED(mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&mouseStateCurrent)))
    {
        log << "Failed to get mouse state.";
        return false;
    }

    bool result = buttonIsDown(mouseState, button) && !buttonIsDown(mouseStateCurrent, button);

    mouseState = mouseStateCurrent;

    return result;
}


bool GsDirectInput8::mouseDown(
    GsMouseButtonType button)
{
    DIMOUSESTATE mouseStateCurrent;

    if(FAILED(mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&mouseStateCurrent)))
    {
        log << "Failed to get mouse state.";
        return false;
    }

    mouseState = mouseStateCurrent;

    return buttonIsDown(mouseStateCurrent, button);
}

bool GsDirectInput8::mouseUp(
    GsMouseButtonType button)
{
    DIMOUSESTATE mouseStateCurrent;

    if(FAILED(mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&mouseStateCurrent)))
    {
        log << "Failed to get mouse state.";
        return false;
    }

    mouseState = mouseStateCurrent;

    return !buttonIsDown(mouseStateCurrent, button);
}

GsResult GsDirectInput8::init(
    const HINSTANCE instance,
    const HWND window)
{
    if(FAILED(DirectInput8Create(instance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&device, nullptr)))
    {
        log << "DirectInput 8 device creation failed.";
        return GS_FAIL;
    }

    log << "DirectInput 8 device creation succeeded.";

    //      // keyboard
    //      if ((*directInput)->CreateDevice(GUID_SysKeyboard, keyboard, NULL) < 0)
    //          return false;
    //      if (FAILED((*keyboard)->SetDataFormat(&c_dfDIKeyboard)))
    //          return false;
    //      if (FAILED((*keyboard)->SetCooperativeLevel(*windowHandle, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
    //          return false;
    //      if (FAILED((*keyboard)->Acquire()))
    //          return false;

    if(FAILED(device->CreateDevice(GUID_SysMouse, &mouse, NULL)))
    {
        log << "Mouse device creation failed.";
        return GS_FAIL;
    }

    log << "Mouse device creation succeeded.";

    if(FAILED(mouse->SetDataFormat(&c_dfDIMouse)))
    {
        log << "Mouse data format setting failed.";
        return GS_FAIL;
    }

    log << "Mouse data format setting succeeded.";

    if(FAILED(mouse->SetCooperativeLevel(window, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
    {
        log << "Mouse cooperation level setting failed.";
        return GS_FAIL;
    }

    log << "Mouse cooperation level setting succeeded.";

    if(FAILED(mouse->Acquire()))
    {
        log << "Mouse capture failed.";
        return GS_FAIL;
    }

    log << "Mouse capture succeeded.";

    if(FAILED(mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&mouseState)))
    {
        log << "Mouse initial state getting failed.";
        return GS_FAIL;
    }

    log << "Mouse initial state getting succeeded.";

    return GS_OK;
}

bool GsDirectInput8::buttonIsDown(
    const DIMOUSESTATE mouseState,
    const GsMouseButtonType button) const
{
    return (mouseState.rgbButtons[button] & 0x80);
}

GsMousePosition GsDirectInput8::getMousePosition()
{
    GsMousePosition result;

    DIMOUSESTATE mouseStateCurrent;

    if(FAILED(mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&mouseStateCurrent)))
    {
        log << "Failed to get mouse state.";
        result.x = -1;
        result.y = -1;
        return result;
    }

    mouseState = mouseStateCurrent;

    result.x = mouseStateCurrent.lX;
    result.y = mouseStateCurrent.lY;
	log << std::to_string(result.x) + "; " + std::to_string(result.y);

    return result;
}

bool GsDirectInput8::mouseScrolled(
	int* delta)
{
	DIMOUSESTATE mouseStateCurrent;

	if (FAILED(mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&mouseStateCurrent)))
	{
		log << "Failed to get mouse state.";
		return false;
	}

	bool result = false;
	if (mouseState.lZ - mouseStateCurrent.lZ)
		result = true;

	if (delta)
		*delta = mouseState.lZ - mouseStateCurrent.lZ;

	mouseState = mouseStateCurrent;

	return result;
}
