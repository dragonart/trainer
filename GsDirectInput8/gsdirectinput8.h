#pragma once


#include "gsdirectinput8_global.h"

#include "../GsInput/gsinputdevice.h"

#include <dinput.h>


class GSDIRECTINPUT8_EXPORT GsDirectInput8: public GsInputDevice
{
    public:
        GsDirectInput8(
            GsLog& log);
        ~GsDirectInput8();

		GsResult init(
			const HINSTANCE instance,
			const HWND window);
        void release();

        bool mouseMoved(
            GsMousePositionDelta* delta);
		GsMousePosition getMousePosition();

        bool mouseClicked(
            GsMouseButtonType button);
		bool mouseUnClicked(
			GsMouseButtonType button);
        
		bool mouseDown(
            GsMouseButtonType button);
        bool mouseUp(
            GsMouseButtonType button);

		bool mouseScrolled(
			int* delta);


    private:
		LPDIRECTINPUT8 device;
		LPDIRECTINPUTDEVICE8 mouse;

		DIMOUSESTATE mouseState;

		bool buttonIsDown(
			const DIMOUSESTATE mouseState,
			const GsMouseButtonType button) const;
};