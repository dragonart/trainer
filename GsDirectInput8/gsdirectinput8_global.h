#ifndef GSDIRECTINPUT8_GLOBAL_H
#define GSDIRECTINPUT8_GLOBAL_H

#include <QtCore/qglobal.h>

#ifdef GSDIRECTINPUT8_LIB
# define GSDIRECTINPUT8_EXPORT Q_DECL_EXPORT
#else
# define GSDIRECTINPUT8_EXPORT Q_DECL_IMPORT
#endif

#endif // GSDIRECTINPUT8_GLOBAL_H
