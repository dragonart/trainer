#pragma once


#include <d3d9.h>
#include "gsdirect3d9skinmanager.h"
#include "gsdirect3d9vertexcachemanager.h"


class GsDirect3D9VertexCacheManager;


class GsDirect3D9VertexCache
{
public:
	GsDirect3D9VertexCache(
		GsLog& log,
		UINT verticesMaxNumber,
		UINT indicesMaxNumber,
		UINT stride,
		GsDirect3D9SkinManager& skinManager,
		LPDIRECT3DDEVICE9 device,
		GsDirect3D9VertexCacheManager& vertexManager,
		DWORD index, 
		DWORD vertexFormat);
	~GsDirect3D9VertexCache();

	GsResult flush(
		bool useShaders);

	GsResult add(
		UINT verticesNumber, 
		UINT indicesNumber,
		const void* vertices, 
		const WORD* indices,
		bool useShaders);

	void setSkin(
		UINT skinIndex,
		bool useShader);
	bool usesSkin(
		UINT skinIndex);
	bool isEmpty();

	int getVerticesNumber();


private:
	GsLog& log;

	LPDIRECT3DVERTEXBUFFER9 vertexBuffer;
	LPDIRECT3DINDEXBUFFER9 indexBuffer;
	LPDIRECT3DDEVICE9 device;

	GsDirect3D9SkinManager& skinManager;
	GsDirect3D9VertexCacheManager& vertexCacheManager;
	GsSkin currentSkin;
	UINT currentSkinIndex;
	DWORD index;
	DWORD vertexFormat;

	UINT verticesMaxNumber;
	UINT indicesMaxNumber;
	UINT verticesNumber;
	UINT indicesNumber;
	UINT stride;
};