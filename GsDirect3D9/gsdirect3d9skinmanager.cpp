#include "gsdirect3d9skinmanager.h"
#include <d3dx9tex.h>


#define RGB16BIT(r, g, b) ((b % 32) + ((g % 64) << 6) + ((r % 32) << 11))


GsDirect3D9SkinManager::GsDirect3D9SkinManager(
    GsLog& newLog,
    LPDIRECT3DDEVICE9 newDevice) : GsSkinManager(newLog)
{
    device = newDevice;
    materialsNumber = 0;
    texturesNumber = 0;
    skinsNumber = 0;
    materials = nullptr;
    textures = nullptr;
    skins = nullptr;
}

GsDirect3D9SkinManager::~GsDirect3D9SkinManager()
{
    if(textures)
    {
        for(UINT i = 0; i < texturesNumber; i++)
        {
            if(textures[i].data)
            {
                ((LPDIRECT3DTEXTURE9)textures[i].data)->Release();
                textures[i].data = nullptr;
            }

            if(arePracticallyEqual(textures[i].alpha, (float)0.0))
                if(textures[i].colorKeys)
                {
                    delete []textures[i].colorKeys;
                    textures[i].colorKeys = nullptr;
                }

            if(textures[i].name)
            {
                delete []textures[i].name;
                textures[i].name = nullptr;
            }
        }

        free(textures);
        textures = nullptr;
    }

    if(materials)
    {
        free(materials);
        materials = nullptr;
    }

    if(skins)
    {
        free(skins);
        skins = nullptr;
    }

    log << "Skin manager is destroyed successfully.";
}

inline bool GsDirect3D9SkinManager::colorsAreEqual(
    const GsColor& color1,
    const GsColor& color2)
{
    if(color1.red != color2.red)
        return false;

    if(color1.green != color2.green)
        return false;

    if(color1.blue != color2.blue)
        return false;

    if(color1.alpha != color2.alpha)
        return false;

    return true;
}

bool GsDirect3D9SkinManager::materialsAreEqual(
    const GsMaterial& material1,
    const GsMaterial& material2)
{
    if(!colorsAreEqual(material1.ambient, material2.ambient))
        return false;

    if(!colorsAreEqual(material1.diffuse, material2.diffuse))
        return false;

    if(!colorsAreEqual(material1.emissive, material2.emissive))
        return false;

    if(!colorsAreEqual(material1.specular, material2.specular))
        return false;

    if(material1.power != material2.power)
        return false;

    return true;
}

GsResult GsDirect3D9SkinManager::addSkin(
    const GsColor& ambient,
    const GsColor& diffuse,
	const GsColor& emissive,
    const GsColor& specular,
    float power,
    UINT* index)
{
    if(skinsNumber % 50 == 0)
    {
        UINT newSkinsAllocatedNumber = (skinsNumber + 50) * sizeof(GsSkin);
        skins = (GsSkin*)realloc(skins, newSkinsAllocatedNumber);

        if(!skins)
            return GS_FAIL__OUT_OF_MEMORY;
    }

    GsMaterial newMaterial;
    newMaterial.ambient = ambient;
	newMaterial.diffuse = diffuse;
	newMaterial.emissive = emissive;
    newMaterial.specular = specular;
    newMaterial.power = power;

    bool materialFound = false;
    UINT materialIndex;

    for(materialIndex = 0; materialIndex < materialsNumber; materialIndex++)
        if(materialsAreEqual(materials[materialIndex], newMaterial))
        {
            materialFound = true;
            break;
        }

    if(materialFound)
        skins[skinsNumber].materialIndex = materialIndex;
    else
    {
        skins[skinsNumber].materialIndex = materialsNumber;

        if(materialsNumber % 50 == 0)
        {
            UINT newMaterialsAllocatedNumber = (materialsNumber + 50) * sizeof(GsMaterial);
            materials = (GsMaterial*)realloc(materials, newMaterialsAllocatedNumber);

            if(!materials)
                return GS_FAIL__OUT_OF_MEMORY;
        }

        memcpy(&materials[materialsNumber], &newMaterial, sizeof(GsMaterial));
        materialsNumber++;
    }

    skins[skinsNumber].alphaEnabled = false;

    for(int i = 0; i < GS_TEXTURES_PER_SKIN; i++)
        skins[skinsNumber].textureIndices[i] = GS_MAX_ID;

    if(index)
        *index = skinsNumber;

    skinsNumber++;
    return GS_OK;
}


GsResult GsDirect3D9SkinManager::addTexture(
    UINT skinIndex,
    const char* name,
    bool alphaEnabled,
    float alpha,
    GsColor* colorKeys,
    DWORD colorKeysNumber)
{
    if(skinIndex >= skinsNumber)
        return GS_FAIL__INVALID_SKIN_ID;

    if(skins[skinIndex].textureIndices[GS_TEXTURES_PER_SKIN - 1] != GS_MAX_ID)
    {
        log << "Error: addTexture() failed, all stages set.";
        return GS_FAIL;
    }

    bool textureFound = false;
    UINT textureIndex;

    for(textureIndex = 0; textureIndex < texturesNumber; textureIndex++)
        if(strcmp(name, textures[textureIndex].name) == 0)
        {
            textureFound = true;
            break;
        }

    if(!textureFound)
    {
        if(texturesNumber % 50 == 0)
        {
            UINT newTexturesAllocatedNumber = (texturesNumber + 50) * sizeof(GsTexture);
            textures = (GsTexture*)realloc(textures, newTexturesAllocatedNumber);

            if(!textures)
            {
                log << "Error: adding texture failed (realloc).";
                return GS_FAIL__OUT_OF_MEMORY;
            }
        }

        if(alphaEnabled)
            skins[skinIndex].alphaEnabled = true;
        else
            textures[texturesNumber].alpha = 1.0;

        textures[texturesNumber].name = new char[strlen(name) + 1];
        memcpy(textures[texturesNumber].name, name, strlen(name) + 1);

        if(failed(createTexture(textures[texturesNumber], alphaEnabled)))
        {
            log << "Error: createTexture() failed.";
            return GS_FAIL;
        }

        if(alphaEnabled)
        {
            GsTexture* newTexture = &textures[texturesNumber];
            newTexture->colorKeysNumber = colorKeysNumber;
            newTexture->colorKeys = new GsColor[colorKeysNumber];
            memcpy(newTexture->colorKeys, colorKeys, sizeof(GsColor)*newTexture->colorKeysNumber);
            LPDIRECT3DTEXTURE9 texture = (LPDIRECT3DTEXTURE9)newTexture->data;

            for(DWORD i = 0; i < colorKeysNumber; i++)
            {
                if(failed(setAlphaKey(texture, UCHAR(colorKeys[i].red * 255), UCHAR(colorKeys[i].green * 255), UCHAR(colorKeys[i].blue * 255), UCHAR(colorKeys[i].alpha * 255))))
                {
                    log << "Error: setAlphaKey() failed.";
                    return GS_FAIL;
                }
            }

            if(alpha < 1.0)
            {
                newTexture->alpha = alpha;

                if(failed(setTransparency(texture, UCHAR(alpha * 255))))
                {
                    log << "Error: setTransparency() failed.";
                    return GS_FAIL;
                }
            }
        }

        textureIndex = texturesNumber;
        texturesNumber++;
    }

    for(int i = 0; i < GS_TEXTURES_PER_SKIN; i++)
        if(skins[skinIndex].textureIndices[i] == GS_MAX_ID)
        {
            skins[skinIndex].textureIndices[i] = textureIndex;
            break;
        }

    return GS_OK;
}

GsResult GsDirect3D9SkinManager::createTexture(
    GsTexture& texture,
    bool alphaEnabled)
{
//     D3DLOCKED_RECT d3dRectangle;
//     D3DFORMAT format;
//     DIBSECTION dibs;
//     GsResult result;
//     int lineWidth;
//     void* memory = nullptr;
//     HBITMAP bmp = (HBITMAP)LoadImageA(nullptr, (LPCSTR)texture.name, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
// 
//     if(!bmp)
//     {
//         log << "Error: can't open texture '" + std::string(texture.name) + std::string("'.");
//         return GS_FAIL__FILE_NOT_FOUND;
//     }
// 
//     GetObject(bmp, sizeof(DIBSECTION), &dibs);
// 
//     if(dibs.dsBmih.biBitCount != 24)
//     {
//         DeleteObject(bmp);
//         log << "Error: texture is not 24-bit.";
//         return GS_FAIL__TEXTURE_FILE_NOT_24_BIT;
//     }
// 
//     if(alphaEnabled)
//         format = D3DFMT_A8R8G8B8;
//     else
//         format = D3DFMT_R5G6B5;
// 
//     long width = dibs.dsBmih.biWidth;
//     long height = dibs.dsBmih.biHeight;
//     BYTE* bmpBits = (BYTE*)dibs.dsBm.bmBits;
// 
//     if(failed(result = device->CreateTexture(width, height, 1, 0, format, D3DPOOL_MANAGED, (LPDIRECT3DTEXTURE9*)&texture.data, nullptr)))
//         return GS_FAIL__DIRECT3DDEVICE9_CREATE_TEXTURE;
// 
//     LPDIRECT3DTEXTURE9 textureData = (LPDIRECT3DTEXTURE9)texture.data;
// 
//     if(failed(textureData->LockRect(0, &d3dRectangle, nullptr, 0)))
//     {
//         log << "Error: can't lock texture to copy pixels.";
//         return GS_FAIL__DIRECT3DTEXTURE9_LOCK_RECT;
//     }
// 
//     if(alphaEnabled)
//     {
//         lineWidth = d3dRectangle.Pitch >> 2;
//         memory = (DWORD*)d3dRectangle.pBits;
//     }
//     else
//     {
//         lineWidth = d3dRectangle.Pitch >> 1;
//         memory = (USHORT*)d3dRectangle.pBits;
//     }
// 
//     for(int y = 0; y < height; y++)
//         for(int x = 0; x < width; x++)
//             if(alphaEnabled)
//             {
//                 DWORD color = 0xff000000;
//                 int i = (y * width + x) * 3;
//                 memcpy(&color, &bmpBits[i], sizeof(BYTE) * 3);
//                 ((DWORD*)memory)[x + y * lineWidth] = color;
//             }
//             else
//             {
//                 UCHAR B = (bmpBits[(x + y * width) * 3 + 0] >> 3);
//                 UCHAR G = (bmpBits[(x + y * width) * 3 + 1] >> 3);
//                 UCHAR R = (bmpBits[(x + y * width) * 3 + 2] >> 3);
//                 USHORT color = RGB16BIT((int)(((float)R / 255.0) * 32.0), (int)(((float)G / 255.0) * 64.0), (int)(((float)B / 255.0) * 32.0));
// 
//                 ((USHORT*)memory)[x + y * lineWidth] = color;
//             }
// 
//     textureData->UnlockRect(0);
//     DeleteObject(bmp);

//     return GS_OK;
	if (FAILED(D3DXCreateTextureFromFileA(device, texture.name, (LPDIRECT3DTEXTURE9*)&texture.data)))
	{
		log << "Failed to create texture from file.";
		return GS_FAIL;
	}

	log << "Alpha channel is not provided just now.";

	return GS_OK;
}


DWORD GsDirect3D9SkinManager::makeD3DColor(
    UCHAR R,
    UCHAR G,
    UCHAR B,
    UCHAR A)
{
    return (A << 24) | (R << 16) | (G << 8) | B;
}

GsResult GsDirect3D9SkinManager::setAlphaKey(
    LPDIRECT3DTEXTURE9& texture,
    UCHAR R,
    UCHAR G,
    UCHAR B,
    UCHAR A)
{
    D3DSURFACE_DESC d3dDesc;
    D3DLOCKED_RECT d3dRectangle;
    DWORD key, color;


    texture->GetLevelDesc(0, &d3dDesc);

    if(d3dDesc.Format != D3DFMT_A8R8G8B8)
        return GS_FAIL__INVALID_TEXTURE_FORMAT;

    key = makeD3DColor(R, G, B, 255);

    if(A > 0)
        color = makeD3DColor(R, G, B, A);
    else
        color = makeD3DColor(0, 0, 0, 0);

    if(FAILED(texture->LockRect(0, &d3dRectangle, nullptr, 0)))
        return GS_FAIL__DIRECT3DTEXTURE9_LOCK_RECT;

    for(DWORD y = 0; y < d3dDesc.Height; y++)
        for(DWORD x = 0; x < d3dDesc.Width; x++)
            if(((DWORD*)d3dRectangle.pBits)[x + d3dDesc.Width * y] = key)
                ((DWORD*)d3dRectangle.pBits)[x + d3dDesc.Width * y] = color;

    texture->UnlockRect(0);
    return GS_OK;
}

GsResult GsDirect3D9SkinManager::setTransparency(
    LPDIRECT3DTEXTURE9& texture,
    UCHAR alpha)
{
    D3DSURFACE_DESC d3dDesc;
    D3DLOCKED_RECT d3dRectangle;
    DWORD color;
    UCHAR A, R, G, B;


    texture->GetLevelDesc(0, &d3dDesc);

    if(d3dDesc.Format != D3DFMT_A8R8G8B8)
        return GS_FAIL__INVALID_TEXTURE_FORMAT;

    if(FAILED(texture->LockRect(0, &d3dRectangle, nullptr, 0)))
        return GS_FAIL__DIRECT3DTEXTURE9_LOCK_RECT;

    for(auto y = 0; y < d3dDesc.Height; y++)
        for(auto x = 0; x < d3dDesc.Width; x++)
        {
            color = ((DWORD*)d3dRectangle.pBits)[x + d3dDesc.Width * y];
            A = (UCHAR)((color & 0xff000000) >> 24);
            R = (UCHAR)((color & 0x00ff0000) >> 16);
            G = (UCHAR)((color & 0x0000ff00) >> 8);
            B = (UCHAR)((color & 0x000000ff) >> 0);

            if(A >= alpha)
                A = alpha;

            ((DWORD*)d3dRectangle.pBits)[x + d3dDesc.Width * y] = makeD3DColor(R, G, B, A);
        }

    texture->UnlockRect(0);

    return GS_OK;
}

void GsDirect3D9SkinManager::logCurrentStatus(
    bool detailed)
{
    log << "Skin manager contains: " + std::to_string(skinsNumber) + " skins, " + std::to_string(materialsNumber) + " materials, " + std::to_string(
            texturesNumber) + " textures.";

    if(detailed)
        log << "Detailed log for skin manager is not implemented.";
}

UINT GsDirect3D9SkinManager::getSkinsNumber()
{
    return skinsNumber;
}

void GsDirect3D9SkinManager::reset()
{
    for(UINT i = 0; i < texturesNumber; i++)
    {
        if(textures[i].data)
        {
            ((LPDIRECT3DTEXTURE9)textures[i].data)->Release();
            textures[i].data = nullptr;
        }

        if(arePracticallyEqual(textures[i].alpha, (float)0.0))
            if(textures[i].colorKeys)
            {
                delete []textures[i].colorKeys;
                textures[i].colorKeys = nullptr;
            }

        if(textures[i].name)
        {
            delete []textures[i].name;
            textures[i].name = nullptr;
        }
    }

    if(materials)
    {
        free(materials);
        materials = nullptr;
    }

    if(textures)
    {
        free(textures);
        textures = nullptr;
    }

    if(skins)
    {
        free(skins);
        skins = nullptr;
    }

    materialsNumber = 0;
    texturesNumber = 0;
    skinsNumber = 0;
    materials = nullptr;
    textures = nullptr;
    skins = nullptr;
    log << "Skin manager reset succeeded.";
}

GsSkin& GsDirect3D9SkinManager::getSkin(
    UINT index)
{
    if(index < skinsNumber)
        return skins[index];

    GsSkin emptySkin;
    return emptySkin;
}

GsMaterial& GsDirect3D9SkinManager::getMaterial(
    UINT index)
{
    if(index < materialsNumber)
        return materials[index];

    GsMaterial emptyMaterial;
    return emptyMaterial;
}


const char* GsDirect3D9SkinManager::getTextureName(
    UINT textureIndex,
    float* alpha,
    GsColor* alphaKeys,
    UCHAR* alphaKeysNumber)
{
    if(textureIndex >= texturesNumber)
        return nullptr;

    if(alpha)
        *alpha = textures[textureIndex].alpha;

    if(alphaKeysNumber)
        *alphaKeysNumber = textures[textureIndex].colorKeysNumber;

    if(textures[textureIndex].colorKeys && alphaKeys)
        memcpy(alphaKeys, textures[textureIndex].colorKeys, sizeof(GsColor) * textures[textureIndex].colorKeysNumber);

    return textures[textureIndex].name;
}

GsResult GsDirect3D9SkinManager::addTextureHeightmapAsBump(
    UINT skinIndex,
    const char* name)
{
    GsTexture* newTexture = nullptr;
    UINT textureIndex;
    bool textureFound = false;

    if(skinIndex >= skinsNumber)
        return GS_FAIL__INVALID_ID;

    if(skins[skinIndex].textureIndices[GS_TEXTURES_PER_SKIN - 1] != GS_MAX_ID)
    {
        log << "Error: addTexture() failed, all stages set.";
        return GS_FAIL;
    }

    for(textureIndex = 0; textureIndex < texturesNumber; textureIndex++)
        if(strcmp(name, textures[textureIndex].name) == 0)
        {
            textureFound = true;
            break;
        }

    if(!textureFound)
    {
        if(texturesNumber % 50 == 0)
        {
            UINT newTexturesAllocatedSize = (texturesNumber + 50) * sizeof(GsTexture);
            textures = (GsTexture*)realloc(textures, newTexturesAllocatedSize);

            if(!textures)
            {
                log << "Error: addTexture() failed with realloc().";
                return GS_FAIL__OUT_OF_MEMORY;
            }
        }

        textures[texturesNumber].alpha = 1.0;
        textures[texturesNumber].colorKeys = nullptr;
        textures[texturesNumber].name = new char[strlen(name) + 1];
        memcpy(textures[texturesNumber].name, name, strlen(name) + 1);

        if(failed(createTexture(textures[texturesNumber], true)))
        {
            log << "Error: createTexture() failed.";
            return GS_FAIL;
        }

        if(failed(convertToNormalMap(textures[texturesNumber])))
        {
            log << "Error: convertToNormalMap() failed.";
            return GS_FAIL;
        }

        textureIndex = texturesNumber;
        texturesNumber++;
    }

    for(int i = 0; i < GS_TEXTURES_PER_SKIN; i++)
        if(skins[skinIndex].textureIndices[i] == GS_MAX_ID)
        {
            skins[skinIndex].textureIndices[i] = textureIndex;
            break;
        }

    return GS_OK;
}

GsResult GsDirect3D9SkinManager::exchangeTexture(
    UINT skinIndex,
    UINT textureStage,
    const char* name,
    bool alphaEnabled,
    float alpha,
    GsColor* colorKeys,
    DWORD colorKeysNumber)
{
    if(skinIndex >= skinsNumber)
        return GS_FAIL__INVALID_ID;

    if(textureStage > GS_TEXTURES_PER_SKIN - 1)
        return GS_FAIL;

    if(!name)
    {
        skins[skinIndex].textureIndices[textureStage] = GS_MAX_ID;
        return GS_OK;
    }

    UINT textureIndex;
    bool textureFound;

    for(textureIndex = 0; textureIndex < texturesNumber; textureIndex++)
        if(strcmp(name, textures[textureIndex].name) == 0)
        {
            textureFound = true;
            break;
        }

    if(!textureFound)
    {
        if(texturesNumber % 50 == 0)
        {
            UINT newTexturesAllocatedSize = (texturesNumber + 50) * sizeof(GsTexture);
            textures = (GsTexture*)realloc(textures, newTexturesAllocatedSize);

            if(!textures)
                return GS_FAIL__OUT_OF_MEMORY;
        }

        if(alphaEnabled)
            skins[skinIndex].alphaEnabled = true;
        else
            textures[texturesNumber].alpha = 1.0;

        textures[texturesNumber].colorKeys = nullptr;
        textures[texturesNumber].name = new char[strlen(name) + 1];
        memcpy(textures[texturesNumber].name, name, strlen(name) + 1);

        if(failed(createTexture(textures[texturesNumber], alphaEnabled)))
        {
            log << "Error: createTexture() failed.";
            return GS_FAIL;
        }

        if(alphaEnabled)
        {
            GsTexture* newTexture = &textures[texturesNumber];
            newTexture->colorKeysNumber = colorKeysNumber;
            newTexture->colorKeys = new GsColor[colorKeysNumber];
            memcpy(newTexture->colorKeys, colorKeys, sizeof(GsColor) * newTexture->colorKeysNumber);
            LPDIRECT3DTEXTURE9 texture = (LPDIRECT3DTEXTURE9)newTexture->data;

            for(DWORD i = 0; i < colorKeysNumber; i++)
            {
                if(failed(setAlphaKey(texture, UCHAR(colorKeys[i].red * 255), UCHAR(colorKeys[i].green * 255), UCHAR(colorKeys[i].blue * 255), UCHAR(colorKeys[i].alpha * 255))))
                {
                    log << "Error: setAlphaKey() failed.";
                    return GS_FAIL;
                }
            }

            if(alpha < 1.0)
            {
                newTexture->alpha = alpha;

                if(failed(setTransparency(texture, UCHAR(alpha * 255))))
                {
                    log << "Error: setTransparency() failed.";
                    return GS_FAIL;
                }
            }
        }
        else
            alpha = textures[texturesNumber].alpha = 0.0;

        textureIndex = texturesNumber;
        texturesNumber++;
    }

    skins[skinIndex].textureIndices[textureStage] = textureIndex;
    return GS_OK;
}

GsResult GsDirect3D9SkinManager::exchangeMaterial(
    UINT skinIndex,
    const GsColor& ambient,
    const GsColor& diffuse,
    const GsColor& emissive,
    const GsColor& specular,
    float specularPower)
{
    if(skinIndex >= skinsNumber)
        return GS_FAIL__INVALID_ID;

    GsMaterial newMaterial;
    newMaterial.ambient = ambient;
    newMaterial.diffuse = diffuse;
    newMaterial.emissive = emissive;
    newMaterial.specular = specular;
    newMaterial.power = specularPower;

    UINT materialIndex;
    bool materialFound = false;

    for(materialIndex = 0; materialIndex < materialsNumber; materialIndex++)
        if(materialsAreEqual(newMaterial, materials[materialIndex]))
        {
            materialFound = true;
            break;
        }

    if(materialFound)
        skins[skinIndex].materialIndex = materialIndex;
    else
    {
        skins[skinIndex].materialIndex = materialsNumber;

        if(materialsNumber % 50 == 0)
        {
            UINT newMaterialsAllocatedSize = (materialsNumber + 50) * sizeof(GsMaterial);
            materials = (GsMaterial*)realloc(materials, newMaterialsAllocatedSize);

            if(!materials)
                return GS_FAIL__OUT_OF_MEMORY;
        }

        memcpy(&materials[materialsNumber], &newMaterial, sizeof(GsMaterial));
        materialsNumber++;
    }

    return GS_OK;
}

DWORD GsDirect3D9SkinManager::vectorToRGBA(
    GsVector& vector,
    float height)
{
    DWORD r = (DWORD)(127.0f * vector.x + 128.0f);
    DWORD g = (DWORD)(127.0f * vector.y + 128.0f);
    DWORD b = (DWORD)(127.0f * vector.z + 128.0f);
    DWORD a = (DWORD)(255.0f * height);

    return((a << 24L) + (r << 16L) + (g << 8L) + (b << 0L));
}


GsResult GsDirect3D9SkinManager::convertToNormalMap(
    GsTexture& texture)
{
    D3DLOCKED_RECT d3dRectangle;
    D3DSURFACE_DESC desc;

    LPDIRECT3DTEXTURE9 textureData = (LPDIRECT3DTEXTURE9)texture.data;

    textureData->GetLevelDesc(0, &desc);

    if(FAILED(textureData->LockRect(0, &d3dRectangle, nullptr, 0)))
    {
        log << "Error: can't lock texture to copy pixels.";
        return GS_FAIL;
    }

    DWORD* pixels = (DWORD*)d3dRectangle.pBits;

    for(DWORD j = 0; j < desc.Height; j++)
    {
        for(DWORD i = 0; i < desc.Width; i++)
        {
            DWORD color00 = pixels[0];
            DWORD color10 = pixels[1];
            DWORD color01 = pixels[d3dRectangle.Pitch / sizeof(DWORD)];

            float height00 = (float)((color00 & 0x00ff0000) >> 16) / 255.0;
            float height10 = (float)((color10 & 0x00ff0000) >> 16) / 255.0;
            float height01 = (float)((color01 & 0x00ff0000) >> 16) / 255.0;

            GsVector point00(i + 0.0, j + 0.0, height00);
            GsVector point10(i + 1.0, j + 0.0, height10);
            GsVector point01(i + 0.0, j + 1.0, height01);
            GsVector vector10 = point10 - point00;
            GsVector vector01 = point01 - point00;

            GsVector normal = GsVector::cross(vector10, vector01);
            normal.normalize();

            *pixels++ = vectorToRGBA(normal, height00);
        }
    }

    textureData->UnlockRect(0);

    LPDIRECT3DSURFACE9 surface = nullptr;
    textureData->GetSurfaceLevel(0, &surface);
    D3DXSaveSurfaceToFile((LPCWSTR)"normal.bmp", D3DXIFF_BMP, surface, nullptr, nullptr);

    return GS_OK;
}

GsTexture& GsDirect3D9SkinManager::getTexture(
    UINT index)
{
    if(index < texturesNumber)
        return textures[index];

    GsTexture emptyTexture;
    return emptyTexture;
}
