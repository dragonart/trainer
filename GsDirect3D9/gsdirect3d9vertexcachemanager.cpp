#include "gsdirect3d9vertexcachemanager.h"


GsDirect3D9VertexCacheManager::GsDirect3D9VertexCacheManager(
	GsLog& newLog, 
	GsDirect3D9SkinManager& newSkinManager, 
	LPDIRECT3DDEVICE9 newDevice, 
	GsDirect3D9& newDirect3D9, 
	UINT newVerticesMaxNumber, 
	UINT newIndicesMaxNumber) : GsVertexCacheManager(newLog), skinManager(newSkinManager), device(newDevice), direct3D9(newDirect3D9)
{
	DWORD id = 1;
	int i = 0;

	staticBuffers = nullptr;
	indexBuffers = nullptr;
	staticBuffersNumber = 0;
	indexBuffersNumber = 0;

	activeCacheIndex = GS_MAX_ID;
	activeStaticBufferIndex = GS_MAX_ID;
	activeIndexBufferIndex = GS_MAX_ID;

	for (i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
	{
		cachePositionOnly[i] = new GsDirect3D9VertexCache(newLog, newVerticesMaxNumber, newIndicesMaxNumber, sizeof(GsVertexPositionOnly), newSkinManager, newDevice, *this, id++, FVF_PVERTEX);
		cacheUntransformedUnlit[i] = new GsDirect3D9VertexCache(newLog, newVerticesMaxNumber, newIndicesMaxNumber, sizeof(GsVertexPositionNormalTexture), newSkinManager, newDevice, *this, id++, FVF_VERTEX);
		cacheUntransformedLit[i] = new GsDirect3D9VertexCache(newLog, newVerticesMaxNumber, newIndicesMaxNumber, sizeof(GsVertexPositionColorTexture), newSkinManager, newDevice, *this, id++, FVF_LVERTEX);
		cacheCharacterAnimation[i] = new GsDirect3D9VertexCache(newLog, newVerticesMaxNumber, newIndicesMaxNumber, sizeof(GsVertexPositionNormalTexture2Bones), newSkinManager, newDevice, *this, id++, FVF_CVERTEX);
		cacheThreeTextures[i] = new GsDirect3D9VertexCache(newLog, newVerticesMaxNumber, newIndicesMaxNumber, sizeof(GsVertexPositionNormal3Textures), newSkinManager, newDevice, *this, id++, FVF_T3VERTEX);
		cacheUntransformedUnlitWithTanget[i] = new GsDirect3D9VertexCache(newLog, newVerticesMaxNumber, newIndicesMaxNumber, sizeof(GsVertexPositionNormalTextureSomething), newSkinManager, newDevice, *this, id++, FVF_TVERTEX);
	}

	log << "Vertex cache manager constructed.";
}

GsDirect3D9VertexCacheManager::~GsDirect3D9VertexCacheManager()
{
	UINT n = 0;
	int i = 0;

	if (staticBuffers)
	{
		for (n = 0; n < staticBuffersNumber; n++)
		{
			if (staticBuffers[n].vertexBuffer)
			{
				staticBuffers[n].vertexBuffer->Release();
				staticBuffers[n].vertexBuffer = nullptr;
			}
			if (staticBuffers[n].indexBuffer)
			{
				staticBuffers[n].indexBuffer->Release();
				staticBuffers[n].indexBuffer = nullptr;
			}
		}
		free(staticBuffers);
		staticBuffers = nullptr;
	}

	for (n = 0; n < indexBuffersNumber; n++)
		if (indexBuffers[n].indexBuffer)
		{
			indexBuffers[n].indexBuffer->Release();
			indexBuffers[n].indexBuffer = nullptr;
		}

	if (indexBuffers)
	{
		free(indexBuffers);
		indexBuffers = nullptr;
	}

	for (i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
	{
		if (cachePositionOnly[i])
		{
			delete cachePositionOnly[i];
			cachePositionOnly[i] = nullptr;
		}

		if (cacheUntransformedUnlit[i])
		{
			delete cacheUntransformedUnlit[i];
			cacheUntransformedUnlit[i] = nullptr;
		}

		if (cacheUntransformedLit[i])
		{
			delete cacheUntransformedLit[i];
			cacheUntransformedLit[i] = nullptr;
		}

		if (cacheCharacterAnimation[i])
		{
			delete cacheCharacterAnimation[i];
			cacheCharacterAnimation[i] = nullptr;
		}

		if (cacheThreeTextures[i])
		{
			delete cacheThreeTextures[i];
			cacheThreeTextures[i] = nullptr;
		}

		if (cacheUntransformedUnlitWithTanget[i])
		{
			delete cacheUntransformedUnlitWithTanget[i];
			cacheUntransformedUnlitWithTanget[i] = nullptr;
		}
	}

	log << "Vertex cache manager' data is destroyed.";
}


GsDirect3D9& GsDirect3D9VertexCacheManager::getGsDirect3D9()
{
	return direct3D9;
}

GsResult GsDirect3D9VertexCacheManager::render(
	GsVertexType vertexType, 
	UINT verticesNumber, 
	UINT indicesNumber, 
	const void* vertices, 
	const WORD* indices, 
	UINT skinIndex)
{
	GsDirect3D9VertexCache** cache = nullptr;
	GsDirect3D9VertexCache* cacheEmpty = nullptr;
	GsDirect3D9VertexCache* cacheFullest = nullptr;
	int emptyCacheIndex = -1;
	int fullestCacheIndex = 0;

	bool useShaders = direct3D9.isUsingShaders();

	switch (vertexType)
	{
	case GS_VERTEX__UNTRANSFORMED_POSITION_ONLY:
		cache = cachePositionOnly;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT:
		cache = cacheUntransformedUnlit;
		break;

	case GS_VERTEX__UNTRANSFORMED_LIT:
		cache = cacheUntransformedLit;
		break;

	case GS_VERTEX__CHARACTER_ANIMATION:
		cache = cacheCharacterAnimation;
		break;

	case GS_VERTEX__THREE_TEXTURE_COORD_PAIRS:
		cache = cacheThreeTextures;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT_WITH_TANGENT_VECTOR:
		cache = cacheUntransformedUnlitWithTanget;
		break;

	default:
		return GS_FAIL__INVALID_ID;
	}

	cacheFullest = cache[0];

	activeStaticBufferIndex = GS_MAX_ID;

	for (int i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
	{
		if (cache[i]->usesSkin(skinIndex))
			return cache[i]->add(verticesNumber, indicesNumber, vertices, indices, useShaders);

		if (cache[i]->isEmpty())
			cacheEmpty = cache[i];

		if (cache[i]->getVerticesNumber() > cacheFullest->getVerticesNumber())
			cacheFullest = cache[i];
	}

	if (cacheEmpty)
	{
		cacheEmpty->setSkin(skinIndex, useShaders);
		return cacheEmpty->add(verticesNumber, indicesNumber, vertices, indices, useShaders);
	}

	cacheFullest->flush(useShaders);
	cacheFullest->setSkin(skinIndex, useShaders);
	return cacheFullest->add(verticesNumber, indicesNumber, vertices, indices, useShaders);
}

GsResult GsDirect3D9VertexCacheManager::render(
	UINT staticBufferIndex)
{
	HRESULT result;
	int iT = 0;

	GsRenderState shadeMode = direct3D9.getShadeMode();

	activeCacheIndex = GS_MAX_ID;

	if (staticBufferIndex >= staticBuffersNumber)
	{
		log << "Error: invalid static buffer index.";
		return GS_FAIL__INVALID_ID;
	}

	if(activeStaticBufferIndex != staticBufferIndex)
	{
		if (staticBuffers[staticBufferIndex].useIndices)
			device->SetIndices(staticBuffers[staticBufferIndex].indexBuffer);

		device->SetStreamSource(0, staticBuffers[staticBufferIndex].vertexBuffer, 0, staticBuffers[staticBufferIndex].stride);
		activeStaticBufferIndex = staticBufferIndex;
	}
	else
		if (activeIndexBufferIndex != GS_MAX_ID)
		{
			if (staticBuffers[staticBufferIndex].useIndices)
				device->SetIndices(staticBuffers[staticBufferIndex].indexBuffer);
			activeIndexBufferIndex = GS_MAX_ID;
		}

	if (direct3D9.getActiveSkinIndex() != staticBuffers[staticBufferIndex].skinIndex)
	{
		GsSkin* skin = &skinManager.getSkin(staticBuffers[staticBufferIndex].skinIndex);

		if (shadeMode == GS_RENDER_STATE__RENDER_SOLID_POLYGONS)
		{
			GsMaterial* material = &skinManager.getMaterial(skin->materialIndex);

			if (!direct3D9.isUsingShaders())
			{
				D3DMATERIAL9 mat =
				{
					material->diffuse.red, material->diffuse.green, material->diffuse.blue, material->diffuse.alpha,
					material->ambient.red, material->ambient.green, material->ambient.blue, material->ambient.alpha,
					material->specular.red, material->specular.green, material->specular.blue, material->specular.alpha,
					material->emissive.red, material->emissive.green, material->emissive.blue, material->emissive.alpha,
					material->power
				};

				device->SetMaterial(&mat);
			}
			else
			{
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorAmbient", (float*)&material->ambient, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorDiffuse", (float*)&material->diffuse, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorEmissive", (float*)&material->emissive, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorSpecular", (float*)&material->specular, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloatForShader(GS_SHADER__PIXEL, "powerSpecular", material->power, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 1, 1, &material->ambient);
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 2, 1, &material->diffuse);
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 3, 1, &material->emissive);
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 4, 1, &material->specular);
			}

			if (direct3D9.isUsingTextures())
			{
				for (iT = 0; iT < 8; iT++)
				{
					if (skin->textureIndices[iT] != GS_MAX_ID)
					{
						device->SetTexture(iT, (LPDIRECT3DTEXTURE9)skinManager.getTexture(skin->textureIndices[iT]).data);

						device->SetTextureStageState(iT, D3DTSS_TEXCOORDINDEX, 0);
						device->SetTextureStageState(iT, D3DTSS_COLORARG1, D3DTA_TEXTURE);
						device->SetTextureStageState(iT, D3DTSS_COLORARG2, D3DTA_CURRENT);
						device->SetTextureStageState(iT, D3DTSS_COLOROP, direct3D9.getTextureOperation(iT));
					}
					else
						break;
				}

				device->SetTextureStageState(iT, D3DTSS_COLOROP, D3DTOP_DISABLE);
			}
			else
			{
				device->SetTexture(0, nullptr);
				device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
			}
		}
		else
		{
			GsColor wireframeColor = direct3D9.getWireframeColor();

			D3DMATERIAL9 materialWire =
			{
				wireframeColor.red, wireframeColor.green, wireframeColor.blue, wireframeColor.alpha,
				wireframeColor.red, wireframeColor.green, wireframeColor.blue, wireframeColor.alpha,
				0.0, 0.0, 0.0, 1.0,
				0.0, 0.0, 0.0, 1.0,
				1.0
			};

			device->SetMaterial(&materialWire);

			device->SetTexture(0, nullptr);
			device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		}

		if (skin->alphaEnabled)
		{
			device->SetRenderState(D3DRS_ALPHAREF, 50);
			device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
			device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
			device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		}
		else
		{
			device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
			device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		}

		direct3D9.setActiveSkinIndex(staticBuffers[staticBufferIndex].skinIndex);
	}

	if (!direct3D9.isUsingShaders())
		device->SetFVF(staticBuffers[staticBufferIndex].vertexFormat);

	if (direct3D9.isUsingAdditiveBlending())
	{
		device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
		device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}

	if (!direct3D9.isUsingColorBuffer())
	{
		device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
		device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}

	if (staticBuffers[staticBufferIndex].useIndices)
	{
		switch (shadeMode)
		{
		case GS_RENDER_STATE__RENDER_JUST_VERTICES:
			result = device->DrawPrimitive(D3DPT_POINTLIST, 0, staticBuffers[staticBufferIndex].verticesNumber);
			break;

		case GS_RENDER_STATE__RENDER_TWO_VERTICES_AS_LINE:
			result = device->DrawIndexedPrimitive(D3DPT_LINELIST, 0, 0, staticBuffers[staticBufferIndex].verticesNumber, 0, staticBuffers[staticBufferIndex].indicesNumber / 2);
			break;

		case GS_RENDER_STATE__RENDER_POLYHULL_AS_POLYLINE:
			result = device->DrawIndexedPrimitive(D3DPT_LINESTRIP, 0, 0, staticBuffers[staticBufferIndex].verticesNumber, 0, staticBuffers[staticBufferIndex].verticesNumber);
			break;

		default:
			result = device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, staticBuffers[staticBufferIndex].verticesNumber, 0, staticBuffers[staticBufferIndex].trianglesNumber);
		}
	}
	else
	{
		switch (shadeMode)
		{
		case GS_RENDER_STATE__RENDER_JUST_VERTICES:
			result = device->DrawPrimitive(D3DPT_POINTLIST, 0, staticBuffers[staticBufferIndex].verticesNumber);
			break;

		case GS_RENDER_STATE__RENDER_TWO_VERTICES_AS_LINE:
			result = device->DrawPrimitive(D3DPT_LINELIST, 0, staticBuffers[staticBufferIndex].verticesNumber / 2);
			break;

		case GS_RENDER_STATE__RENDER_POLYHULL_AS_POLYLINE:
			result = device->DrawPrimitive(D3DPT_LINESTRIP, 0, staticBuffers[staticBufferIndex].verticesNumber);
			break;

		default:
			result = device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, staticBuffers[staticBufferIndex].trianglesNumber);
		}
	}

	if (FAILED(result))
		return GS_FAIL;
	
	return GS_OK;
}

GsResult GsDirect3D9VertexCacheManager::render(
	UINT staticBufferIndex,
	UINT indexBufferIndex,
	UINT skinIndex)
{
	int iT = 0;

	GsRenderState shadeMode = direct3D9.getShadeMode();

	activeCacheIndex = GS_MAX_ID;

	if ((staticBufferIndex >= staticBuffersNumber) || (indexBufferIndex >= indexBuffersNumber))
	{
		log << "Error: invalid buffer index.";
		return GS_FAIL__INVALID_ID;
	}

	if (activeStaticBufferIndex != staticBufferIndex)
	{
		device->SetStreamSource(0, staticBuffers[staticBufferIndex].vertexBuffer, 0, staticBuffers[staticBufferIndex].stride);
		activeStaticBufferIndex = staticBufferIndex;
	}

	if (activeIndexBufferIndex != indexBufferIndex)
	{
		device->SetIndices(indexBuffers[indexBufferIndex].indexBuffer);
		activeIndexBufferIndex = indexBufferIndex;
	}

	if (direct3D9.getActiveSkinIndex() != skinIndex)
	{
		GsSkin* skin = &skinManager.getSkin(skinIndex);

		if (shadeMode == GS_RENDER_STATE__RENDER_SOLID_POLYGONS)
		{
			GsMaterial* material = &skinManager.getMaterial(skin->materialIndex);

			if (!direct3D9.isUsingShaders())
			{
				D3DMATERIAL9 mat =
				{
					material->diffuse.red, material->diffuse.green, material->diffuse.blue, material->diffuse.alpha,
					material->ambient.red, material->ambient.green, material->ambient.blue, material->ambient.alpha,
					material->specular.red, material->specular.green, material->specular.blue, material->specular.alpha,
					material->emissive.red, material->emissive.green, material->emissive.blue, material->emissive.alpha,
					material->power
				};

				device->SetMaterial(&mat);
			}
			else
			{
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorAmbient", (float*)&material->ambient, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorDiffuse", (float*)&material->diffuse, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorEmissive", (float*)&material->emissive, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorSpecular", (float*)&material->specular, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloatForShader(GS_SHADER__PIXEL, "powerSpecular", material->power, getGsDirect3D9().getActivePixelShaderIndex());

// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 1, 1, &material->ambient);
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 2, 1, &material->diffuse);
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 3, 1, &material->emissive);
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 4, 1, &material->specular);
			}

			if (direct3D9.isUsingTextures())
			{
				for (iT = 0; iT < 8; iT++)
				{
					if (skin->textureIndices[iT] != GS_MAX_ID)
					{
						device->SetTexture(iT, (LPDIRECT3DTEXTURE9)skinManager.getTexture(skin->textureIndices[iT]).data);

						device->SetTextureStageState(iT, D3DTSS_TEXCOORDINDEX, 0);
						device->SetTextureStageState(iT, D3DTSS_COLORARG1, D3DTA_TEXTURE);
						device->SetTextureStageState(iT, D3DTSS_COLORARG2, D3DTA_CURRENT);
						device->SetTextureStageState(iT, D3DTSS_COLOROP, direct3D9.getTextureOperation(iT));
					}
					else 
						break;
				}
				device->SetTextureStageState(iT, D3DTSS_COLOROP, D3DTOP_DISABLE);
			}
			else
			{
				device->SetTexture(0, nullptr);
				device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
			}
		}
		else
		{
			GsColor wireframeColor = direct3D9.getWireframeColor();

			D3DMATERIAL9 materialWireframe =
			{
				wireframeColor.red, wireframeColor.green, wireframeColor.blue, wireframeColor.alpha,
				wireframeColor.red, wireframeColor.green, wireframeColor.blue, wireframeColor.alpha,
				0.0, 0.0, 0.0, 1.0,
				0.0, 0.0, 0.0, 1.0,
				1.0
			};

			device->SetMaterial(&materialWireframe);

			device->SetTexture(0, nullptr);
			device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		}

		if (skin->alphaEnabled)
		{
			device->SetRenderState(D3DRS_ALPHAREF, 50);
			device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
			device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
			device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		}
		else
		{
			device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
			device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		}

		direct3D9.setActiveSkinIndex(skinIndex);
	}

	if (!direct3D9.isUsingShaders())
		device->SetFVF(staticBuffers[staticBufferIndex].vertexFormat);

	if (direct3D9.isUsingAdditiveBlending())
	{
		device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
		device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}

	if (!direct3D9.isUsingColorBuffer())
	{
		device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
		device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}

	HRESULT result;
	switch (shadeMode)
	{
	case GS_RENDER_STATE__RENDER_JUST_VERTICES:
		result = device->DrawPrimitive(D3DPT_POINTLIST, 0, staticBuffers[staticBufferIndex].verticesNumber);
		break;

	case GS_RENDER_STATE__RENDER_TWO_VERTICES_AS_LINE:
		result = device->DrawIndexedPrimitive(D3DPT_LINELIST, 0, 0, staticBuffers[staticBufferIndex].verticesNumber, 0, indexBuffers[indexBufferIndex].indicesNumber / 2);
		break;

	case GS_RENDER_STATE__RENDER_POLYHULL_AS_POLYLINE:
		result = device->DrawIndexedPrimitive(D3DPT_LINESTRIP, 0, 0, staticBuffers[staticBufferIndex].verticesNumber, 0, staticBuffers[staticBufferIndex].verticesNumber);
		break;

	default:
		result = device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, staticBuffers[staticBufferIndex].verticesNumber, 0, indexBuffers[indexBufferIndex].trianglesNumber);
	}

	if (FAILED(result))
		return GS_FAIL;

	return GS_OK;
}

GsResult GsDirect3D9VertexCacheManager::render(
	UINT staticBufferIndex, 
	UINT skinIndex, 
	UINT startIndex, 
	UINT verticesNumber, 
	UINT trianglesNumber)
{
	int iT = 0;

	GsRenderState shadeMode = direct3D9.getShadeMode();

	activeCacheIndex = GS_MAX_ID;

	if (staticBufferIndex >= staticBuffersNumber)
	{
		log << "Error: invalid static buffer index.";
		return GS_FAIL__INVALID_ID;
	}

	if (activeStaticBufferIndex != staticBufferIndex)
	{
		if (staticBuffers[staticBufferIndex].useIndices)
			device->SetIndices(staticBuffers[staticBufferIndex].indexBuffer);

		device->SetStreamSource(0, staticBuffers[staticBufferIndex].vertexBuffer, 0, staticBuffers[staticBufferIndex].stride);
		activeStaticBufferIndex = staticBufferIndex;
	}
	else
		if (activeStaticBufferIndex != GS_MAX_ID)
		{
			if (staticBuffers[staticBufferIndex].useIndices)
				device->SetIndices(staticBuffers[staticBufferIndex].indexBuffer);
			activeIndexBufferIndex = GS_MAX_ID;
		}

	if (direct3D9.getActiveSkinIndex() != skinIndex)
	{
		GsSkin* skin = &skinManager.getSkin(skinIndex);

		if (shadeMode == GS_RENDER_STATE__RENDER_SOLID_POLYGONS)
		{
			GsMaterial* material = &skinManager.getMaterial(skin->materialIndex);

			if (!direct3D9.isUsingShaders())
			{
				D3DMATERIAL9 mat =
				{
					material->diffuse.red, material->diffuse.green, material->diffuse.blue, material->diffuse.alpha,
					material->ambient.red, material->ambient.green, material->ambient.blue, material->ambient.alpha,
					material->specular.red, material->specular.green, material->specular.blue, material->specular.alpha,
					material->emissive.red, material->emissive.green, material->emissive.blue, material->emissive.alpha,
					material->power
				};

				device->SetMaterial(&mat);
			}
			else
			{
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorAmbient", (float*)&material->ambient, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorDiffuse", (float*)&material->diffuse, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorEmissive", (float*)&material->emissive, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorSpecular", (float*)&material->specular, getGsDirect3D9().getActivePixelShaderIndex());
// 				getGsDirect3D9().registerConstantFloatForShader(GS_SHADER__PIXEL, "powerSpecular", material->power, getGsDirect3D9().getActivePixelShaderIndex());

// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 1, 1, &material->ambient);
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 2, 1, &material->diffuse);
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 3, 1, &material->emissive);
// 				getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 4, 1, &material->specular);
			}

			if (direct3D9.isUsingTextures())
			{
				for (iT = 0; iT < 8; iT++)
				{
					if (skin->textureIndices[iT] != GS_MAX_ID)
					{
						device->SetTexture(iT, (LPDIRECT3DTEXTURE9)skinManager.getTexture(skin->textureIndices[iT]).data);

						device->SetTextureStageState(iT, D3DTSS_TEXCOORDINDEX, 0);
						device->SetTextureStageState(iT, D3DTSS_COLORARG1, D3DTA_TEXTURE);
						device->SetTextureStageState(iT, D3DTSS_COLORARG2, D3DTA_CURRENT);
						device->SetTextureStageState(iT, D3DTSS_COLOROP, direct3D9.getTextureOperation(iT));
					}
					else
						break;
				}

				device->SetTextureStageState(iT, D3DTSS_COLOROP, D3DTOP_DISABLE);
			}
			else
			{
				device->SetTexture(0, nullptr);
				device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
			}
		}
		else
		{
			GsColor wireframeColor = direct3D9.getWireframeColor();

			D3DMATERIAL9 materialWireframe =
			{
				wireframeColor.red, wireframeColor.green, wireframeColor.blue, wireframeColor.alpha,
				wireframeColor.red, wireframeColor.green, wireframeColor.blue, wireframeColor.alpha,
				0.0, 0.0, 0.0, 1.0,
				0.0, 0.0, 0.0, 1.0,
				1.0
			};

			device->SetMaterial(&materialWireframe);

			device->SetTexture(0, nullptr);
			device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		}

		if (skin->alphaEnabled)
		{
			device->SetRenderState(D3DRS_ALPHAREF, 50);
			device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
			device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
			device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		}
		else
		{
			device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
			device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		}

		direct3D9.setActiveSkinIndex(skinIndex);
	}

	if (!direct3D9.isUsingShaders())
		device->SetFVF(staticBuffers[staticBufferIndex].vertexFormat);

	if (!staticBuffers[staticBufferIndex].useIndices)
		return GS_FAIL;

	if (direct3D9.isUsingAdditiveBlending())
	{
		device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
		device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}

	if (!direct3D9.isUsingColorBuffer())
	{
		device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
		device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}


	HRESULT result;
	switch (shadeMode)
	{
	case GS_RENDER_STATE__RENDER_JUST_VERTICES:
		result = device->DrawPrimitive(D3DPT_POINTLIST, 0, verticesNumber);
		break;

	case GS_RENDER_STATE__RENDER_TWO_VERTICES_AS_LINE:
		result = device->DrawIndexedPrimitive(D3DPT_LINELIST, 0, 0, verticesNumber, startIndex, trianglesNumber);
		break;

	case GS_RENDER_STATE__RENDER_POLYHULL_AS_POLYLINE:
		result = device->DrawIndexedPrimitive(D3DPT_LINESTRIP, 0, 0, verticesNumber, startIndex, trianglesNumber);
		break;
	
	default:
		result = device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, verticesNumber, startIndex, trianglesNumber);
	}

	if (FAILED(result))
		return GS_FAIL;

	return GS_OK;
}

GsResult GsDirect3D9VertexCacheManager::forceFlushAll()
{
	GsResult result = GS_OK;

	bool useShaders = direct3D9.isUsingShaders();
	int i;

	for (i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
		if (!cachePositionOnly[i]->isEmpty())
			if (failed(cachePositionOnly[i]->flush(useShaders)))
				result = GS_FAIL;

	for (i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
		if (!cacheUntransformedUnlit[i]->isEmpty())
			if (failed(cacheUntransformedUnlit[i]->flush(useShaders)))
				result = GS_FAIL;

	for (i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
		if (!cacheUntransformedLit[i]->isEmpty())
			if (failed(cacheUntransformedLit[i]->flush(useShaders)))
				result = GS_FAIL;

	for (i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
		if (!cacheCharacterAnimation[i]->isEmpty())
			if (failed(cacheCharacterAnimation[i]->flush(useShaders)))
				result = GS_FAIL;

	for (i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
		if (!cacheThreeTextures[i]->isEmpty())
			if (failed(cacheThreeTextures[i]->flush(useShaders)))
				result = GS_FAIL;

	for (i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
		if (!cacheUntransformedUnlitWithTanget[i]->isEmpty())
			if (failed(cacheUntransformedUnlitWithTanget[i]->flush(useShaders)))
				result = GS_FAIL;

	invalidateStates();

	return result;
}

GsResult GsDirect3D9VertexCacheManager::createStaticBuffer(
	GsVertexType vertexType, 
	UINT skinIndex, 
	UINT verticesNumber, 
	UINT indicesNumber, 
	const void* vertices, 
	const WORD* indices, 
	UINT* index)
{
	HRESULT result;
	DWORD actualVertexFormat;
	void* data;

	if (staticBuffersNumber >= GS_STATIC_BUFFERS_MAX_NUMBER)
		return GS_FAIL__OUT_OF_MEMORY;

	if ((staticBuffersNumber % 50) == 0)
	{
		int n = (staticBuffersNumber + 50) * sizeof(GsStaticBuffer);
		staticBuffers = (GsStaticBuffer*)realloc(staticBuffers, n);
		if (!staticBuffers)
			return GS_FAIL__OUT_OF_MEMORY;
	}

	staticBuffers[staticBuffersNumber].verticesNumber = verticesNumber;
	staticBuffers[staticBuffersNumber].indicesNumber = indicesNumber;
	staticBuffers[staticBuffersNumber].skinIndex = skinIndex;
	staticBuffers[staticBuffersNumber].indexBuffer = nullptr;
	staticBuffers[staticBuffersNumber].vertexBuffer = nullptr;

	switch (vertexType)
	{
	case GS_VERTEX__UNTRANSFORMED_POSITION_ONLY:
		staticBuffers[staticBuffersNumber].stride = sizeof(GsVertexPositionOnly);
		staticBuffers[staticBuffersNumber].vertexFormat = FVF_PVERTEX;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT:
		staticBuffers[staticBuffersNumber].stride = sizeof(GsVertexPositionNormalTexture);
		staticBuffers[staticBuffersNumber].vertexFormat = FVF_VERTEX;
		break;

	case GS_VERTEX__UNTRANSFORMED_LIT:
		staticBuffers[staticBuffersNumber].stride = sizeof(GsVertexPositionColorTexture);
		staticBuffers[staticBuffersNumber].vertexFormat = FVF_LVERTEX;
		break;

	case GS_VERTEX__CHARACTER_ANIMATION:
		staticBuffers[staticBuffersNumber].stride = sizeof(GsVertexPositionNormalTexture2Bones);
		staticBuffers[staticBuffersNumber].vertexFormat = FVF_CVERTEX;
		break;

	case GS_VERTEX__THREE_TEXTURE_COORD_PAIRS:
		staticBuffers[staticBuffersNumber].stride = sizeof(GsVertexPositionNormal3Textures);
		staticBuffers[staticBuffersNumber].vertexFormat = FVF_T3VERTEX;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT_WITH_TANGENT_VECTOR:
		staticBuffers[staticBuffersNumber].stride = sizeof(GsVertexPositionNormalTextureSomething);
		staticBuffers[staticBuffersNumber].vertexFormat = FVF_TVERTEX;
		break;

	default:
		return GS_FAIL__INVALID_ID;
	}

	if (indicesNumber > 0)
	{
		staticBuffers[staticBuffersNumber].useIndices = true;
		staticBuffers[staticBuffersNumber].trianglesNumber = (int)(indicesNumber / 3.0);

		result = device->CreateIndexBuffer(indicesNumber * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &staticBuffers[staticBuffersNumber].indexBuffer, nullptr);
		if (FAILED(result))
			return GS_FAIL__DIRECT3DDEVICE9__CREATE_INDEX_BUFFER;

		if (SUCCEEDED(staticBuffers[staticBuffersNumber].indexBuffer->Lock(0, 0, (void**)&data, 0)))
		{
			memcpy(data, indices, indicesNumber * sizeof(WORD));
			staticBuffers[staticBuffersNumber].indexBuffer->Unlock();
		}
		else
			return GS_FAIL__DIRECT3DINDEXBUFFER9__LOCK;
	}
	else
	{
		staticBuffers[staticBuffersNumber].useIndices = false;
		staticBuffers[staticBuffersNumber].trianglesNumber = (int)(verticesNumber / 3.0);
		staticBuffers[staticBuffersNumber].indexBuffer = nullptr;
	}

	if (direct3D9.isUsingShaders())
		actualVertexFormat = 0;
	else
		actualVertexFormat = staticBuffers[staticBuffersNumber].vertexFormat;

	result = device->CreateVertexBuffer(verticesNumber * staticBuffers[staticBuffersNumber].stride, D3DUSAGE_WRITEONLY, actualVertexFormat, D3DPOOL_DEFAULT, &staticBuffers[staticBuffersNumber].vertexBuffer, nullptr);
	if (FAILED(result))
		return GS_FAIL__DIRECT3DDEVICE9__CREATE_VERTEX_BUFFER;

	if (SUCCEEDED(staticBuffers[staticBuffersNumber].vertexBuffer->Lock(0, 0, (void**)&data, 0)))
	{
		memcpy(data, vertices, verticesNumber * staticBuffers[staticBuffersNumber].stride);
		staticBuffers[staticBuffersNumber].vertexBuffer->Unlock();
	}
	else
		return GS_FAIL__DIRECT3DVERTEXBUFFER9__LOCK;

	*index = staticBuffersNumber;

	staticBuffersNumber++;

	return GS_OK;
}

GsResult GsDirect3D9VertexCacheManager::forceFlush(
	GsVertexType vertexType)
{
	GsDirect3D9VertexCache** cache = nullptr;
	GsResult result = GS_OK;
	int i = 0;

	switch (vertexType)
	{
	case GS_VERTEX__UNTRANSFORMED_POSITION_ONLY:
		cache = cachePositionOnly;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT:
		cache = cacheUntransformedUnlit;
		break;

	case GS_VERTEX__UNTRANSFORMED_LIT:
		cache = cacheUntransformedLit;
		break;

	case GS_VERTEX__CHARACTER_ANIMATION:
		cache = cacheCharacterAnimation;
		break;

	case GS_VERTEX__THREE_TEXTURE_COORD_PAIRS:
		cache = cacheThreeTextures;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT_WITH_TANGENT_VECTOR:
		cache = cacheUntransformedUnlitWithTanget;
		break;

	default:
		return GS_FAIL__INVALID_ID;
	}

	for (i = 0; i < GS_VERTEX_CACHES_NUMBER; i++)
		if (failed(cache[i]->flush(direct3D9.isUsingShaders())))
			result = GS_FAIL;

	return result;
}

void GsDirect3D9VertexCacheManager::invalidateStates()
{
	direct3D9.setActiveSkinIndex(GS_MAX_ID);
	activeIndexBufferIndex = GS_MAX_ID;
	activeStaticBufferIndex = GS_MAX_ID;
}

GsResult GsDirect3D9VertexCacheManager::renderPoints(
	GsVertexType vertexType, 
	UINT verticesNumber, 
	const void* vertices, 
	const GsColor* color)
{
	D3DMATERIAL9 material;
	DWORD vertexFormat;
	int stride;

	invalidateStates();

	if (color)
	{
		memset(&material, 0, sizeof(D3DMATERIAL9));
		material.Diffuse.r = material.Ambient.r = color->red;
		material.Diffuse.g = material.Ambient.g = color->green;
		material.Diffuse.b = material.Ambient.b = color->blue;
		material.Diffuse.a = material.Ambient.a = color->alpha;
		device->SetMaterial(&material);
	}

	device->SetTexture(0, nullptr);

	switch (vertexType)
	{
	case GS_VERTEX__UNTRANSFORMED_POSITION_ONLY:
		stride = sizeof(GsVertexPositionOnly);
		device->SetRenderState(D3DRS_LIGHTING, FALSE);
		vertexFormat = FVF_PVERTEX;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT:
		stride = sizeof(GsVertexPositionNormalTexture);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		vertexFormat = FVF_VERTEX;
		break;

	case GS_VERTEX__UNTRANSFORMED_LIT:
		stride = sizeof(GsVertexPositionColorTexture);
		device->SetRenderState(D3DRS_LIGHTING, FALSE);
		vertexFormat = FVF_LVERTEX;
		break;

	case GS_VERTEX__CHARACTER_ANIMATION:
		stride = sizeof(GsVertexPositionNormalTexture2Bones);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		vertexFormat = FVF_CVERTEX;
		break;

	case GS_VERTEX__THREE_TEXTURE_COORD_PAIRS:
		stride = sizeof(GsVertexPositionNormal3Textures);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		vertexFormat = FVF_T3VERTEX;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT_WITH_TANGENT_VECTOR:
		stride = sizeof(GsVertexPositionNormalTextureSomething);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		vertexFormat = FVF_TVERTEX;
		break;

	default:
		return GS_FAIL__INVALID_ID;
	}

	if (direct3D9.isUsingShaders())
	{
		direct3D9.activateVertexShader(0, vertexType);
		direct3D9.activatePixelShader(0);
	}
	else
		device->SetFVF(vertexFormat);

	if (FAILED(device->DrawPrimitiveUP(D3DPT_POINTLIST, verticesNumber, vertices, stride)))
	{
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		return GS_FAIL;
	}

	device->SetRenderState(D3DRS_LIGHTING, TRUE);
	return GS_OK;
}

GsResult GsDirect3D9VertexCacheManager::createIndexBuffer(
	UINT indicesNumber,
	const WORD* indices,
	UINT* index)
{
	void* data;

	if (indexBuffersNumber >= (GS_MAX_ID - 1))
		return GS_FAIL__OUT_OF_MEMORY;

	if ((indexBuffersNumber % 50) == 0)
	{
		int n = (indexBuffersNumber + 50) * sizeof(GsIndexBuffer);
		indexBuffers = (GsIndexBuffer*)realloc(indexBuffers, n);

		if (!indexBuffers)
			return GS_FAIL__OUT_OF_MEMORY;
	}

	indexBuffers[indexBuffersNumber].indicesNumber = indicesNumber;
	indexBuffers[indexBuffersNumber].trianglesNumber = (int)(indicesNumber / 3.0);
	indexBuffers[indexBuffersNumber].indexBuffer = nullptr;

	if (FAILED(device->CreateIndexBuffer(indicesNumber * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &indexBuffers[indexBuffersNumber].indexBuffer, nullptr)))
		return GS_FAIL__DIRECT3DDEVICE9__CREATE_INDEX_BUFFER;

	if (SUCCEEDED(indexBuffers[indexBuffersNumber].indexBuffer->Lock(0, 0, (void**)&data, 0)))
	{
		memcpy(data, indices, indicesNumber * sizeof(WORD));
		indexBuffers[indexBuffersNumber].indexBuffer->Unlock();
	}
	else
		return GS_FAIL__DIRECT3DINDEXBUFFER9__LOCK;

	*index = indexBuffersNumber;
	indexBuffersNumber++;

	return GS_OK;
}

GsResult GsDirect3D9VertexCacheManager::renderNaked(
	UINT verticesNumber, 
	const void* vertices, 
	bool textured)
{
	invalidateStates();
	direct3D9.setUseShaders(false);
	device->SetTexture(0, nullptr);

	if (textured)
		device->SetTexture(0, (LPDIRECT3DTEXTURE9)skinManager.getTexture(2).data);
	else
		device->SetTexture(0, nullptr);

	device->SetFVF(FVF_PVERTEX);
	if (FAILED(device->DrawPrimitiveUP(D3DPT_TRIANGLELIST, verticesNumber / 3, vertices, sizeof(GsVertexPositionOnly))))
		return GS_FAIL;

	return GS_OK;
}

GsResult GsDirect3D9VertexCacheManager::renderLines(
	GsVertexType vertexType, 
	UINT verticesNumber, 
	const void* vertices, 
	const GsColor* color, 
	bool stripLine)
{
	D3DMATERIAL9 material;
	DWORD vertexFormat;
	int stride;

	if (failed(forceFlushAll()))
		return GS_FAIL;

	invalidateStates();

	if (color)
	{
		memset(&material, 0, sizeof(D3DMATERIAL9));
		material.Diffuse.r = material.Ambient.r = color->red;
		material.Diffuse.g = material.Ambient.g = color->green;
		material.Diffuse.b = material.Ambient.b = color->blue;
		material.Diffuse.a = material.Ambient.a = color->alpha;
		
		device->SetMaterial(&material);
	}

	device->SetTexture(0, nullptr);
	device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
	device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
	device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

	switch (vertexType)
	{
	case GS_VERTEX__UNTRANSFORMED_POSITION_ONLY:
		stride = sizeof(GsVertexPositionOnly);
		device->SetRenderState(D3DRS_LIGHTING, FALSE);
		vertexFormat = FVF_PVERTEX;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT:
		stride = sizeof(GsVertexPositionNormalTexture);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		vertexFormat = FVF_VERTEX;
		break;

	case GS_VERTEX__UNTRANSFORMED_LIT:
		stride = sizeof(GsVertexPositionColorTexture);
		device->SetRenderState(D3DRS_LIGHTING, FALSE);
		vertexFormat = FVF_LVERTEX;
		break;

	case GS_VERTEX__CHARACTER_ANIMATION:
		stride = sizeof(GsVertexPositionNormalTexture2Bones);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		vertexFormat = FVF_CVERTEX;
		break;

	case GS_VERTEX__THREE_TEXTURE_COORD_PAIRS:
		stride = sizeof(GsVertexPositionNormal3Textures);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		vertexFormat = FVF_T3VERTEX;
		break;

	case GS_VERTEX__UNTRANSFORMED_UNLIT_WITH_TANGENT_VECTOR:
		stride = sizeof(GsVertexPositionNormalTextureSomething);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		vertexFormat = FVF_TVERTEX;
		break;

	default:
		return GS_FAIL__INVALID_ID;
	}

	if (direct3D9.isUsingShaders())
	{
		direct3D9.activateVertexShader(0, vertexType);
		direct3D9.activatePixelShader(0);
	}
	else
		device->SetFVF(vertexFormat);

	if (!stripLine)
	{
		if (FAILED(device->DrawPrimitiveUP(D3DPT_LINELIST, verticesNumber / 2, vertices, stride)))
		{
			device->SetRenderState(D3DRS_LIGHTING, TRUE);
			return GS_FAIL;
		}
	}
	else
	{
		if (FAILED(device->DrawPrimitiveUP(D3DPT_LINESTRIP, verticesNumber - 1, vertices, stride)))
		{
			device->SetRenderState(D3DRS_LIGHTING, TRUE);
			return GS_FAIL;
		}
	}

	device->SetRenderState(D3DRS_LIGHTING, TRUE);
	
	return GS_OK;
}

GsResult GsDirect3D9VertexCacheManager::renderLine(
	const float* start, 
	const float* end, 
	const GsColor* color)
{
	D3DMATERIAL9 material;
	GsVertexPositionColorTexture vertices[2];

	if (!color)
		return GS_FAIL__INVALID_PARAMETER;

	if (failed(forceFlushAll()))
		return GS_FAIL;

	invalidateStates();

	getGsDirect3D9().setUseShaders(false);

	vertices[0].x = start[0];
	vertices[0].y = start[1];
	vertices[0].z = start[2];
	vertices[1].x = end[0];
	vertices[1].y = end[1];
	vertices[1].z = end[2];

	vertices[0].color = vertices[1].color = D3DCOLOR_COLORVALUE(color->red, color->green, color->blue, color->alpha);

	memset(&material, 0, sizeof(D3DMATERIAL9));
	material.Diffuse.r = material.Ambient.r = color->red;
	material.Diffuse.g = material.Ambient.g = color->green;
	material.Diffuse.b = material.Ambient.b = color->blue;
	material.Diffuse.a = material.Ambient.a = color->alpha;

	device->SetMaterial(&material);
	device->SetTexture(0, nullptr);

	device->SetFVF(FVF_LVERTEX);
	device->SetVertexShader(nullptr);
	device->SetRenderState(D3DRS_LIGHTING, TRUE);

	if (FAILED(device->DrawPrimitiveUP(D3DPT_LINELIST, 1, vertices, sizeof(GsVertexPositionColorTexture))))
	{
		device->SetFVF(NULL);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);

		return GS_FAIL;
	}

	device->SetRenderState(D3DRS_LIGHTING, TRUE);
	device->SetFVF(NULL);

	return GS_OK;
}

DWORD GsDirect3D9VertexCacheManager::getActiveCacheIndex()
{
	return activeCacheIndex;
}

void GsDirect3D9VertexCacheManager::setActiveCacheIndex(
	DWORD index)
{
	activeCacheIndex = index;
}

GsRenderState GsDirect3D9VertexCacheManager::getShadeMode()
{
	return direct3D9.getShadeMode();
}