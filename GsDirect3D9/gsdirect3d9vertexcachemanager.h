#pragma once


#include "..\GsGraphics\gsvertexcachemanager.h"
#include "gsdirect3d9skinmanager.h"
#include "gsdirect3d9.h"
#include "gsdirect3d9vertexcache.h"

#include <d3d9.h>


const auto GS_VERTEX_CACHES_NUMBER = 10;


struct GsStaticBuffer
{
	int stride;
	UINT skinIndex;
	bool useIndices;
	int verticesNumber;
	int indicesNumber;
	int trianglesNumber;
	DWORD vertexFormat;

	LPDIRECT3DVERTEXBUFFER9 vertexBuffer;
	LPDIRECT3DINDEXBUFFER9 indexBuffer;
};

struct GsIndexBuffer
{
	int indicesNumber;
	int trianglesNumber;
	
	LPDIRECT3DINDEXBUFFER9 indexBuffer;
};


const auto GS_STATIC_BUFFERS_MAX_NUMBER = 65535;


class GsDirect3D9VertexCache;


class GsDirect3D9VertexCacheManager : public GsVertexCacheManager
{
public:
	GsDirect3D9VertexCacheManager(
		GsLog& log,
		GsDirect3D9SkinManager& skinManager,
		LPDIRECT3DDEVICE9 device,
		GsDirect3D9& direct3D9,
		UINT verticesMaxNumber,
		UINT indicesMaxNumber);
	~GsDirect3D9VertexCacheManager();

	GsResult createStaticBuffer(
		GsVertexType vertexType,
		UINT skinIndex,
		UINT verticesNumber,
		UINT indicesNumber,
		const void* vertices,
		const WORD* indices,
		UINT* index);

	GsResult createIndexBuffer(
		UINT indicesNumber, 
		const WORD* indices, 
		UINT* index);

	GsResult render(
		GsVertexType vertexType,
		UINT verticesNumber,
		UINT indicesNumber,
		const void* vertices,
		const WORD* indices,
		UINT skinIndex);

	GsResult renderNaked(
		UINT verticesNumber, 
		const void* vertices, 
		bool textured);

	GsResult render(
		UINT staticBufferIndex);
	GsResult render(
		UINT staticBufferIndex,
		UINT indexBufferIndex,
		UINT skinIndex);
	GsResult render(
		UINT staticBufferIndex,
		UINT skinIndex,
		UINT startIndex,
		UINT verticesNumber,
		UINT trianglesNumber);

	GsResult renderPoints(
		GsVertexType vertexType,
		UINT verticesNumber,
		const void* vertices,
		const GsColor* color);

	GsResult renderLines(
		GsVertexType vertexType,
		UINT verticesNumber,
		const void* vertices,
		const GsColor* color,
		bool stripLine);

	GsResult renderLine(
		const float* start,
		const float* end,
		const GsColor* color);

	GsResult forceFlushAll();

	GsResult forceFlush(
		GsVertexType vertexType);

	DWORD getActiveCacheIndex();

	void setActiveCacheIndex(
		DWORD index);

	GsDirect3D9& getGsDirect3D9();

	void invalidateStates();

	GsRenderState getShadeMode();


private:
	GsDirect3D9SkinManager& skinManager;
	LPDIRECT3DDEVICE9 device;
	GsDirect3D9& direct3D9;

	GsStaticBuffer* staticBuffers;
	GsIndexBuffer* indexBuffers;
	UINT staticBuffersNumber;
	UINT indexBuffersNumber;

	GsDirect3D9VertexCache* cachePositionOnly[GS_VERTEX_CACHES_NUMBER];
	GsDirect3D9VertexCache* cacheUntransformedUnlit[GS_VERTEX_CACHES_NUMBER];
	GsDirect3D9VertexCache* cacheUntransformedLit[GS_VERTEX_CACHES_NUMBER];
	GsDirect3D9VertexCache* cacheCharacterAnimation[GS_VERTEX_CACHES_NUMBER];
	GsDirect3D9VertexCache* cacheThreeTextures[GS_VERTEX_CACHES_NUMBER];
	GsDirect3D9VertexCache* cacheUntransformedUnlitWithTanget[GS_VERTEX_CACHES_NUMBER];

	DWORD activeCacheIndex;
	DWORD activeStaticBufferIndex;
	DWORD activeIndexBufferIndex;
};