#include "gsdirect3d9vertexcache.h"


GsDirect3D9VertexCache::GsDirect3D9VertexCache(
	GsLog& newLog, 
	UINT newVerticesMaxNumber, 
	UINT newIndicesMaxNumber, 
	UINT newStride, 
	GsDirect3D9SkinManager& newSkinManager, 
	LPDIRECT3DDEVICE9 newDevice, 
	GsDirect3D9VertexCacheManager& newVertexManager, 
	DWORD newIndex, 
	DWORD newVertexFormat) : log(newLog), skinManager(newSkinManager), vertexCacheManager(newVertexManager)
{
	device = newDevice;
	verticesMaxNumber = newVerticesMaxNumber;
	indicesMaxNumber = newIndicesMaxNumber;
	verticesNumber = 0;
	indicesNumber = 0;
	index = newIndex;
	vertexFormat = newVertexFormat;
	stride = newStride;

	memset(&currentSkin, GS_MAX_ID, sizeof(GsSkin));
	currentSkinIndex = GS_MAX_ID;

	vertexBuffer = nullptr;
	indexBuffer = nullptr;

	if(FAILED(device->CreateVertexBuffer(verticesMaxNumber * stride, D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &vertexBuffer, nullptr)))
		vertexBuffer = nullptr;

	if (FAILED(device->CreateIndexBuffer(indicesMaxNumber * sizeof(WORD), D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &indexBuffer, nullptr)))
		indexBuffer = nullptr;
}

GsDirect3D9VertexCache::~GsDirect3D9VertexCache()
{
	if (vertexBuffer)
	{
		vertexBuffer->Release();
		vertexBuffer = nullptr;
	}

	if (indexBuffer)
	{
		indexBuffer->Release();
		indexBuffer = nullptr;
	}
}

bool GsDirect3D9VertexCache::usesSkin(
	UINT skinIndex)
{
	return (currentSkinIndex == skinIndex);
}

bool GsDirect3D9VertexCache::isEmpty()
{
	if (verticesNumber > 0)
		return false;

	return true;
}

int GsDirect3D9VertexCache::getVerticesNumber()
{
	return verticesNumber;
}

void GsDirect3D9VertexCache::setSkin(
	UINT skinIndex, 
	bool useShader)
{
	if (!usesSkin(skinIndex))
	{
		GsSkin* skin = &skinManager.getSkin(skinIndex);

		if (!isEmpty())
			flush(useShader);

		memcpy(&currentSkin, skin, sizeof(GsSkin));
		currentSkinIndex = skinIndex;

		vertexCacheManager.invalidateStates();
	}
}

GsResult GsDirect3D9VertexCache::add(
	UINT addingVerticesNumber, 
	UINT addingIndicesNumber, 
	const void* vertices, 
	const WORD* indices, 
	bool useShaders)
{
	BYTE* temporaryVertices = nullptr;
	WORD* temporaryIndices = nullptr;

	int sizeV = stride * addingVerticesNumber;
	int sizeI = sizeof(WORD) * addingIndicesNumber;

	int posV;
	int posI;
	DWORD flags;

	if (addingVerticesNumber > verticesMaxNumber || addingIndicesNumber > indicesMaxNumber)
		return GS_FAIL__VERTEXCACHE__CANT_ADD_SO_MUCH;

	if ((verticesNumber + addingVerticesNumber > verticesMaxNumber) || (indicesNumber + addingIndicesNumber > indicesMaxNumber))
		if (failed(flush(useShaders)))
		{
			log << "Failed flush().";
			return GS_FAIL__VERTEXCACHE__FLUSH;
		}

	if (verticesNumber == 0)
	{
		posV = posI = 0;
		flags = D3DLOCK_DISCARD;
	}
	else
	{
		posV = stride * verticesNumber;
		posI = sizeof(WORD)* indicesNumber;
		flags = D3DLOCK_NOOVERWRITE;
	}

	if (FAILED(vertexBuffer->Lock(posV, sizeV, (void**)&temporaryVertices, flags)))
		return GS_FAIL__DIRECT3DVERTEXBUFFER9__LOCK;
	if (FAILED(indexBuffer->Lock(posI, sizeI, (void**)&temporaryIndices, flags)))
	{
		vertexBuffer->Unlock();
		return GS_FAIL__DIRECT3DINDEXBUFFER9__LOCK;
	}

	memcpy(temporaryVertices, vertices, sizeV);

	int base = verticesNumber;
	if (!indices)
		addingIndicesNumber = addingVerticesNumber;

	for (UINT i = 0; i < addingIndicesNumber; i++)
	{
		if (indices != nullptr)
			temporaryIndices[i] = indices[i] + base;
		else
			temporaryIndices[i] = i + base;

		indicesNumber++;
	}

	verticesNumber += addingVerticesNumber;

	vertexBuffer->Unlock();
	indexBuffer->Unlock();

	return GS_OK;
}

GsResult GsDirect3D9VertexCache::flush(
	bool useShaders)
{
	int iT = 0;

	if (verticesNumber <= 0)
		return GS_OK;

	if (vertexCacheManager.getActiveCacheIndex() != index)
	{
		if (!useShaders)
			device->SetFVF(vertexFormat);

		device->SetIndices(indexBuffer);
		device->SetStreamSource(0, vertexBuffer, 0, stride);
		vertexCacheManager.setActiveCacheIndex(index);
	}

	if (vertexCacheManager.getGsDirect3D9().getActiveSkinIndex() != currentSkinIndex)
	{
		LPDIRECT3DTEXTURE9 texture = nullptr;
		GsMaterial* material = &skinManager.getMaterial(currentSkin.materialIndex);

		if (vertexCacheManager.getGsDirect3D9().getShadeMode() == GS_RENDER_STATE__RENDER_SOLID_POLYGONS)
		{
			if (!vertexCacheManager.getGsDirect3D9().isUsingShaders())
			{
				D3DMATERIAL9 mat = 
				{
					material->diffuse.red, material->diffuse.green, material->diffuse.blue, material->diffuse.alpha,
					material->ambient.red, material->ambient.green, material->ambient.blue, material->ambient.alpha,
					material->specular.red, material->specular.green, material->specular.blue, material->specular.alpha,
					material->emissive.red, material->emissive.green, material->emissive.blue, material->emissive.alpha,
					material->power
				};

				device->SetMaterial(&mat);
			}
			else
			{
				vertexCacheManager.getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 1, 1, &material->ambient);
				vertexCacheManager.getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 2, 1, &material->diffuse);
				vertexCacheManager.getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 3, 1, &material->emissive);
				vertexCacheManager.getGsDirect3D9().setShaderConstant(GS_SHADER__PIXEL, GS_DATA__FLOAT, 4, 1, &material->specular);
			}

			if (vertexCacheManager.getGsDirect3D9().isUsingTextures())
			{
				for (iT = 0; iT < 8; iT++)
				{
					if (currentSkin.textureIndices[iT] != GS_MAX_ID)
					{
						texture = (LPDIRECT3DTEXTURE9)skinManager.getTexture(currentSkin.textureIndices[iT]).data;

						if (FAILED(device->SetTexture(iT, texture)))
							log << "Failed SetTexture() in vertexCache::flush().";

						device->SetTextureStageState(iT, D3DTSS_TEXCOORDINDEX, 0);
						device->SetTextureStageState(iT, D3DTSS_COLORARG1, D3DTA_TEXTURE);
						device->SetTextureStageState(iT, D3DTSS_COLORARG2, D3DTA_CURRENT);
						device->SetTextureStageState(iT, D3DTSS_COLOROP, vertexCacheManager.getGsDirect3D9().getTextureOperation(iT));
					}
					else
						break;
				}

				device->SetTextureStageState(iT, D3DTSS_COLOROP, D3DTOP_DISABLE);
			}
			else
			{
				device->SetTexture(0, nullptr);
				device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
			}
		}
		else
		{
			GsColor wireframeColor = vertexCacheManager.getGsDirect3D9().getWireframeColor();

			D3DMATERIAL9 materialWire =
			{
				wireframeColor.red, wireframeColor.green, wireframeColor.blue, wireframeColor.alpha,
				wireframeColor.red, wireframeColor.green, wireframeColor.blue, wireframeColor.alpha,
				0.0, 0.0, 0.0, 1.0,
				0.0, 0.0, 0.0, 1.0,
				1.0
			};
			device->SetMaterial(&materialWire);

			device->SetTexture(0, nullptr);
			device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
		}

		if (currentSkin.alphaEnabled)
		{
			device->SetRenderState(D3DRS_ALPHAREF, 50);
			device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
			device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
			device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		}
		else
		{
			device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
			device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		}

		vertexCacheManager.getGsDirect3D9().setActiveSkinIndex(currentSkinIndex);
	}

	if (vertexCacheManager.getGsDirect3D9().isUsingAdditiveBlending())
	{
		device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
		device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}

	if (!vertexCacheManager.getGsDirect3D9().isUsingColorBuffer())
	{
		device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
		device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}

	GsRenderState shadeMode = vertexCacheManager.getGsDirect3D9().getShadeMode();


	HRESULT result;
	switch (shadeMode)
	{
	case GS_RENDER_STATE__RENDER_JUST_VERTICES:
		result = device->DrawPrimitive(D3DPT_POINTLIST, 0, verticesNumber);
		break;

	case GS_RENDER_STATE__RENDER_TWO_VERTICES_AS_LINE:
		result = device->DrawIndexedPrimitive(D3DPT_LINELIST, 0, 0, verticesNumber, 0, indicesNumber / 2);
		break;

	case GS_RENDER_STATE__RENDER_POLYHULL_AS_POLYLINE:
		result = device->DrawIndexedPrimitive(D3DPT_LINESTRIP, 0, 0, verticesNumber, 0, verticesNumber);
		break;

	default:
		result = device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, verticesNumber, 0, indicesNumber / 3);
	}

	if (FAILED(result))
	{
		log << "Failed IDirect3DDevice::Draw[Indexed]Primitive().";
		return GS_FAIL__VERTEXCACHE__FLUSH;
	}

	verticesNumber = 0;
	indicesNumber = 0;
	return GS_OK;
}
