#include "gsdirect3d9.h"

#include "..\GsGraphics\gscolor.h"
#include "..\GsMathematics\gsvector.h"
#include "..\GsMathematics\gsmatrix.h"
#include "gsdirect3d9skinmanager.h"
#include "gsdirect3d9vertexcachemanager.h"


#pragma comment(lib, "d3d9.lib")


GsDirect3D9::GsDirect3D9(
    GsLog& newLog) : GsRenderDevice(newLog)
{
    device = nullptr;
    direct3D = nullptr;

    clearColor = D3DCOLOR_COLORVALUE(0.0f, 0.0f, 0.0f, 1.0f);

    initialized = false;
    sceneRunning = false;


    useShaders = false;
    canDoShaders = false;
    useAdditiveBlending = false;
    useColorBuffer = true;
    useTextures = true;

    skinManager = nullptr;
    vertexCacheManager = nullptr;

    declarationVertexPositionOnly = nullptr;
    declarationVertexPositionNormalTexture = nullptr;
    declarationVertexPositionColorTexture = nullptr;
    declarationVertexPositionNormalTexture2Bones = nullptr;
    declarationVertexPositionNormal3Textures = nullptr;
    declarationVertexPositionNormalTextureSomething = nullptr;

    fonts = nullptr;

    vertexShadersNumber = 0;
    pixelShadersNumber = 0;
    fontsNumber = 0;
}

GsDirect3D9::~GsDirect3D9()
{
    release();
}

GsResult GsDirect3D9::init(
    HWND newWindow,
    GsDisplayParameters newDisplayParameters)
{
    GsResult result;

    window = newWindow;

    direct3D = Direct3DCreate9(D3D_SDK_VERSION);

    if(!direct3D)
    {
        log << "FAIL: Direct3DCreate9 failed in GsDirect3D9::init()";
        return GS_FAIL__DIRECT3D_CREATE9;
    }
    else
        log << "Direct3D creation succeeded.";

    D3DCAPS9 caps;

    if(FAILED(direct3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps)))
    {
        log << "FAIL: LPDIRECT3D9::GetDeviceCaps() in GsDirect3D9::init()";
        return GS_FAIL__DIRECT3D_GET_DEVICE_CAPS;
    }
    else
        log << "Getting graphic device capabilities succeeded.";


    if(FAILED(direct3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
    {
        log << "FAIL: LPDIRECT3D9::GetAdapterDisplayMode() in GsDirect3D9::init()";
        return GS_FAIL__DIRECT3D_GET_ADAPTER_DISPLAY_MODE;
    }
    else
        log << "Getting graphic adapter display mode succeeded.";

    setDisplayParameters(newDisplayParameters);

    resetPresentParameters();


    int vertexProcessingType = 0;

    if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
        vertexProcessingType = D3DCREATE_HARDWARE_VERTEXPROCESSING;
    else
        vertexProcessingType = D3DCREATE_SOFTWARE_VERTEXPROCESSING;

    if(FAILED(direct3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, window, vertexProcessingType, &presentParameters, &device)))
    {
        log << "Graphic device with 24-bit depth stencil creation failed. Trying to create with 16-bit depth stencil.";

        presentParameters.AutoDepthStencilFormat = D3DFMT_D16;

        if(FAILED(direct3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, window, vertexProcessingType, &presentParameters, &device)))
        {
            direct3D->Release();

            log << "FAIL: LPDIRECT3D9::CreateDevice() in GsDirect3D9::init().";
            return GS_FAIL__DIRECT3D_CREATE_DEVICE;
        }
    }
    else
        log << "Graphic device creation succeeded.";

    direct3D->Release();

    oneTimeInit();

    initialized = true;
    sceneRunning = false;
    logDeviceCapabilities(caps);

    return GS_OK;
}

void GsDirect3D9::release()
{
    if(initialized)
        log << "Direct3D 9 release started.";


    if(skinManager)
    {
        delete skinManager;
        skinManager = nullptr;
    }

    if(vertexCacheManager)
    {
        delete vertexCacheManager;
        vertexCacheManager = nullptr;
    }

    if(declarationVertexPositionOnly)
    {
        declarationVertexPositionOnly->Release();
        declarationVertexPositionOnly = nullptr;
    }

    if(declarationVertexPositionNormalTexture)
    {
        declarationVertexPositionNormalTexture->Release();
        declarationVertexPositionNormalTexture = nullptr;
    }

    if(declarationVertexPositionColorTexture)
    {
        declarationVertexPositionColorTexture->Release();
        declarationVertexPositionColorTexture = nullptr;
    }

    if(declarationVertexPositionNormal3Textures)
    {
        declarationVertexPositionNormal3Textures->Release();
        declarationVertexPositionNormal3Textures = nullptr;
    }

    if(declarationVertexPositionNormalTexture2Bones)
    {
        declarationVertexPositionNormalTexture2Bones->Release();
        declarationVertexPositionNormalTexture2Bones = nullptr;
    }

    if(declarationVertexPositionNormalTextureSomething)
    {
        declarationVertexPositionNormalTextureSomething->Release();
        declarationVertexPositionNormalTextureSomething = nullptr;
    }


    for(UINT i = 0; i < vertexShadersNumber; i++)
    {
        if(vertexShaders[i])
        {
            vertexShaders[i]->Release();
            vertexShaders[i] = nullptr;
        }

        if(vertexShaderConstantTables[i])
        {
            vertexShaderConstantTables[i]->Release();
            vertexShaderConstantTables[i] = nullptr;
        }
    }

    for(UINT i = 0; i < pixelShadersNumber; i++)
    {
        if(pixelShaders[i])
        {
            pixelShaders[i]->Release();
            pixelShaders[i] = nullptr;
        }

        if(pixelShaderConstantTables[i])
        {
            pixelShaderConstantTables[i]->Release();
            pixelShaderConstantTables[i] = nullptr;
        }
    }

    for(UINT i = 0; i < fontsNumber; i++)
        if(fonts[i])
        {
            fonts[i]->Release();
            fonts[i] = nullptr;
        }

    if(fonts)
    {
        free(fonts);
        fonts = nullptr;
    }


    if(device)
    {
        device->Release();
        device = nullptr;
    }

    if(direct3D)
    {
        direct3D->Release();
        direct3D = nullptr;
    }

    if(initialized)
        log << "Direct3D 9 release succeeded.";

    initialized = false;
}

bool GsDirect3D9::isInitialized()
{
    return initialized;
}

bool GsDirect3D9::isSceneRunning()
{
    return sceneRunning;
}

GsResult GsDirect3D9::beginRendering(
    GsClearTarget clearTarget)
{
    GsResult result;

    if(failed(result = clear(clearTarget)))
    {
        log << "FAILED: GsDirect3D9::clear() in GsDirect3D9::beginRendering().";
        return result;
    }

    if(FAILED(device->BeginScene()))
    {
        log << "FAILED: DIRECT3DDEVICE9::BeginScene() in GsDirect3D9::beginRendering().";
        return GS_FAIL__DIRECT3DDEVICE_BEGIN_SCENE;
    }

    sceneRunning = true;
    return GS_OK;
}

void GsDirect3D9::endRendering()
{
    if(failed(vertexCacheManager->forceFlushAll()))
        log << "Error: forceFlushAll() from endRendering() failed.";

    device->EndScene();
    device->Present(nullptr, nullptr, nullptr, nullptr);

    sceneRunning = false;
}

GsResult GsDirect3D9::clear(
    GsClearTarget clearTarget)
{
    DWORD target = 0;

    if(clearTarget & GS_CLEAR__PIXEL)
        target |= D3DCLEAR_TARGET;

    if(clearTarget & GS_CLEAR__DEPTH)
        target |= D3DCLEAR_ZBUFFER;

    if(clearTarget & GS_CLEAR__STENCIL)
        target |= D3DCLEAR_STENCIL;

    if(sceneRunning)
        device->EndScene();

    if(FAILED(device->Clear(0, nullptr, target, clearColor, 1.0f, 0)))
    {

        log << "FAILED: DIRECT3DDEVICE9::Clear() in GsDirect3D9::clear().";
        return GS_FAIL__DIRECT3DDEVICE9_CLEAR;
    }

    if(sceneRunning)
        device->BeginScene();

    return GS_OK;
}

void GsDirect3D9::setClearColor(
    GsColor color)
{
    clearColor = D3DCOLOR_COLORVALUE(color.red, color.green, color.blue, 1.0f);

    char buffer[9];
    log << "Change clear color to 0x" + std::string(itoa(clearColor, buffer, 16)) + ".";
}

GsResult GsDirect3D9::reset()
{
    resetPresentParameters();

    HRESULT result;

    if(FAILED(result = device->Reset(&presentParameters)))
    {
        log << "FAILED: DIRECT3DDEVICE9::Reset() in GsDirect3D9::reset().";

        switch(result)
        {
            case D3DERR_DEVICELOST:
                log << "Device lost.";
                break;

            case D3DERR_DEVICEREMOVED:
                log << "Device removed.";
                break;

            case D3DERR_DRIVERINTERNALERROR:
                log << "Driver internal error.";
                break;

            case D3DERR_OUTOFVIDEOMEMORY:
                log << "Out of video memory.";
                break;

            default:
                log << "Unknown error #" + std::to_string(result) + ".";
        }

        return GS_FAIL__DIRECT3DDEVICE9_RESET;
    }

    if(windowed)
        SetWindowPos(window, HWND_NOTOPMOST, windowX, windowY, windowWidth, windowHeight, SWP_SHOWWINDOW);

    log << "Graphics reset succeeded.";
    return GS_OK;
}


void GsDirect3D9::setDisplayParameters(
    GsDisplayParameters displayParameters)
{
    // displayMode must be initialized before this

    windowed = displayParameters.windowed;

    windowX = displayParameters.x;
    windowY = displayParameters.y;

    windowWidth = displayParameters.width;
    windowHeight = displayParameters.height;

    screenWidth = displayMode.Width;
    screenHeight = displayMode.Height;
}

GsResult GsDirect3D9::switchWindowMode()
{
    GsResult result;

    if(windowed)
    {
        WINDOWINFO windowInfo;
        windowInfo.cbSize = sizeof(WINDOWINFO);
        GetWindowInfo(window, &windowInfo);
        windowX = windowInfo.rcWindow.left;
        windowY = windowInfo.rcWindow.top;

        windowWidth = windowInfo.rcWindow.right - windowX;
        windowHeight = windowInfo.rcWindow.bottom - windowY;
    }

    windowed = !windowed;



    if(failed(result = reset()))
    {
        log << "FAILED: GsDirect3D9::reset() in GsDirect3D9::switchWindowMode().";
        return result;
    }

    log << "Switching display mode to " + std::string(windowed ? "windowed" : "full screen") + " succeeded.";
    return GS_OK;
}

void GsDirect3D9::resetPresentParameters()
{
    // displayMode must be initialized before this

    ZeroMemory(&presentParameters, sizeof(D3DPRESENT_PARAMETERS));

    if(windowed)
    {
        presentParameters.BackBufferWidth = windowWidth;
        presentParameters.BackBufferHeight = windowHeight;
    }
    else
    {
        presentParameters.BackBufferWidth = screenWidth;
        presentParameters.BackBufferHeight = screenHeight;
    }

    presentParameters.BackBufferFormat = displayMode.Format;
    presentParameters.BackBufferCount = 1;
    presentParameters.MultiSampleType = D3DMULTISAMPLE_NONE;
    presentParameters.MultiSampleQuality = 0;
    presentParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
    presentParameters.hDeviceWindow = window;
    presentParameters.Windowed = windowed;
    presentParameters.EnableAutoDepthStencil = true;
    presentParameters.AutoDepthStencilFormat = D3DFMT_D24S8;
    presentParameters.Flags = 0;
    presentParameters.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
    presentParameters.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

    log << "Graphic device present parameters reset succeeded.";
}


GsResult GsDirect3D9::setView3D(
    GsVector& right,
    GsVector& up,
    GsVector& direction,
    GsVector& position)
{
//    if(!sceneRunning)
//        return GS_FAIL__SCENE_NOT_RUNNING;

    matrixView3D._14 = 0.0;
    matrixView3D._24 = 0.0;
    matrixView3D._34 = 0.0;
    matrixView3D._44 = 1.0;

    matrixView3D._11 = right.x;
    matrixView3D._21 = right.y;
    matrixView3D._31 = right.z;
    matrixView3D._41 = -right * position;

    matrixView3D._12 = up.x;
    matrixView3D._22 = up.y;
    matrixView3D._32 = up.z;
    matrixView3D._42 = -up * position;

    matrixView3D._13 = direction.x;
    matrixView3D._23 = direction.y;
    matrixView3D._33 = direction.z;
    matrixView3D._43 = -direction * position;

    
        if(FAILED(device->SetTransform(D3DTS_VIEW, &matrixView3D)))
            return GS_FAIL__DIRECT3DDEVICE9__SET_TRANSFORM;

    calculateViewProjectionMatrix();
    calculateWorldViewProjectionMatrix();

    return GS_OK;
}

bool GsDirect3D9::isWindowed()
{
    return windowed;
}

GsResolution GsDirect3D9::getResolution()
{
    GsResolution resolution;

    resolution.screenWidth = screenWidth;
    resolution.screenHeight = screenHeight;
    resolution.bitsPerPixel = 32;

    return resolution;
}

UINT GsDirect3D9::getActiveSkinIndex()
{
    return activeSkinIndex;
}

void GsDirect3D9::setActiveSkinIndex(
    UINT index)
{
    activeSkinIndex = index;
}

UINT GsDirect3D9::getActiveVertexShaderIndex()
{
    return activeVertexShaderIndex;
}

UINT GsDirect3D9::getActivePixelShaderIndex()
{
    return activePixelShaderIndex;
}

GsColor GsDirect3D9::getWireframeColor()
{
    return wireframeColor;
}

GsRenderState GsDirect3D9::getShadeMode()
{
    return shadeMode;
}

bool GsDirect3D9::isUsingAdditiveBlending()
{
    return useAdditiveBlending;
}

bool GsDirect3D9::isUsingColorBuffer()
{
    return useColorBuffer;
}

bool GsDirect3D9::isUsingTextures()
{
    return useTextures;
}

bool GsDirect3D9::isThatCanUseShaders()
{
    return canUseShaders;
}

bool GsDirect3D9::isThatCanDoShaders()
{
    return canDoShaders;
}

bool GsDirect3D9::isUsingShaders()
{
    return useShaders;
}

D3DTEXTUREOP GsDirect3D9::getTextureOperation(
    UCHAR index)
{
    if(index >= GS_TEXTURES_PER_SKIN)
        return D3DTOP_FORCE_DWORD;

    return textureOperations[index];
}

GsResult GsDirect3D9::setViewLookAt(
    GsVector& from,
    GsVector& to,
    GsVector& worldUp)
{
    GsVector direction, up, temporary;

    direction = to - from;
    direction.normalize();

    temporary = direction * (worldUp * direction);
    up = worldUp - temporary;

    if(arePracticallyEqual(up.getLength(), (float)0.0))
    {
        GsVector yAxis(0.0, 1.0, 0.0);

        temporary = direction * direction.y;
        up = yAxis - temporary;

        if(arePracticallyEqual(up.getLength(), (float)0.0))
        {
            GsVector zAxis(0.0, 0.0, 1.0);

            temporary = direction * direction.z;
            up = zAxis - temporary;

            if(arePracticallyEqual(up.getLength(), (float)0.0))
                return GS_FAIL__DIRECT3D9_VIEW_LOOK_AT__VECTOR_IS_TOO_SMALL;
        }
    }

    up.normalize();

    GsVector right = GsVector::cross(up, direction);

    return setView3D(right, up, direction, from);
}

GsResult GsDirect3D9::getFrustum(
    GsClippingPlanes& clippingPlanes)
{
    clippingPlanes.planeLeft.normal.x = -(matrixViewProjection._14 + matrixViewProjection._11);
    clippingPlanes.planeLeft.normal.y = -(matrixViewProjection._24 + matrixViewProjection._21);
    clippingPlanes.planeLeft.normal.z = -(matrixViewProjection._34 + matrixViewProjection._31);
    clippingPlanes.planeLeft.distanceToOrigin = -(matrixViewProjection._44 + matrixViewProjection._41);

    clippingPlanes.planeRight.normal.x = -(matrixViewProjection._14 - matrixViewProjection._11);
    clippingPlanes.planeRight.normal.y = -(matrixViewProjection._24 - matrixViewProjection._21);
    clippingPlanes.planeRight.normal.z = -(matrixViewProjection._34 - matrixViewProjection._31);
    clippingPlanes.planeRight.distanceToOrigin = -(matrixViewProjection._44 - matrixViewProjection._41);

    clippingPlanes.planeTop.normal.x = -(matrixViewProjection._14 - matrixViewProjection._12);
    clippingPlanes.planeTop.normal.y = -(matrixViewProjection._24 - matrixViewProjection._22);
    clippingPlanes.planeTop.normal.z = -(matrixViewProjection._34 - matrixViewProjection._32);
    clippingPlanes.planeTop.distanceToOrigin = -(matrixViewProjection._44 - matrixViewProjection._42);

    clippingPlanes.planeBottom.normal.x = -(matrixViewProjection._14 + matrixViewProjection._12);
    clippingPlanes.planeBottom.normal.y = -(matrixViewProjection._24 + matrixViewProjection._22);
    clippingPlanes.planeBottom.normal.z = -(matrixViewProjection._34 + matrixViewProjection._32);
    clippingPlanes.planeBottom.distanceToOrigin = -(matrixViewProjection._44 + matrixViewProjection._42);

    clippingPlanes.planeNear.normal.x = -matrixViewProjection._13;
    clippingPlanes.planeNear.normal.y = -matrixViewProjection._23;
    clippingPlanes.planeNear.normal.z = -matrixViewProjection._33;
    clippingPlanes.planeNear.distanceToOrigin = -matrixViewProjection._43;

    clippingPlanes.planeFar.normal.x = -(matrixViewProjection._14 - matrixViewProjection._13);
    clippingPlanes.planeFar.normal.y = -(matrixViewProjection._24 - matrixViewProjection._23);
    clippingPlanes.planeFar.normal.z = -(matrixViewProjection._34 - matrixViewProjection._33);
    clippingPlanes.planeFar.distanceToOrigin = -(matrixViewProjection._44 - matrixViewProjection._43);


    float length = clippingPlanes.planeLeft.normal.getLength();
    clippingPlanes.planeLeft.normal.normalize();
    clippingPlanes.planeLeft.distanceToOrigin /= length;

    length = clippingPlanes.planeRight.normal.getLength();
    clippingPlanes.planeRight.normal.normalize();
    clippingPlanes.planeRight.distanceToOrigin /= length;

    length = clippingPlanes.planeTop.normal.getLength();
    clippingPlanes.planeTop.normal.normalize();
    clippingPlanes.planeTop.distanceToOrigin /= length;

    length = clippingPlanes.planeBottom.normal.getLength();
    clippingPlanes.planeBottom.normal.normalize();
    clippingPlanes.planeBottom.distanceToOrigin /= length;

    length = clippingPlanes.planeNear.normal.getLength();
    clippingPlanes.planeNear.normal.normalize();
    clippingPlanes.planeNear.distanceToOrigin /= length;

    length = clippingPlanes.planeFar.normal.getLength();
    clippingPlanes.planeFar.normal.normalize();
    clippingPlanes.planeFar.distanceToOrigin /= length;

    return GS_OK;
}

void GsDirect3D9::setClippingPlanes(
    float nearDistance,
    float farDistance)
{
    clippingDistanceNear = nearDistance;
    clippingDistanceFar = farDistance;

    if(clippingDistanceNear <= 0.0)
        clippingDistanceNear = GS_MINIMAL_CLIPPING_DISTANCE;

    if(clippingDistanceFar <= 1.0)
        clippingDistanceFar = 1.0;

    if(clippingDistanceNear >= clippingDistanceFar)
    {
        clippingDistanceNear = clippingDistanceFar;
        clippingDistanceFar = clippingDistanceNear + 1.0;
    }

    prepare2D();


    // change orthogonal projection
    float q = 1.0 / (clippingDistanceFar - clippingDistanceNear);
    float x = -q * clippingDistanceNear;

    matrixProjectionOrthogonal._33 = q;
    matrixProjectionOrthogonal._43 = x;


    // change perspective projection
    q *= clippingDistanceFar;
    x = -q * clippingDistanceNear;

    matrixProjectionPerspective._33 = q;
    matrixProjectionPerspective._43 = x;
}

void GsDirect3D9::prepare2D()
{
    memset(&matrixView2D, 0, sizeof(float) * 16);
    memset(&matrixProjection2D, 0, sizeof(float) * 16);

    float width, height;

    if(windowed)
    {
        width = windowWidth;
        height = windowHeight;
    }
    else
    {
        width = screenWidth;
        height = screenHeight;
    }

    matrixProjection2D._11 = 2.0 / width;
    matrixProjection2D._22 = 2.0 / height;
    matrixProjection2D._33 = 1.0 / (clippingDistanceFar - clippingDistanceNear);
    matrixProjection2D._43 = -clippingDistanceNear * (1.0 / (clippingDistanceFar - clippingDistanceNear));
    matrixProjection2D._44 = 1.0;


    matrixView2D._11 = matrixView2D._33 = matrixView2D._44 = 1.0;
    float tx, ty, tz;
    tx = -width * 0.5;
    ty = height * 0.5;
    tz = clippingDistanceNear + 0.1;

    matrixView2D._22 = -1.0;
    matrixView2D._41 = tx;
    matrixView2D._42 = ty;
    matrixView2D._43 = tz;
}

GsResult GsDirect3D9::calculatePrespectiveProjectionMatrix(
    float fov,
    float aspect,
    D3DMATRIX& matrix)
{
    if(arePracticallyEqual(clippingDistanceFar, clippingDistanceNear))
        return GS_FAIL__DIRECT3D9__CLIPPING_DISTANCES_ARE_TOO_CLOSE;

    float sine = sin(fov / 2.0);

    if(arePracticallyEqual(sine, (float)0.0))
        return GS_FAIL__DIRECT3D9__FOV_IS_TOO_SMALL;

    float cosine = cos(fov / 2.0);

    float w = aspect * (cosine / sine);
    float h = 1.0 * (cosine / sine);
    float q = clippingDistanceFar / (clippingDistanceFar - clippingDistanceNear);

    memset(&matrix, 0, sizeof(D3DMATRIX));
    matrix._11 = w;
    matrix._22 = h;
    matrix._33 = q;
    matrix._34 = 1.0;
    matrix._43 = -q * clippingDistanceNear;

    return GS_OK;
}

void GsDirect3D9::calculateViewProjectionMatrix()
{
    GsMatrix a, b;

    if(mode == GS_PROJECTION__2D)
    {
        a = fromDirect3DMatrix(matrixProjection2D);
        b = fromDirect3DMatrix(matrixView2D);
    }
    else
    {
        b = fromDirect3DMatrix(matrixView3D);

        if(mode = GS_PROJECTION__PERSPECTIVE)
            a = fromDirect3DMatrix(matrixProjectionPerspective);
        else
            a = fromDirect3DMatrix(matrixProjectionOrthogonal);
    }

    GsMatrix result = a * b;
    matrixViewProjection = fromGsMatrix(result);
}

void GsDirect3D9::calculateWorldViewProjectionMatrix()
{
    GsMatrix projection, view, world;

    world = fromDirect3DMatrix(matrixWorld);

    if(mode = GS_PROJECTION__2D)
    {
        projection = fromDirect3DMatrix(matrixProjection2D);
        view = fromDirect3DMatrix(matrixView2D);
    }
    else
    {
        view = fromDirect3DMatrix(matrixView3D);

        if(mode = GS_PROJECTION__PERSPECTIVE)
            projection = fromDirect3DMatrix(matrixProjectionPerspective);
        else
            projection = fromDirect3DMatrix(matrixProjectionOrthogonal);
    }

    GsMatrix result = projection * view * world;

    matrixWorldViewProjection = fromGsMatrix(result);
}


GsResult GsDirect3D9::setMode(
    GsEngineMode newMode)
{
//    if(!sceneRunning)
//        return GS_OK;

    if(mode != newMode)
        mode = newMode;

    vertexCacheManager->forceFlushAll();

    D3DVIEWPORT9 d3dViewPort;
    d3dViewPort.X = viewPort.x;
    d3dViewPort.Y = viewPort.y;
    d3dViewPort.Width = viewPort.width;
    d3dViewPort.Height = viewPort.height;
    d3dViewPort.MinZ = 0.0;
    d3dViewPort.MaxZ = 1.0;

    if(mode == GS_PROJECTION__2D)
    {
        if(FAILED(device->SetViewport(&d3dViewPort)))
            return GS_FAIL__DIRECT3DDEVICE9__SET_VIEWPORT;

        
        {
            if(FAILED(device->SetTransform(D3DTS_PROJECTION, &matrixProjection2D)))
                return GS_FAIL__DIRECT3DDEVICE9__SET_TRANSFORM;

            if(FAILED(device->SetTransform(D3DTS_VIEW, &matrixView3D)))
                return GS_FAIL__DIRECT3DDEVICE9__SET_TRANSFORM;
        }
    }
    else
    {
        if(FAILED(device->SetViewport(&d3dViewPort)))
            return GS_FAIL__DIRECT3DDEVICE9__SET_VIEWPORT;

        
        {
            if(FAILED(device->SetTransform(D3DTS_VIEW, &matrixView3D)))
                return GS_FAIL__DIRECT3DDEVICE9__SET_TRANSFORM;

            if(mode == GS_PROJECTION__PERSPECTIVE)
            {
                if(FAILED(device->SetTransform(D3DTS_PROJECTION, &matrixProjectionPerspective)))
                    return GS_FAIL__DIRECT3DDEVICE9__SET_TRANSFORM;
            }
            else
                if(FAILED(device->SetTransform(D3DTS_PROJECTION, &matrixProjectionOrthogonal)))
                    return GS_FAIL__DIRECT3DDEVICE9__SET_TRANSFORM;
        }

        calculateViewProjectionMatrix();
        calculateWorldViewProjectionMatrix();
    }

    return GS_OK;
}

GsResult GsDirect3D9::initStage(
    float newFov,
    GsViewPort* newViewPort)
{
    float aspect;
    bool ownRectangle = false;

    if(newViewPort == nullptr)
    {
        int width, height;

        if(windowed)
        {
            width = windowWidth;
            height = windowHeight;
        }
        else
        {
            width = screenWidth;
            height = screenHeight;
        }

        GsViewPort viewPortOwn = { 0, 0, width, height };
        viewPort = viewPortOwn;
    }
    else
        viewPort = *newViewPort;

    aspect = (float)(viewPort.height) / (float)(viewPort.width);

    if(FAILED(calculatePrespectiveProjectionMatrix(newFov, aspect, matrixProjectionPerspective)))
        return GS_FAIL__DIRECT3D9__CALCULATE_PERSPECTIVE_PROJECTION_MATRIX;

    memset(&matrixProjectionOrthogonal, 0, sizeof(float) * 16);
    matrixProjectionOrthogonal._11 = 2.0 / viewPort.width;
    matrixProjectionOrthogonal._22 = 2.0 / viewPort.height;
    matrixProjectionOrthogonal._33 = 1.0 / (clippingDistanceFar - clippingDistanceNear);

    matrixProjectionOrthogonal._43 = clippingDistanceNear / (clippingDistanceNear - clippingDistanceFar);
    matrixProjectionOrthogonal._44 = 1.0;

    return GS_OK;
}

GsPoint GsDirect3D9::transform3Dto2D(
    const GsVector& position)
{
    float clipX, clipY;
    float x, y, w;

    DWORD width, height;

    if(mode = GS_PROJECTION__2D)
    {
        if(windowed)
        {
            width = windowWidth;
            height = windowHeight;
        }
        else
        {
            width = screenWidth;
            height = screenHeight;
        }
    }
    else
    {
        width = viewPort.width;
        height = viewPort.height;
    }

    clipX = (float)width / 2.0;
    clipY = (float)height / 2.0;

    x = matrixViewProjection._11 * position.x + matrixViewProjection._21 * position.y + matrixViewProjection._31 * position.z + matrixViewProjection._41;
    y = matrixViewProjection._12 * position.x + matrixViewProjection._22 * position.y + matrixViewProjection._32 * position.z + matrixViewProjection._42;
    w = matrixViewProjection._14 * position.x + matrixViewProjection._24 * position.y + matrixViewProjection._34 * position.z + matrixViewProjection._44;

    GsPoint result;

    result.x = (LONG)((1.0 + x / w) * clipX);
    result.y = (LONG)((1.0 + y / w) * clipY);

    return result;
}

void GsDirect3D9::transform2Dto3D(
    const GsPoint& point,
    GsVector* position,
    GsVector* direction)
{
    D3DMATRIX* view;
    D3DMATRIX* projection;
    GsMatrix invertedView;
    GsVector s;
    DWORD width, height;

    if(mode == GS_PROJECTION__2D)
    {
        if(windowed)
        {
            width = windowWidth;
            height = windowHeight;
        }
        else
        {
            width = screenWidth;
            height = screenHeight;
        }

        view = &matrixView2D;
    }
    else
    {
        width = viewPort.width;
        height = viewPort.height;

        view = &matrixView3D;

        if(mode == GS_PROJECTION__PERSPECTIVE)
            projection = &matrixProjectionPerspective;
        else
            projection = &matrixProjectionOrthogonal;
    }

    s.x = ((point.x * 2.0 / width) - 1.0) / matrixProjectionPerspective._11;
    s.y = -((point.y * 2.0 / height) - 1.0) / matrixProjectionPerspective._22;
    s.z = 1.0;

    invertedView = GsMatrix::inverseOf(fromDirect3DMatrix(matrixView3D));

    direction->x = s.x * invertedView[0][0] + s.y * invertedView[0][1] + s.z * invertedView[0][2];
    direction->y = s.x * invertedView[1][0] + s.y * invertedView[1][1] + s.z * invertedView[1][2];
    direction->z = s.x * invertedView[2][0] + s.y * invertedView[2][1] + s.z * invertedView[2][2];

    position->x = invertedView[0][3];
    position->y = invertedView[1][3];
    position->z = invertedView[2][3];
    position->normalize();
}

GsResult GsDirect3D9::oneTimeInit()
{
    shadeMode = GS_RENDER_STATE__RENDER_SOLID_POLYGONS;

    skinManager = new GsDirect3D9SkinManager(log, device);
    vertexCacheManager = new GsDirect3D9VertexCacheManager(log, *(GsDirect3D9SkinManager*)skinManager, device, *this, GS_VERTICES_MAX_NUMBER, GS_INDICES_MAX_NUMBER);

    device->SetRenderState(D3DRS_LIGHTING, TRUE);
    device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
    device->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

    ZeroMemory(&standardMaterial, sizeof(D3DMATERIAL9));
    standardMaterial.Ambient.r = 1.0;
    standardMaterial.Ambient.g = 1.0;
    standardMaterial.Ambient.b = 1.0;
    standardMaterial.Ambient.a = 1.0;

    if(FAILED(device->SetMaterial(&standardMaterial)))
    {
        log << "Failed to set material in oneTimeInit().";
        return GS_FAIL__DIRECT3DDEVICE9__SET_MATERIAL;
    }

    device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);

    GsViewPort viewPort;
    viewPort.x = 0;
    viewPort.y = 0;

    if(windowed)
    {
        viewPort.width = windowWidth;
        viewPort.height = windowHeight;
    }
    else
    {
        viewPort.width = screenWidth;
        viewPort.height = screenHeight;
    }

    mode = GS_PROJECTION__PERSPECTIVE;
    setActiveSkinIndex(GS_MAX_ID);

    ZeroMemory(&matrixView3D, sizeof(D3DMATRIX));
    matrixView3D._11 = matrixView3D._22 = matrixView3D._33 = matrixView3D._44 = 1.0;

    setClippingPlanes(GS_DEFAULT_CLIPPING_DISTANCE_NEAR, GS_DEFAULT_CLIPPING_DISTANCE_FAR);

    prepareShaders();

    GsColor ambientColorWhite = { 1.0, 1.0, 1.0, 1.0 };
    setAmbientLightColor(ambientColorWhite);
    log << "Set ambient light color to 0xffffffff.";

    setTextureStage(0, GS_RENDER_STATE__TEXTURES_MODULATE);
    setTextureStage(1, GS_RENDER_STATE__NOTHING);
    setTextureStage(2, GS_RENDER_STATE__NOTHING);
    setTextureStage(3, GS_RENDER_STATE__NOTHING);
    setTextureStage(4, GS_RENDER_STATE__NOTHING);
    setTextureStage(5, GS_RENDER_STATE__NOTHING);
    setTextureStage(6, GS_RENDER_STATE__NOTHING);
    setTextureStage(7, GS_RENDER_STATE__NOTHING);

    if(failed(initStage(GS_DEFAULT_FOV, &viewPort)))
    {
        log << "Default FOV stage initialization failed.";
        return GS_FAIL__DIRECT3D9__INIT_STAGE;
    }

    log << "Default FOV stage initialization succeeded.";

    if(failed(setMode(GS_PROJECTION__PERSPECTIVE)))
        return GS_FAIL_DIRECT3D9__SET_MODE;

    setWorldTransform(nullptr);

    log << "oneTimeInit() succeeded.";

    return GS_OK;
}

void GsDirect3D9::setWorldTransform(
    const GsMatrix* newMatrixWorld)
{
    vertexCacheManager->forceFlushAll();

    if(!newMatrixWorld)
    {
        GsMatrix matrix;
        matrix.setIdentity();

        matrixWorld = fromGsMatrix(matrix);
    }
    else
        matrixWorld = fromGsMatrix(*newMatrixWorld);

    calculateWorldViewProjectionMatrix();

    if(canDoShaders)
    {
        D3DXMATRIXA16 matView, matProj;
        device->GetTransform(D3DTS_VIEW, &matView);
        device->GetTransform(D3DTS_PROJECTION, &matProj);

        D3DXMATRIXA16 matWorldViewProj = matrixWorld * matView * matProj;

        for(UINT i = 0; i < vertexShadersNumber; i++)
        {
            if(FAILED(vertexShaderConstantTables[i]->SetMatrix(device, "matrixWorldViewProjection", &matWorldViewProj)))
                log << "Failed to set world-view-projection matrix in vertex shader constant table.";

            if(FAILED(vertexShaderConstantTables[i]->SetMatrix(device, "matrixWorld", &matrixWorld)))
                log << "Failed to set world matrix in vertex shader constant table.";
        }
    }

    device->SetTransform(D3DTS_WORLD, &matrixWorld);
}

void GsDirect3D9::prepareShaders()
{
    D3DCAPS9 d3dCapabilities;

    if(FAILED(device->GetDeviceCaps(&d3dCapabilities)))
    {
        log << "Failed GetDeviceCaps() in prepareShaders().";
        canDoShaders = false;
        return;
    }

    if(d3dCapabilities.VertexShaderVersion < D3DVS_VERSION(1, 1))
    {
        log << "Warning: vertex shader version < 1.1 so not supported.";
        canDoShaders = false;
        return;
    }

    if(d3dCapabilities.PixelShaderVersion < D3DPS_VERSION(1, 1))
    {
        log << "Warning: vertex shader version < 1.1 so not supported.";
        canDoShaders = false;
        return;
    }


    D3DVERTEXELEMENT9 declarationVertexPositionOnlyFormat[] =
    {
        { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
        D3DDECL_END()
    };

    D3DVERTEXELEMENT9 declarationVertexPositionNormalTextureFormat[] =
    {
        { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
        { 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
        { 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
        D3DDECL_END()
    };

    D3DVERTEXELEMENT9 declarationVertexPositionColorTextureFormat[] =
    {
        { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
        { 0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
        { 0, 16, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
        D3DDECL_END()
    };

    D3DVERTEXELEMENT9 declarationVertexPositionNormalTexture2BonesFormat[] =
    {
        { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
        { 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
        { 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
        { 0, 32, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
        D3DDECL_END()
    };

    D3DVERTEXELEMENT9 declarationVertexPositionNormal3TexturesFormat[] =
    {
        { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
        { 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
        { 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
        { 0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1 },
        { 0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2 },
        D3DDECL_END()
    };

    D3DVERTEXELEMENT9 declarationVertexPositionNormalTextureSomethingFormat[] =
    {
        { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
        { 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
        { 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
        { 0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 },
        D3DDECL_END()
    };

    device->CreateVertexDeclaration(declarationVertexPositionOnlyFormat, &declarationVertexPositionOnly);
    device->CreateVertexDeclaration(declarationVertexPositionNormalTextureFormat, &declarationVertexPositionNormalTexture);
    device->CreateVertexDeclaration(declarationVertexPositionColorTextureFormat, &declarationVertexPositionColorTexture);
    device->CreateVertexDeclaration(declarationVertexPositionNormalTexture2BonesFormat, &declarationVertexPositionNormalTexture2Bones);
    device->CreateVertexDeclaration(declarationVertexPositionNormal3TexturesFormat, &declarationVertexPositionNormal3Textures);
    device->CreateVertexDeclaration(declarationVertexPositionNormalTextureSomethingFormat, &declarationVertexPositionNormalTextureSomething);

    device->SetFVF(NULL);

    canDoShaders = true;
    log << "Using shaders activated.";
}

GsResult GsDirect3D9::createVertexShader(
    const void* data,
    UINT size,
    bool loadFromFile,
    bool isCompiled,
    UINT* index)
{
    LPD3DXBUFFER code = nullptr;
    LPD3DXBUFFER debug = nullptr;
    HRESULT resultC, resultA;
    DWORD* shader = nullptr;
    HANDLE file, map;

    if(vertexShadersNumber >= GS_MAX_SHADERS_NUMBER - 1)
    {
        log << "Error: max number of vertex shaders is reached.";
        return GS_FAIL__OUT_OF_MEMORY;
    }

    if(isCompiled)
    {
        if(loadFromFile)
        {
            file = CreateFileA((LPCSTR)data, GENERIC_READ, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

            if(file == INVALID_HANDLE_VALUE)
            {
                log << "Failed createFile() in GsDirect3D9::createVertexShader().";
                return GS_FAIL__DIRECT3D9__CREATE_VERTEX_SHADER__FILE_NOT_FOUND;
            }

            map = CreateFileMappingA(file, 0, PAGE_READONLY, 0, 0, 0);
            shader = (DWORD*)MapViewOfFile(map, FILE_MAP_READ, 0, 0, 0);
        }
        else
            shader = (DWORD*)data;
    }
    else
    {
        if(loadFromFile)
            resultA = D3DXAssembleShaderFromFileA((LPCSTR)data, nullptr, nullptr, 0, &code, &debug);
        else
            resultA = D3DXAssembleShader((char*)data, size - 1, nullptr, nullptr, 0, &code, &debug);

        if(SUCCEEDED(resultA))
        {
            shader = (DWORD*)code->GetBufferPointer();
            log << "Vertex shader assemble succeeded.";
        }
        else
        {
            log << "Failed to assemble vertex shader.";

            if(resultA == D3DERR_INVALIDCALL)
                log << "Invalid call.";
            else
                if(resultA == D3DXERR_INVALIDDATA)
                    log << "Invalid data.";
                else
                    if(resultA == E_OUTOFMEMORY)
                        log << "Out of memory.";
                    else
                        log << "Unknown error.";

            if(debug->GetBufferPointer())
                log << "Shader debug: " + std::string((char*)debug->GetBufferPointer());
            else
                log << "No shader debug information stored.";

            return GS_FAIL__DIRECT3D9__CREATE_VERTEX_SHADER__ASSEMBLE_VERTEX_SHADER;
        }
    }

    if(FAILED(resultC = device->CreateVertexShader(shader, &vertexShaders[vertexShadersNumber])))
    {
        log << "Failed CreateVertexShader().";

        if(resultC == D3DERR_INVALIDCALL)
            log << "Invalid call.";

        if(resultC == D3DERR_OUTOFVIDEOMEMORY)
            log << "Out of video memory.";

        if(resultC == E_OUTOFMEMORY)
            log << "Out of memory.";

        return GS_FAIL__DIRECT3DDEVICE9__CREATE_VERTEX_SHADER;
    }

    log << "Creating vertex shader succeeded.";

    if(index)
        *index = vertexShadersNumber;

    vertexShadersNumber++;

    if(isCompiled && loadFromFile)
    {
        UnmapViewOfFile(shader);
        CloseHandle(map);
        CloseHandle(file);
    }

    return GS_OK;
}

GsResult GsDirect3D9::createVertexShader(
    const char* fileName,
    UINT* index)
{
    HRESULT result;
    LPD3DXBUFFER code = nullptr;
    LPD3DXBUFFER errorMessages = nullptr;

    if(FAILED(result = D3DXCompileShaderFromFileA((LPCSTR)fileName, nullptr, nullptr, (LPCSTR)"main", (LPCSTR)"vs_2_0", D3DXSHADER_OPTIMIZATION_LEVEL3, &code, &errorMessages,
                       &vertexShaderConstantTables[vertexShadersNumber])))
    {
        log << "Error: invalid vertex shader '" + std::string(fileName) + "' code.";

        if(code)
            code->Release();

        switch(result)
        {
            case D3DERR_INVALIDCALL:
                log << "Invalid call.";
                break;

            case D3DXERR_INVALIDDATA:
                log << "Invalid data.";
                break;

            case E_OUTOFMEMORY:
                log << "Out of memory.";
                break;

            case E_NOTIMPL:
                log << "Not implemented.";
                break;

            default:
                log << (const char*)errorMessages->GetBufferPointer();
        }

        return GS_FAIL;
    }

    if(FAILED(device->CreateVertexShader((DWORD*)code->GetBufferPointer(), &vertexShaders[vertexShadersNumber])))
    {
        log << "Vertex shader '" + std::string(fileName) + "' creation failed.";

        if(code)
            code->Release();

        return GS_FAIL;
    }

    log << "Vertex shader '" + std::string(fileName) + "' creation succeeded.";

    if(index)
        *index = vertexShadersNumber;

    vertexShadersNumber++;
    code->Release();

    return GS_OK;
}

GsResult GsDirect3D9::activateVertexShader(
    UINT index,
    GsVertexType vertexType)
{
    if(!canDoShaders)
    {
        log << "Vertex shader activation failed.";
        log << "No shader support.";
        return GS_FAIL__DIRECT3D9__ACTIVATE_VERTEX_SHADER__NO_SHADER_SUPPORT;
    }

	if (index == -1)
		return GS_OK;

    if(index >= vertexShadersNumber)
    {
        log << "Vertex shader activation failed.";
        log << "Invalid shader index.";
        return GS_FAIL__DIRECT3D9_ACTIVATE_VERTEX_SHADER__INVALID_SHADER_ID;
    }

    vertexCacheManager->forceFlushAll();

    HRESULT result;

    switch(vertexType)
    {
        case GS_VERTEX__UNTRANSFORMED_POSITION_ONLY:
            result = device->SetVertexDeclaration(declarationVertexPositionOnly);
            break;

        case GS_VERTEX__UNTRANSFORMED_UNLIT:
            result = device->SetVertexDeclaration(declarationVertexPositionNormalTexture);
            break;

        case GS_VERTEX__UNTRANSFORMED_LIT:
            result = device->SetVertexDeclaration(declarationVertexPositionColorTexture);
            break;

        case GS_VERTEX__CHARACTER_ANIMATION:
            result = device->SetVertexDeclaration(declarationVertexPositionNormalTexture2Bones);
            break;

        case GS_VERTEX__THREE_TEXTURE_COORD_PAIRS:
            result = device->SetVertexDeclaration(declarationVertexPositionNormal3Textures);
            break;

        case GS_VERTEX__UNTRANSFORMED_UNLIT_WITH_TANGENT_VECTOR:
            result = device->SetVertexDeclaration(declarationVertexPositionNormalTextureSomething);
            break;

        default:
            result =  GS_FAIL__DIRECT3D9__ACTIVATE_VERTEX_SHADER__INVALID_VERTEX_TYPE;
    }

    switch(result)
    {
        case GS_FAIL__DIRECT3DDEVICE9__SET_VERTEX_DECLARATION:
            log << "Vertex shader activation failed.";
            log << "SetVertexDeclaration() failed.";
            break;

        case GS_FAIL__DIRECT3D9__ACTIVATE_VERTEX_SHADER__INVALID_VERTEX_TYPE:
            log << "Vertex shader activation failed.";
            log << "Invalid vertex type.";
            break;

        default:
            break;
    }


    if(FAILED(device->SetVertexShader(vertexShaders[index])))
    {
        log << "Vertex shader activation failed.";
        log << "SetVertexShader() failed.";
        return GS_FAIL__DIRECT3DDEVICE9__SET_VERTEX_SHADER;
    }

    activeVertexShaderIndex = index;
    useShaders = true;

    return GS_OK;
}


GsResult GsDirect3D9::createPixelShader(
    const void* data,
    UINT size,
    bool loadFromFile,
    bool isCompiled,
    UINT* index)
{
    LPD3DXBUFFER code = nullptr;
    LPD3DXBUFFER debug = nullptr;
    HRESULT resultC, resultA;
    DWORD* shader = nullptr;
    HANDLE file, map;

    if(pixelShadersNumber >= GS_MAX_SHADERS_NUMBER)
    {
        log << "Error: max pixel shaders is riched.";
        return GS_FAIL__OUT_OF_MEMORY;
    }

    if(isCompiled)
    {
        if(loadFromFile)
        {
            file = CreateFileA((LPCSTR)data, GENERIC_READ, 0, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

            if(file == INVALID_HANDLE_VALUE)
            {
                log << "Failed: CreateFile() in CreatePixelShader().";
                return GS_FAIL__DIRECT3D9__CREATE_PIXEL_SHADER__FILE_NOT_FOUND;
            }

            map = CreateFileMappingA(file, 0, PAGE_READONLY, 0, 0, 0);
            shader = (DWORD*)MapViewOfFile(map, FILE_MAP_READ, 0, 0, 0);
        }
        else
            shader = (DWORD*)data;
    }
    else
    {
        if(loadFromFile)
            resultA = D3DXAssembleShaderFromFileA((LPCSTR)(char*)data, nullptr, nullptr, 0, &code, &debug);
        else
            resultA = D3DXAssembleShader((char*)data, size - 1, nullptr, nullptr, 0, &code, &debug);

        if(SUCCEEDED(resultA))
            shader = (DWORD*)code->GetBufferPointer();
        else
        {
            log << "Failed to assemble pixel shader.";

            if(resultA == D3DERR_INVALIDCALL)
                log << "Invalid call.";

            if(resultA == D3DXERR_INVALIDDATA)
                log << "Invalid data.";

            if(resultA == E_OUTOFMEMORY)
                log << "Out of memory.";

            if(debug->GetBufferPointer())
                log << "Shader debug information: " + std::string((char*)debug->GetBufferPointer());
            else
                log << "No shader debug information stored.";

            return GS_FAIL__DIRECT3D9__CREATE_PIXEL_SHADER__ASSEMBLE_SHADER;
        }
    }

    if(FAILED(resultC = device->CreatePixelShader(shader, &pixelShaders[pixelShadersNumber])))
    {
        log << "Failed CreatePixelShader().";

        if(resultC == D3DERR_INVALIDCALL)
            log << "Invalid call.";

        if(resultC == D3DERR_OUTOFVIDEOMEMORY)
            log << "Out of video memory.";

        if(resultC == E_OUTOFMEMORY)
            log << "Out of memory.";

        return GS_FAIL__DIRECT3DDEVICE9__CREATE_PIXEL_SHADER;
    }

    if(index)
        *index = pixelShadersNumber;

    pixelShadersNumber++;

    if(isCompiled && loadFromFile)
    {
        UnmapViewOfFile(shader);
        CloseHandle(map);
        CloseHandle(file);
    }

    return GS_OK;
}

GsResult GsDirect3D9::createPixelShader(
    const char* fileName,
    UINT* index)
{
    HRESULT result;
    LPD3DXBUFFER code = nullptr;
    LPD3DXBUFFER errorMessages = nullptr;

    if(FAILED(result = D3DXCompileShaderFromFileA((LPCSTR)fileName, nullptr, nullptr, (LPCSTR)"main", (LPCSTR)"ps_2_0", D3DXSHADER_OPTIMIZATION_LEVEL3, &code, &errorMessages,
                       &pixelShaderConstantTables[pixelShadersNumber])))
    {
        log << "Error: invalid pixel shader '" + std::string(fileName) + "' code.";

        if(code)
            code->Release();

        switch(result)
        {
            case D3DERR_INVALIDCALL:
                log << "Invalid call.";
                break;

            case D3DXERR_INVALIDDATA:
                log << "Invalid data.";
                break;

            case E_OUTOFMEMORY:
                log << "Out of memory.";
                break;

            case E_NOTIMPL:
                log << "Not implemented.";
                break;

            default:
				if (errorMessages)
					log << (const char*)errorMessages->GetBufferPointer();
        }

        return GS_FAIL;
    }

    if(FAILED(device->CreatePixelShader((DWORD*)code->GetBufferPointer(), &pixelShaders[pixelShadersNumber])))
    {
        log << "Pixel shader '" + std::string(fileName) + "' creation failed.";

        if(code)
            code->Release();

        return GS_FAIL;
    }

    log << "Pixel shader '" + std::string(fileName) + "' creation succeeded.";

    if(index)
        *index = pixelShadersNumber;

    pixelShadersNumber++;
    code->Release();

    return GS_OK;
}


GsResult GsDirect3D9::activatePixelShader(
    UINT index)
{
    if(!canDoShaders)
    {
        log << "Pixel shader activation failed.";
        log << "No shader support.";
        return GS_FAIL__DIRECT3D9__ACRTIVATE_PIXEL_SHADER__NO_SHADER_SUPPORT;
    }

	if (index == -1)
		return GS_OK;

    if(index >= pixelShadersNumber)
    {
        log << "Pixel shader activation failed.";
        log << "Invalid shader index.";
        return GS_FAIL__DIRECT3D9__ACTIVATE_PIXEL_SHADER__INVALID_ID;
    }

    vertexCacheManager->forceFlushAll();

    if(FAILED(device->SetPixelShader(pixelShaders[index])))
    {
        log << "Pixel shader activation failed.";
        log << "SetPixelShader() failed.";
        return GS_FAIL__DIRECT3DDEVICE9__SET_PIXEL_SHADER;
    }

    activePixelShaderIndex = index;
    useShaders = true;

    return GS_OK;
}

void GsDirect3D9::setBackFaceCulling(
    GsRenderState state)
{
    vertexCacheManager->forceFlushAll();

    switch(state)
    {
        case GS_RENDER_STATE__CULL_CLOCKWISE:
            device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
            break;

        case GS_RENDER_STATE__CULL_CONTRCLOCKWISE:
            device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
            break;

        case GS_RENDER_STATE__CULL_NONE:
            device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
            break;

        default:
            log << "Wrong call of setBackFaceCulling().";
    }
}

void GsDirect3D9::setDepthBufferMode(
    GsRenderState mode)
{
    vertexCacheManager->forceFlushAll();

    switch(mode)
    {
        case GS_RENDER_STATE__DEPTH_READ_WRITE:
            device->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
            device->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
            break;

        case GS_RENDER_STATE__DEPTH_READ_ONLY:
            device->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
            device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
            break;

        case GS_RENDER_STATE__DEPTH_NONE:
            device->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
            device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
            break;

        default:
            log << "Wrong call of setDepthBufferMode().";
    }
}

void GsDirect3D9::setShadeMode(
    GsRenderState mode,
    float pointSize,
    const GsColor* newWireframeColor)
{
    if(newWireframeColor && !skinManager->colorsAreEqual(wireframeColor, *newWireframeColor))
    {
        vertexCacheManager->forceFlushAll();
        vertexCacheManager->invalidateStates();
        memcpy(&wireframeColor, newWireframeColor, sizeof(GsColor));
    }

    if(mode == shadeMode)
    {
        if(mode == GS_RENDER_STATE__RENDER_JUST_VERTICES)
        {
            vertexCacheManager->forceFlushAll();
            device->SetRenderState(D3DRS_POINTSIZE, toDWORD(pointSize));
        }

        return;
    }
    else
        vertexCacheManager->forceFlushAll();

    if(mode == GS_RENDER_STATE__RENDER_TRIANGULATED_WIREFRAMES)
    {
        device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
        device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_FLAT);
        shadeMode = mode;
    }
    else
    {
        if(mode != GS_RENDER_STATE__RENDER_SOLID_POLYGONS)
            device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_FLAT);

        device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
        device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
        shadeMode = mode;
    }

    if(mode == GS_RENDER_STATE__RENDER_JUST_VERTICES)
    {
        if(pointSize > 0.0)
        {
            device->SetRenderState(D3DRS_POINTSPRITEENABLE, TRUE);
            device->SetRenderState(D3DRS_POINTSCALEENABLE, TRUE);
            device->SetRenderState(D3DRS_POINTSIZE, toDWORD(pointSize));
            device->SetRenderState(D3DRS_POINTSIZE_MIN, toDWORD(0.0));
            device->SetRenderState(D3DRS_POINTSCALE_A, toDWORD(0.0));
            device->SetRenderState(D3DRS_POINTSCALE_B, toDWORD(0.0));
            device->SetRenderState(D3DRS_POINTSCALE_C, toDWORD(1.0));
        }
        else
        {
            device->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
            device->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
        }
    }
    else
    {
        device->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
        device->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
    }

    vertexCacheManager->invalidateStates();
}

DWORD GsDirect3D9::toDWORD(
    float real)
{
    return *((DWORD*) & ((float)real));
}

GsResult GsDirect3D9::createFont(
    const char* type,
    int weight,
    bool italic,
    bool underline,
    bool striked,
    DWORD size,
    UINT* index)
{
    HRESULT result;

    fonts = (LPD3DXFONT*)realloc(fonts, sizeof(LPD3DXFONT) * (fontsNumber + 1));

    result = D3DXCreateFont(device, size, 0, weight, 0, italic, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH | FF_DONTCARE, (LPCTSTR)type,
                            &fonts[fontsNumber]);
    result = D3DXCreateFontA(device, 22, 0, FW_NORMAL, 1, italic, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLEARTYPE_NATURAL_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Arial",
                             &fonts[0]);

    if(FAILED(result))
    {
        log << "Creating font failed.";
        return GS_FAIL;
    }

    if(index)
        *index = fontsNumber;

    fontsNumber++;

    log << "Creating font succeeded.";
    return GS_OK;
}

GsResult GsDirect3D9::drawText(
    UINT fontIndex,
    int x,
    int y,
    GsColor color,
    const char* text, ...)
{
    D3DRECT rectangle;
    rectangle.x1 = x;
    rectangle.x2 = x + 100;
    rectangle.y1 = y;
    rectangle.y2 = y + 100;

    char fullText[1024];
    char* args;

    args = (char*)&text + sizeof(text);
    vsprintf(fullText, text, args);

    if(fontIndex >= fontsNumber)
        return GS_FAIL__INVALID_PARAMETER;

    if(!fonts[fontIndex]->DrawTextA(nullptr, (LPCSTR)std::string(fullText).data(), -1, (LPRECT)&rectangle, DT_LEFT | DT_NOCLIP, D3DCOLOR_ARGB(UCHAR(color.alpha * 255),
                                    UCHAR(color.red * 255), UCHAR(color.green * 255), UCHAR(color.blue * 255))))
        return GS_FAIL;

    return GS_OK;
}

void GsDirect3D9::logDeviceCapabilities(
    D3DCAPS9& capabilities)
{
    log << "Graphic adapter info: ";

    if(capabilities.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
    {
        log << "  adapter features hardware transform and light";

        if(capabilities.DevCaps & D3DDEVCAPS_PUREDEVICE)
            log << "  (pure device possible)";
        else
            log << "  (no pure device possible)";
    }
    else
        log << "  adapter only features software transform and light";

    log << "  max texture stages: " + std::to_string(capabilities.MaxTextureBlendStages);
    log << "  max textures for single pass: " + std::to_string(capabilities.MaxSimultaneousTextures);
    log << "  max texture width: " + std::to_string(capabilities.MaxTextureWidth);
    log << "  max texture height: " + std::to_string(capabilities.MaxTextureHeight);


    if(capabilities.VertexShaderVersion < D3DVS_VERSION(1, 1))
        log << "  vertex shader version 1.0";
    else
        if(capabilities.VertexShaderVersion < D3DVS_VERSION(2, 0))
            log << "  vertex shader version 1.1";
        else
            log << "  vertex shader version 2.0 or better";

    if(capabilities.PixelShaderVersion < D3DPS_VERSION(1, 1))
        log << "  pixel shader version 1.0";
    else
        if(capabilities.PixelShaderVersion < D3DPS_VERSION(1, 2))
            log << "  pixel shader version 1.1";
        else
            if(capabilities.PixelShaderVersion < D3DPS_VERSION(1, 3))
                log << "  pixel shader version 1.2";
            else
                if(capabilities.PixelShaderVersion < D3DPS_VERSION(1, 4))
                    log << "  pixel shader version 1.3";
                else
                    if(capabilities.PixelShaderVersion < D3DPS_VERSION(2, 0))
                        log << "  pixel shader version 1.4";
                    else
                        log << "  pixel shader version 2.0 or better";

    log << "Display info: ";

    LPDIRECT3DSURFACE9 depthStencil = nullptr;
    D3DSURFACE_DESC desc;
    D3DFORMAT format = D3DFMT_UNKNOWN;
    D3DDISPLAYMODE mode = { 0, 0, 0, D3DFMT_UNKNOWN };

    if(FAILED(direct3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mode)))
        log << "Error: Direct3D::GetAdapterDisplayMode() failed.";

    if(FAILED(device->GetDepthStencilSurface(&depthStencil)))
        log << "Error: Direct3DDevice::GetDepthStencilSurface failed.";
    else
        if(FAILED(depthStencil->GetDesc(&desc)))
        {
            log << "Error: Direct3DSurface::GetDesk() failed.";
            format = D3DFMT_UNKNOWN;
        }

    log << "  resolution: " + std::to_string(mode.Width) + "x" + std::to_string(mode.Height);
    log << "  refresh rate: " + std::to_string(mode.RefreshRate);

    switch(mode.Format)
    {
        case D3DFMT_A2R10G10B10:
            log << "  pixel format: A2R10G10B10";
            break;

        case D3DFMT_A8R8G8B8:
            log << "  pixel format: A8R8G8B8";
            break;

        case D3DFMT_X8R8G8B8:
            log << "  pixel format: X8R8G8B8";
            break;

        case D3DFMT_A1R5G5B5:
            log << "  pixel format: A1R5G5B5";
            break;

        case D3DFMT_X1R5G5B5:
            log << "  pixel format: X1R5G5B5";
            break;

        case D3DFMT_R5G6B5:
            log << "  pixel format: R5G6B5";
            break;

        default:
            log << "  pixel format: UNKNOWN";
    }

    switch(desc.Format)
    {
        case D3DFMT_D16_LOCKABLE:
            log << "  depth / stencil format: D16_LOCKABLE";
            break;

        case D3DFMT_D32F_LOCKABLE:
            log << "  depth / stencil format: D32F_LOCKABLE";
            break;

        case D3DFMT_D32:
            log << "  depth / stencil format: D32";
            break;

        case D3DFMT_D15S1:
            log << "  depth / stencil format: D15S1";
            break;

        case D3DFMT_D24S8:
            log << "  depth / stencil format: D24S8";
            break;

        case D3DFMT_D24X8:
            log << "  depth / stencil format: D24X8";
            break;

        case D3DFMT_D24X4S4:
            log << "  depth / stencil format: D24X4S4";
            break;

        case D3DFMT_D24FS8:
            log << "  depth / stencil format: D24FS8";
            break;

        case D3DFMT_D16:
            log << "  depth / stencil format: D16";
            break;

        default:
            log << "  depth / stencil format: UNKNOWN";
    }

    depthStencil->Release();
}

void GsDirect3D9::setAmbientLightColor(
    const GsColor& color)
{
    vertexCacheManager->forceFlushAll();

    if(canDoShaders)
    {
        float colors[GS_BASIC_COLORS_NUMBER + 1] = { color.red, color.green, color.blue, color.alpha };
        D3DXVECTOR4 colorForShaders = D3DXVECTOR4(color.red, color.green, color.blue, color.alpha);

        for(UINT i = 0; i < pixelShadersNumber; i++)
            registerConstantFloat4ForShader(GS_SHADER__PIXEL, "colorAmbient", (float*)&color, activePixelShaderIndex);

        //device->SetVertexShaderConstantF(4, colors, 1);
    }

    int red = (int)(color.red * 255.0);
    int green = (int)(color.green * 255.0);
    int blue = (int)(color.blue * 255.0);

    device->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(red, green, blue));
}

void GsDirect3D9::fadeScreen(
    GsColor color)
{
    GsEngineMode oldMode;
    GsVertexPositionColorTexture v[4];
    bool changed = false;
    WORD wI[6] = { 0, 1, 2, 0, 3, 1 };
    DWORD dwColor = D3DCOLOR_COLORVALUE(color.red, color.green, color.blue, color.alpha);

    if(isSceneRunning())
        device->EndScene();

    setActiveSkinIndex(GS_MAX_ID);

    int width, height;

    if(windowed)
    {
        width = windowWidth;
        height = windowHeight;
    }
    else
    {
        width = screenWidth;
        height = screenHeight;
    }

    v[0].x = (float)width;
    v[0].y = 0.0;
    v[0].z = 1.0;
    v[0].tu = 1.0;
    v[0].tv = 0.0;
    v[0].color = dwColor;

    v[1].x = 0.0;
    v[1].y = (float)height;
    v[1].z = 1.0;
    v[1].tu = 0.0;
    v[1].tv = 1.0;
    v[1].color = dwColor;

    v[2].x = 0.0;
    v[2].y = 0.0;
    v[2].z = 1.0;
    v[2].tu = 0.0;
    v[2].tv = 0.0;
    v[2].color = dwColor;

    v[3].x = (float)width;
    v[3].y = (float)height;
    v[3].z = 1.0;
    v[3].tu = 1.0;
    v[3].tv = 1.0;
    v[3].color = dwColor;

    setWorldTransform(nullptr);
    setView3D(GsVector(1.0, 0.0, 0.0), GsVector(0.0, 1.0, 0.0), GsVector(0.0, 0.0, 1.0), GsVector(0.0, 0.0, 0.0));


    setUseShaders(false);

    device->SetTexture(0, nullptr);

    device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
    device->SetFVF(FVF_LVERTEX);

    if(mode != GS_PROJECTION__2D)
    {
        changed = true;
        oldMode = mode;

        setMode(GS_PROJECTION__2D);
    }

    D3DMATERIAL9 material;
    memset(&material, 0, sizeof(D3DMATERIAL9));
    material.Diffuse.r = material.Ambient.r = 1.0;
    material.Diffuse.g = material.Ambient.g = 1.0;
    material.Diffuse.b = material.Ambient.b = 1.0;
    material.Diffuse.a = material.Ambient.a = 1.0;
    device->SetMaterial(&material);

    device->SetRenderState(D3DRS_LIGHTING, FALSE);

    device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
    device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
    device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
    device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
    device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
    device->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

    setDepthBufferMode(GS_RENDER_STATE__DEPTH_NONE);

    device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_FLAT);

    device->BeginScene();

    if(FAILED(device->DrawIndexedPrimitiveUP(D3DPT_TRIANGLELIST, 0, 4, 2, wI, D3DFMT_INDEX16, v, sizeof(GsVertexPositionColorTexture))))
        log << "Failed: DrawPrimitiveUP() in fadeScreen().";

    device->EndScene();

    if(changed)
        setMode(oldMode);

    device->SetMaterial(&standardMaterial);

    device->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_CURRENT);
    device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
    device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    device->SetRenderState(D3DRS_LIGHTING, TRUE);
    device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);

    setDepthBufferMode(GS_RENDER_STATE__DEPTH_READ_WRITE);

    if(isSceneRunning())
        device->BeginScene();
}

void GsDirect3D9::calculateOrthogonalProjectionMatrix(
    float left,
    float right,
    float bottom,
    float top,
    float distanceNear,
    float distanceFar)
{
    float x = 2.0 / (right - left);
    float y = 2.0 / (top - bottom);
    float z = 2.0 / (distanceFar - distanceNear);

    float tx = -(right + left) / (right - left);
    float ty = -(top + bottom) / (top - bottom);
    float tz = -(distanceFar + distanceNear) / (distanceFar - distanceNear);

    memset(&matrixProjectionOrthogonal, 0, sizeof(D3DMATRIX));
    matrixProjectionOrthogonal._11 = x;
    matrixProjectionOrthogonal._22 = y;
    matrixProjectionOrthogonal._33 = z;
    matrixProjectionOrthogonal._44 = 1.0;
    matrixProjectionOrthogonal._41 = tx;
    matrixProjectionOrthogonal._42 = ty;
    matrixProjectionOrthogonal._43 = tz;
}

void GsDirect3D9::setOrthoScale(
    float scale)
{
    int width, height;

    if(windowed)
    {
        width = windowWidth;
        height = windowHeight;
    }
    else
    {
        width = screenWidth;
        height = screenHeight;
    }

    float w = ((float)width) / height * scale;
    float h = scale;

    memset(&matrixProjectionOrthogonal, 0, sizeof(D3DMATRIX));
    matrixProjectionOrthogonal._11 = 2.0 / w;
    matrixProjectionOrthogonal._22 = 2.0 / h;
    matrixProjectionOrthogonal._33 = 1.0 / (clippingDistanceFar - clippingDistanceNear);
    matrixProjectionOrthogonal._43 = clippingDistanceNear / (clippingDistanceNear - clippingDistanceFar);
    matrixProjectionOrthogonal._44 = 1.0;
}

GsVector GsDirect3D9::transform2Dto2D(
    float scale,
    const GsPoint* point,
    GsAxis axis)
{
    GsVector result(0.0, 0.0, 0.0);
    POINT c = { -1, -1 };
    RECT rectangle;

    if(!presentParameters.Windowed)
        return result;
    else
        if(swapChains[0] == nullptr)
            return result;

    GetClientRect(window, &rectangle);

    if(!point)
    {
        GetCursorPos(&c);
        ScreenToClient(window, &c);
    }
    else
        memcpy(&c, point, sizeof(POINT));

    c.x -= long(((float)rectangle.right) / 2.0);
    c.y -= long(((float)rectangle.bottom) / 2.0);

    switch(axis)
    {
        case GS_AXIS__X:
            result.x = 0.0;
            result.y = -((float)c.y) / rectangle.bottom * scale;
            result.z = ((float)c.x) / rectangle.bottom * scale;
            break;

        case GS_AXIS__Y:
            result.x = ((float)c.x) / rectangle.bottom * scale;
            result.y = 0.0;
            result.z = ((float)c.y) / rectangle.bottom * scale;
            break;

        case GS_AXIS__Z:
            result.x = ((float)c.x) / rectangle.bottom * scale;
            result.y = -((float)c.y) / rectangle.bottom * scale;
            result.z = 0.0;
            break;

        default:
            log << "Error: unkonwn axis used in transform2Dto2D().";
    }

    return result;
}

void GsDirect3D9::setUseShaders(
    bool use)
{
    if(!canDoShaders)
        return;

    if(useShaders == use)
        return;

    vertexCacheManager->forceFlushAll();
    vertexCacheManager->invalidateStates();

    useShaders = use;

	if (!useShaders)
    {
        device->SetVertexShader(nullptr);
        device->SetPixelShader(nullptr);
        device->SetVertexDeclaration(nullptr);
    }
    else
        device->SetFVF(0);
}

GsResult GsDirect3D9::setShaderConstant(
    GsShaderType shaderType,
    GsDataType dataType,
    UINT reg,
    UINT number,
    const void* data)
{
    if(!canDoShaders)
        return GS_FAIL;

    switch(shaderType)
    {
        case GS_SHADER__VERTEX:
            if(reg < 20)
                return GS_FAIL__INVALID_PARAMETER;

            switch(dataType)
            {
                case GS_DATA__BOOL:
                    device->SetVertexShaderConstantB(reg, (BOOL*)data, number);
                    break;

                case GS_DATA__INT:
                    device->SetVertexShaderConstantI(reg, (int*)data, number);
                    break;

                case GS_DATA__FLOAT:
                    device->SetVertexShaderConstantF(reg, (float*)data, number);
                    break;

                default:
                    return GS_FAIL__INVALID_PARAMETER;
            }

            break;

        case GS_SHADER__PIXEL:
            switch(dataType)
            {
                case GS_DATA__BOOL:
                    device->SetPixelShaderConstantB(reg, (BOOL*)data, number);
                    break;

                case GS_DATA__INT:
                    device->SetPixelShaderConstantI(reg, (int*)data, number);
                    break;

                case GS_DATA__FLOAT:
                    device->SetPixelShaderConstantF(reg, (float*)data, number);
                    break;

                default:
                    return GS_FAIL__INVALID_PARAMETER;
            }

            break;

        case GS_SHADER__VIDEO_SHADER_MOTHERFUCKERRRR:
            log << "Video shader is not implemented just now. But who knows...";
            return GS_FAIL;
            break;

        default:
            return GS_FAIL__INVALID_PARAMETER;
    }

    return GS_OK;
}

void GsDirect3D9::setUseColorBuffer(
    bool use)
{
    vertexCacheManager->forceFlushAll();
    vertexCacheManager->invalidateStates();

    useColorBuffer = use;

    if(!useColorBuffer)
    {
        device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
        device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
        device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
    }
    else
    {
        device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
        device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
        device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
    }
}

void GsDirect3D9::setStencilBufferMode(
    GsRenderState state,
    DWORD mode)
{
    vertexCacheManager->forceFlushAll();

    switch(state)
    {
        case GS_RENDER_STATE__STENCIL_DISABLED:
            device->SetRenderState(D3DRS_STENCILENABLE, FALSE);
            break;

        case GS_RENDER_STATE__STENCIL_ENABLED:
            device->SetRenderState(D3DRS_STENCILENABLE, TRUE);
            break;

        case GS_RENDER_STATE__DEPTH_BIAS:
            device->SetRenderState(D3DRS_DEPTHBIAS, mode);

        case GS_RENDER_STATE__STENCIL_FUNC_ALWAYS:
            device->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
            break;

        case GS_RENDER_STATE__STENCIL_FUNC_LESS_EQUAL:
            device->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_LESSEQUAL);
            break;

        case GS_RENDER_STATE__STENCIL_MASK:
            device->SetRenderState(D3DRS_STENCILMASK, mode);
            break;

        case GS_RENDER_STATE__STENCIL_WRITE_MASK:
            device->SetRenderState(D3DRS_STENCILWRITEMASK, mode);
            break;

        case GS_RENDER_STATE__STENCIL_REFERENCE:
            device->SetRenderState(D3DRS_STENCILREF, mode);
            break;

        case GS_RENDER_STATE__STENCIL_FAIL_DECREMENTS:
            device->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_DECR);
            break;

        case GS_RENDER_STATE__STENCIL_FAIL_INCREMENTS:
            device->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_INCR);
            break;

        case GS_RENDER_STATE__STENCIL_FAIL_KEEP:
            device->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
            break;

        case GS_RENDER_STATE__STENCIL_Z_FAIL_DECREMENTS:
            device->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_DECR);
            break;

        case GS_RENDER_STATE__STENCIL_Z_FAIL_INCREMENTS:
            device->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_INCR);
            break;

        case GS_RENDER_STATE__STENCIL_Z_FAIL_KEEP:
            device->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
            break;

        case GS_RENDER_STATE__STENCIL_PASS_DECREMENTS:
            device->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_DECR);
            break;

        case GS_RENDER_STATE__STENCIL_PASS_INCREMENTS:
            device->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_INCR);
            break;

        case GS_RENDER_STATE__STENCIL_PASS_KEEP:
            device->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
            break;

        default:
            log << "Invalid parameter in setStencilBufferMode().";
    }
}

void GsDirect3D9::setUseStencilShadowSettings(
    bool use)
{
    vertexCacheManager->invalidateStates();

    if(use)
    {
        device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
        device->SetRenderState(D3DRS_STENCILENABLE, TRUE);
        device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_FLAT);
        device->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
        device->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
        device->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
        device->SetRenderState(D3DRS_STENCILREF, 0x1);
        device->SetRenderState(D3DRS_STENCILMASK, 0xffffffff);
        device->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
        device->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_INCR);

        setUseColorBuffer(false);
    }
    else
    {
        device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
        device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
        device->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
        device->SetRenderState(D3DRS_STENCILENABLE, FALSE);
        device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

        setUseColorBuffer(true);
        setMode(mode);
    }
}

GsResult GsDirect3D9::setTextureStage(
    UCHAR index,
    GsRenderState state)
{
    D3DCAPS9 caps;

    device->GetDeviceCaps(&caps);

    if(caps.MaxSimultaneousTextures < (index + 1))
        return GS_FAIL;

    switch(state)
    {
        case GS_RENDER_STATE__NOTHING:
            textureOperations[index] = D3DTOP_DISABLE;
            break;

        case GS_RENDER_STATE__TEXTURES_ADD_SIGNED:
            textureOperations[index] = D3DTOP_ADDSIGNED;
            break;

        case GS_RENDER_STATE__TEXTURES_MODULATE:
            textureOperations[index] = D3DTOP_MODULATE;
            break;

        case GS_RENDER_STATE__TEXTURES_SELECT_FIRST:
            textureOperations[index] = D3DTOP_SELECTARG1;
            break;

        case GS_RENDER_STATE__TEXTURES_SELECT_SECOND:
            textureOperations[index] = D3DTOP_SELECTARG2;
            break;

        default:
            log << "Invalid texture operation.";
            return GS_FAIL;
    }

    return GS_OK;
}

GsResult GsDirect3D9::setLight(
    const GsLight* light,
    UCHAR stage)
{
    D3DLIGHT9 d3dLight;

    if(!light)
    {
        device->LightEnable(stage, FALSE);
        return GS_OK;
    }

    memset(&d3dLight, 0, sizeof(D3DLIGHT9));


    D3DVECTOR direction, position;

    switch(light->type)
    {
        case GS_LIGHT__DIRECTIONAL:
            d3dLight.Type = D3DLIGHT_DIRECTIONAL;
            direction = { light->direction.x, light->direction.y, light->direction.z };
            memcpy(&d3dLight.Direction, &direction, sizeof(D3DVECTOR));
            break;

        case GS_LIGHT__POINT:
            d3dLight.Type = D3DLIGHT_POINT;
            d3dLight.Range = light->range;
            d3dLight.Attenuation1 = 1.0;
            position = { light->position.x, light->position.y, light->position.z };
            memcpy(&d3dLight.Position, &position, sizeof(D3DVECTOR));
            break;

        case GS_LIGHT__SPOT:
            d3dLight.Type = D3DLIGHT_SPOT;
            d3dLight.Range = light->range;
            d3dLight.Falloff = 1.0;
            d3dLight.Theta = light->theta;
            d3dLight.Phi = light->phi;
            d3dLight.Attenuation0 = light->attenuation0;
            d3dLight.Attenuation1 = light->attenuation1;
            d3dLight.Attenuation2 = 1.0;

            direction = { light->direction.x, light->direction.y, light->direction.z };
            memcpy(&d3dLight.Direction, &direction, sizeof(D3DVECTOR));
            position = { light->position.x, light->position.y, light->position.z };
            memcpy(&d3dLight.Position, &position, sizeof(D3DVECTOR));
            break;

        default:
            log << "Error: invalid light type.";
            return GS_FAIL__INVALID_PARAMETER;
    }

    D3DCOLOR ambient = D3DCOLOR_COLORVALUE(light->ambient.red, light->ambient.green, light->ambient.blue, light->ambient.alpha);
    memcpy(&d3dLight.Ambient, &ambient, sizeof(D3DCOLOR));
    D3DCOLOR diffuse = D3DCOLOR_COLORVALUE(light->diffuse.red, light->diffuse.green, light->diffuse.blue, light->diffuse.alpha);
    memcpy(&d3dLight.Diffuse, &diffuse, sizeof(D3DCOLOR));
    D3DCOLOR specular = D3DCOLOR_COLORVALUE(light->specular.red, light->specular.green, light->specular.blue, light->specular.alpha);
    memcpy(&d3dLight.Specular, &specular, sizeof(D3DCOLOR));

    if(FAILED(device->SetLight(stage, &d3dLight)))
        return GS_FAIL;

    if(FAILED(device->LightEnable(stage, TRUE)))
        return GS_FAIL;

    return GS_OK;
}

void GsDirect3D9::setUseAdditiveBlending(
    bool use)
{
    if(useAdditiveBlending == use)
        return;

    vertexCacheManager->forceFlushAll();
    vertexCacheManager->invalidateStates();

    useAdditiveBlending = use;

    if(!useAdditiveBlending)
    {
        device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
        device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
        device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
    }
}

void GsDirect3D9::setUseTextures(
    bool use)
{
    if(useTextures == use)
        return;

    vertexCacheManager->forceFlushAll();
    vertexCacheManager->invalidateStates();

    useTextures = use;
}

GsMatrix GsDirect3D9::fromDirect3DMatrix(
    D3DXMATRIXA16& d3d)
{
    GsMatrix result;

    for(int i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
        for(int j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
            result[i][j] = d3d.m[j][i];

    return result;
}

D3DXMATRIXA16 GsDirect3D9::fromGsMatrix(
    GsMatrix gs)
{
    D3DMATRIX result;

    for(int i = 0; i < GS_DIMENSIONS_NUMBER + 1; i++)
        for(int j = 0; j < GS_DIMENSIONS_NUMBER + 1; j++)
            result.m[i][j] = gs[j][i];

    return result;
}

GsResult GsDirect3D9::registerConstantFloatForShader(
    GsShaderType shaderType,
    const char* name,
    float value,
    UINT shaderIndex)
{
    switch(shaderType)
    {
        case GS_SHADER__VERTEX:
            if(shaderIndex >= vertexShadersNumber)
            {
                log << "Failed to set float constant '" + std::string(name) + "' in vertex shader constant table. Invalid shader index.";
                return GS_FAIL__INVALID_PARAMETER;
            }
            else
                if(FAILED(vertexShaderConstantTables[shaderIndex]->SetFloat(device, name, value)))
                    log << "Failed to set float constant '" + std::string(name) + "' in vertex shader constant table.";

            break;

        case GS_SHADER__PIXEL:
            if(shaderIndex >= pixelShadersNumber)
            {
                log << "Failed to set float constant '" + std::string(name) + "' in pixel shader constant table. Invalid shader index.";
                return GS_FAIL__INVALID_PARAMETER;
            }
            else
                if(FAILED(pixelShaderConstantTables[shaderIndex]->SetFloat(device, name, value)))
                    log << "Failed to set float constant '" + std::string(name) + "' in pixel shader constant table.";

            break;

        case GS_SHADER__VIDEO_SHADER_MOTHERFUCKERRRR:
            log << "Video shaders are not implemented.";
            return GS_FAIL__INVALID_PARAMETER;

        default:
            return GS_FAIL__INVALID_PARAMETER;
    }

    return GS_OK;
}

GsResult GsDirect3D9::registerConstantFloat4ForShader(
    GsShaderType shaderType,
    const char* name,
    float value[4],
    UINT shaderIndex)
{
    switch(shaderType)
    {
        case GS_SHADER__VERTEX:
            if(shaderIndex >= vertexShadersNumber)
            {
                log << "Failed to set float[4] constant '" + std::string(name) + "' in vertex shader constant table. Invalid shader index.";
                return GS_FAIL__INVALID_PARAMETER;
            }
            else
            {
                D3DXVECTOR4 data = { value[0], value[1], value[2], value[3] };

                if(FAILED(vertexShaderConstantTables[shaderIndex]->SetVector(device, name, &data)))
                {
                    log << "Failed to set float[4] constant '" + std::string(name) + "' in vertex shader constant table.";
                    return GS_FAIL;
                }
            }

            break;

        case GS_SHADER__PIXEL:
            if(shaderIndex >= pixelShadersNumber)
            {
                log << "Failed to set float[4] constant '" + std::string(name) + "' in pixel shader constant table. Invalid shader index.";
                return GS_FAIL__INVALID_PARAMETER;
            }
            else
            {
                D3DXVECTOR4 data = { value[0], value[1], value[2], value[3] };

                if(FAILED(pixelShaderConstantTables[shaderIndex]->SetVector(device, name, &data)))
                {
                    log << "Failed to set float[4] constant '" + std::string(name) + "' in pixel shader constant table.";
                    return GS_FAIL;
                }
            }

            break;

        case GS_SHADER__VIDEO_SHADER_MOTHERFUCKERRRR:
            log << "Video shaders are not implemented.";
            return GS_FAIL__INVALID_PARAMETER;

        default:
            return GS_FAIL__INVALID_PARAMETER;
    }

    return GS_OK;
}

GsResult GsDirect3D9::registerConstantFloat3ForShader(
    GsShaderType shaderType,
    const char* name,
    float value[3],
    UINT shaderIndex)
{
    switch(shaderType)
    {
        case GS_SHADER__VERTEX:
            if(shaderIndex >= vertexShadersNumber)
            {
                log << "Failed to set float[3] constant '" + std::string(name) + "' in vertex shader constant table. Invalid shader index.";
                return GS_FAIL__INVALID_PARAMETER;
            }
            else
            {
                if(FAILED(vertexShaderConstantTables[shaderIndex]->SetFloatArray(device, name, value, 3)))
                {
                    log << "Failed to set float[3] constant '" + std::string(name) + "' in vertex shader constant table.";
                    return GS_FAIL;
                }
            }

            break;

        case GS_SHADER__PIXEL:
            if(shaderIndex >= pixelShadersNumber)
            {
                log << "Failed to set float[3] constant '" + std::string(name) + "' in pixel shader constant table. Invalid shader index.";
                return GS_FAIL__INVALID_PARAMETER;
            }
            else
            {
                if(FAILED(pixelShaderConstantTables[shaderIndex]->SetFloatArray(device, name, value, 3)))
                {
                    log << "Failed to set float[3] constant '" + std::string(name) + "' in pixel shader constant table.";
                    return GS_FAIL;
                }
            }

            break;

        case GS_SHADER__VIDEO_SHADER_MOTHERFUCKERRRR:
            log << "Video shaders are not implemented.";
            return GS_FAIL__INVALID_PARAMETER;

        default:
            return GS_FAIL__INVALID_PARAMETER;
    }

    return GS_OK;

}
