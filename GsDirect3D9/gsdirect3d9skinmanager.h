#pragma once


#include "gsdirect3d9_global.h"

#include "../GsGraphics/gsrenderdevice.h"

#include <d3d9.h>


 class GSDIRECT3D9_EXPORT GsDirect3D9SkinManager : public GsSkinManager
{
    public:
        GsDirect3D9SkinManager(
            GsLog& log,
            LPDIRECT3DDEVICE9 device);
        ~GsDirect3D9SkinManager();

		void reset();

        GsResult addSkin(
            const GsColor& ambient,
			const GsColor& diffuse,
			const GsColor& emissive,
            const GsColor& specular,
            float power,
			UINT* index);

        GsResult addTexture(
            UINT skinIndex,
            const char* name,
            bool alphaEnabled,
            float alpha,
            GsColor* colorKeys,
			DWORD colorKeysNumber);

        bool materialsAreEqual(
            const GsMaterial& material1,
            const GsMaterial& material2);

        void logCurrentStatus(
            bool detailed);

        GsTexture& getTexture(
            UINT index);

		GsSkin& getSkin(
			UINT index);

		GsMaterial& getMaterial(
			UINT index);

		const char* getTextureName(
			UINT textureIndex,
			float* alpha,
			GsColor* alphaKeys,
			UCHAR* alphaKeysNumber);

		GsResult addTextureHeightmapAsBump(
			UINT skinIndex,
			const char* name);

		GsResult exchangeTexture(
			UINT skinIndex,
			UINT textureStage,
			const char* name,
			bool alphaEnabled,
			float alpha,
			GsColor* colorKeys,
			DWORD colorKeysNumber);

		GsResult exchangeMaterial(
			UINT skinIndex,
			const GsColor& ambient,
			const GsColor& diffuse,
			const GsColor& emissive,
			const GsColor& specular,
			float specularPower);

		UINT getSkinsNumber();


    protected:
        LPDIRECT3DDEVICE9 device;

        inline bool colorsAreEqual(
            const GsColor& color1,
            const GsColor& color2);

        GsResult createTexture(
            GsTexture& texture,
            bool alphaEnabled);

		GsResult convertToNormalMap(
			GsTexture& texture);

        GsResult setAlphaKey(
            LPDIRECT3DTEXTURE9& texture,
            UCHAR R,
            UCHAR G,
            UCHAR B,
            UCHAR A);

        GsResult setTransparency(
            LPDIRECT3DTEXTURE9& texture,
            UCHAR alpha);

        DWORD makeD3DColor(
            UCHAR R,
            UCHAR G,
            UCHAR B,
            UCHAR A);

		DWORD vectorToRGBA(
			GsVector& vector,
			float height);
};