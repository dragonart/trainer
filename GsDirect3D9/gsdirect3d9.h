#pragma once

#include "gsdirect3d9_global.h"

#include "../GsGraphics/gsrenderdevice.h"
#include "../GsTools/gslog.h"

#include <d3d9.h>
#include <d3dx9.h>


#define FVF_VERTEX  (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1)
#define FVF_PVERTEX (D3DFVF_XYZ)
#define FVF_LVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1)
#define FVF_CVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1)
#define FVF_T3VERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX3)
#define FVF_TVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1|D3DFVF_TEXCOORDSIZE3(0))

const auto GS_MAX_SHADERS_NUMBER = 65535;


class GSDIRECT3D9_EXPORT GsDirect3D9: public GsRenderDevice
{
    public:
        GsDirect3D9(
            GsLog& newLog);
        ~GsDirect3D9();

        GsResult init(
            HWND window,
            GsDisplayParameters displayParameters);
		void release();

        bool isInitialized();
        bool isSceneRunning();

		bool isWindowed();

		GsResolution getResolution();

		UINT getActiveSkinIndex();
		void setActiveSkinIndex(
			UINT index);

		UINT getActiveVertexShaderIndex();
		UINT getActivePixelShaderIndex();

		GsResult createFont(
			const char* type,
			int weight, 
			bool italic, 
			bool underline, 
			bool striked, 
			DWORD size, 
			UINT* index);
		GsResult drawText(
			UINT fontIndex, 
			int x,
			int y,
			GsColor color,
			const char* text, ...);

			
        GsResult beginRendering(
            GsClearTarget target);
        void endRendering();

        GsResult clear(
            GsClearTarget target);
        void setClearColor(
            GsColor color);

		GsResult switchWindowMode();

		void fadeScreen(
			GsColor color);
		void setAmbientLightColor(
			const GsColor& color);
		void setWorldTransform(
			const GsMatrix* matrix);
		void setBackFaceCulling(
			GsRenderState state);
		void setDepthBufferMode(
			GsRenderState mode);
		void setStencilBufferMode(
			GsRenderState state,
			DWORD mode);
		void setUseStencilShadowSettings(
			bool use);

		GsColor getWireframeColor();

		void setShadeMode(
			GsRenderState mode,
			float pointSize,
			const GsColor* wireframeColor);

		GsRenderState getShadeMode();

		GsResult setTextureStage(
			UCHAR index, 
			GsRenderState state);
		GsResult setLight(
			const GsLight* light, 
			UCHAR stage);

		GsResult createVertexShader(
			const void* data, 
			UINT size, 
			bool loadFromFile, 
			bool isCompiled, 
			UINT* index);
		GsResult createPixelShader(
			const void* data, 
			UINT size, 
			bool loadFromFile, 
			bool isCompiled, 
			UINT* index);

		GsResult createVertexShader(
			const char* fileName, 
			UINT* index);
		GsResult createPixelShader(
			const char* fileName, 
			UINT* index);

		GsResult activateVertexShader(
			UINT index, 
			GsVertexType vertexType);
		GsResult activatePixelShader(
			UINT index);

		void setUseAdditiveBlending(
			bool use);
		bool isUsingAdditiveBlending();

		void setUseColorBuffer(
			bool use);
		bool isUsingColorBuffer();

		void setUseTextures(
			bool use);
		bool isUsingTextures();

		void setUseShaders(
			bool use);
		bool isUsingShaders();
		bool isThatCanUseShaders();
		bool isThatCanDoShaders();

		GsResult setShaderConstant(
			GsShaderType shaderType, 
			GsDataType dataType, 
			UINT reg, 
			UINT number, 
			const void* data);
		
		GsResult setView3D(
			GsVector& right,
			GsVector& up,
			GsVector& direction,
			GsVector& position);
		GsResult setViewLookAt(
			GsVector& from,
			GsVector& to,
			GsVector& worldUp);

		void setClippingPlanes(
			float nearDistance, 
			float farDistance);
		void setOrthoScale(
			float scale);

		GsResult setMode(
			GsEngineMode mode);
		GsResult initStage(
			float fov, 
			GsViewPort* viewPort);
		GsResult getFrustum(
			GsClippingPlanes& clippingPlanes);
		void transform2Dto3D(
			const GsPoint& point, 
			GsVector* position, 
			GsVector* direction);
		GsPoint transform3Dto2D(
			const GsVector& position);
		GsVector transform2Dto2D(
			float scale,
			const GsPoint* point, 
			GsAxis axis);

		D3DTEXTUREOP getTextureOperation(
			UCHAR index);

		GsResult registerConstantFloatForShader(
			GsShaderType shaderType, 
			const char* name, 
			float value, 
			UINT shaderIndex);

		virtual GsResult registerConstantFloat3ForShader(
			GsShaderType shaderType,
			const char* name,
			float value[3],
			UINT shaderIndex);

		GsResult registerConstantFloat4ForShader(
			GsShaderType shaderType,
			const char* name,
			float value[4],
			UINT shaderIndex);


    private:
        LPDIRECT3D9 direct3D;
        LPDIRECT3DDEVICE9 device;

		D3DDISPLAYMODE displayMode;

        std::vector<LPDIRECT3DSWAPCHAIN9>  swapChains;
        D3DPRESENT_PARAMETERS presentParameters;

        D3DCOLOR clearColor;

		D3DMATERIAL9 standardMaterial;
		UINT activeSkinIndex;
		UINT activeVertexShaderIndex;
		UINT activePixelShaderIndex;

		LPD3DXFONT* fonts;
		UINT fontsNumber;
		D3DTEXTUREOP textureOperations[GS_TEXTURES_PER_SKIN];

		D3DXMATRIXA16 matrixView2D;
		D3DXMATRIXA16 matrixView3D;
		D3DXMATRIXA16 matrixProjection2D;
		D3DXMATRIXA16 matrixWorld;
		D3DXMATRIXA16 matrixViewProjection;
		D3DXMATRIXA16 matrixWorldViewProjection;
		D3DXMATRIXA16 matrixWorldViewProjectionTransposed;
		D3DXMATRIXA16 matrixProjectionPerspective;
		D3DXMATRIXA16 matrixProjectionOrthogonal;

		LPDIRECT3DVERTEXDECLARATION9 declarationVertexPositionOnly;
		LPDIRECT3DVERTEXDECLARATION9 declarationVertexPositionNormalTexture;
		LPDIRECT3DVERTEXDECLARATION9 declarationVertexPositionColorTexture;
		LPDIRECT3DVERTEXDECLARATION9 declarationVertexPositionNormalTexture2Bones;
		LPDIRECT3DVERTEXDECLARATION9 declarationVertexPositionNormalTextureSomething;
		LPDIRECT3DVERTEXDECLARATION9 declarationVertexPositionNormal3Textures;

		LPDIRECT3DVERTEXSHADER9 vertexShaders[GS_MAX_SHADERS_NUMBER];
		LPDIRECT3DPIXELSHADER9 pixelShaders[GS_MAX_SHADERS_NUMBER];
		LPD3DXCONSTANTTABLE vertexShaderConstantTables[GS_MAX_SHADERS_NUMBER];
		LPD3DXCONSTANTTABLE pixelShaderConstantTables[GS_MAX_SHADERS_NUMBER];
		UINT vertexShadersNumber;
		UINT pixelShadersNumber;

		GsResult oneTimeInit();

		void prepare2D();
		void prepareShaders();
		void calculateViewProjectionMatrix();
		void calculateWorldViewProjectionMatrix();
		GsResult calculatePrespectiveProjectionMatrix(
			float fov, 
			float aspect, 
			D3DMATRIX& matrix);
		void calculateOrthogonalProjectionMatrix(
			float left, 
			float right, 
			float bottom, 
			float top, 
			float distanceNear, 
			float distanceFar);

		void logDeviceCapabilities(
			D3DCAPS9& capabilities);

		void setDisplayParameters(
			GsDisplayParameters displayParameters);
		
		GsResult reset();

		void resetPresentParameters();

		GsMatrix fromDirect3DMatrix(
			D3DXMATRIXA16& d3d);

		D3DXMATRIXA16 fromGsMatrix(
			GsMatrix gs);

		DWORD toDWORD(
			float real);
};