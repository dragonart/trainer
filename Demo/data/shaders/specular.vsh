float4x4 matrixWorldViewProjection;
float4x4 matrixWorld;

float4   vec_light;    //Позиция источника света
float3   vec_view_pos; //Видовой вектор
float4   vec_eye;      //Позиция наблюдателя

struct VS_INPUT //Входящие данные
{
    float4 position: POSITION;
    float3 normal:   NORMAL;
};

struct VS_OUTPUT //Исходящие данные
{
    float4 position:  POSITION;
    float3 light:     TEXCOORD0;
    float3 normal:    TEXCOORD1;
    float3 view:      TEXCOORD2;
};

VS_OUTPUT main(VS_INPUT original)
{
    VS_OUTPUT result;
    //Трансформируем позицию вершины
    result.position = mul(mat_mvp, original.position);    
    //Сохраняем позицию источника для передачи в пиксельный шейдер в виде
    //3D текстурных координат.
    result.light = vec_light; 
    //Рассчитываем нормаль поверхности и сохраняем для пиксельного шейдера.
    result.normal = normalize(mul(mat_world, original.normal));  
    //Вычисляем видовой вектор и сохраняем для пиксельного шейдера.
    result.view = vec_eye - vec_view_pos;

    return result;
}