#include "../GsEngine/gsengine.h"


int WINAPI WinMain(
	HINSTANCE instance,
	HINSTANCE previousInstance,
	LPSTR commandLine,
	int commandShow)
{
	GsDisplayParameters displayParameters;
	displayParameters.x = 0;
	displayParameters.y = 0;
	displayParameters.width = 700;
	displayParameters.height = 700;
	displayParameters.windowed = true;

	GsEngine engine(instance, previousInstance, commandLine, commandShow, displayParameters);

	engine.loadScript("main.script");

	return engine.main();
}