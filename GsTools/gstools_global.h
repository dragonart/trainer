#pragma once


#include <QtCore/qglobal.h>


#ifdef GSTOOLS_LIB
# define GSTOOLS_EXPORT Q_DECL_EXPORT
#else
# define GSTOOLS_EXPORT Q_DECL_IMPORT
#endif