#include "gslog.h"

#include <windows.h>


GsLog::GsLog()
{
	file.open("log.txt");
}


GsLog::~GsLog()
{
	file.close();
}


void GsLog::log(
	std::string line)
{
	SYSTEMTIME time;
	GetSystemTime(&time);
	
	file << "[" << time.wYear << ".";
	if (time.wMonth < 10)
		file << "0";
	file << time.wMonth << ".";
	if (time.wDay < 10)
		file << "0";
	file << time.wDay << " ";
	
	if (time.wHour < 10)
		file << "0";
	file << time.wHour << ":";
	if (time.wMinute < 10)
		file << "0";
	file << time.wMinute << ":";
	if (time.wSecond < 10)
		file << "0";
	file << time.wSecond << ":";
	if (time.wMilliseconds < 100)
		file << "0";
	if (time.wMilliseconds < 10)
		file << "0";
	file << time.wMilliseconds << "] ";
	
	file << line << "\n";
	file.flush();
}

void GsLog::operator<<(
	std::string line)
{
	log(line);
}

void GsLog::addSeparator()
{
	log("---------------------------------------------------");
}
