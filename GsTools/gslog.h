#pragma once


#include "gstools_global.h"

#include <fstream>
#include <string>


class GSTOOLS_EXPORT GsLog
{
    public:
        GsLog();
        ~GsLog();

        void log(
            std::string line);
        void operator<<(
            std::string line);
		void addSeparator();

    private:
        std::ofstream file;
};