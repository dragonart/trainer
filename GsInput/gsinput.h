#pragma once


#include "gsinput_global.h"


enum GsMouseButtonType
{
	GS_MOUSE_BUTTON__LEFT,
	GS_MOUSE_BUTTON__RIGHT,
	GS_MOUSE_BUTTON__MIDDLE
};


struct GsMousePositionDelta
{
	long deltaX;
	long deltaY;
};

struct GsMousePosition
{
	long x;
	long y;
};




class GSINPUT_EXPORT GsInput
{
public:
	GsInput();
	~GsInput();

private:

};