#pragma once


#include "..\gs.h"
#include "..\GsTools\gslog.h"
#include "gsinput.h"
#include "QtGui\qwindowdefs_win.h"


class GsInputDevice
{
public:
	GsInputDevice(
		GsLog& newLog) : log(newLog) {};
	~GsInputDevice() {};

	virtual GsResult init(
		const HINSTANCE instance, 
		const HWND window) = 0;
	virtual void release() = 0;

	virtual bool mouseMoved(
		GsMousePositionDelta* delta) = 0;
	virtual GsMousePosition getMousePosition() = 0;
	virtual bool mouseClicked(
		GsMouseButtonType button) = 0;
	virtual bool mouseUnClicked(
		GsMouseButtonType button) = 0;
	virtual bool mouseDown(
		GsMouseButtonType button) = 0;
	virtual bool mouseUp(
		GsMouseButtonType button) = 0;
	virtual bool mouseScrolled(
		int* delta) = 0;


protected:
	GsLog& log;
};