#ifndef GSINPUT_GLOBAL_H
#define GSINPUT_GLOBAL_H

#include <QtCore/qglobal.h>

#ifdef GSINPUT_LIB
# define GSINPUT_EXPORT Q_DECL_EXPORT
#else
# define GSINPUT_EXPORT Q_DECL_IMPORT
#endif

#endif // GSINPUT_GLOBAL_H
