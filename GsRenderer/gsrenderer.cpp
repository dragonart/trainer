#include "gsrenderer.h"

#include "../GsDirect3D9/gsdirect3d9.h"


GsRenderer::GsRenderer(
    GsLog& newLog) : log(newLog)
{
    device = nullptr;
}

GsRenderer::~GsRenderer()
{
    releaseDevice();
}

GsResult GsRenderer::createDevice(
    std::string apiName)
{
    if(apiName == "Direct3D 9")
    {
        device = new GsDirect3D9(log);

        log << "Graphics API '" + apiName + "' is chosen.";
    }
    else
    {
        std::string line = "API '" + apiName + "' is not supported.";

        log << "FAIL: " + line;
        log << apiName + " creation failed.";

        return GS_FAIL__API_NOT_SUPPORTED;
    }

    return GS_OK;
}

void GsRenderer::releaseDevice()
{
    log << "Graphics release started.";

    if(device)
    {
        device->release();
        device = nullptr;
    }

    log << "Graphics release succeeded.";
}

GsRenderDevice* GsRenderer::getDevice()
{
    return device;
}