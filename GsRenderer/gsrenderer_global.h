#pragma once


#include <QtCore/qglobal.h>


#ifdef GSRENDERER_LIB
# define GSRENDERER_EXPORT Q_DECL_EXPORT
#else
# define GSRENDERER_EXPORT Q_DECL_IMPORT
#endif