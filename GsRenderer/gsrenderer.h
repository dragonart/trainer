#pragma once


#include "gsrenderer_global.h"

#include "../GsGraphics/gsrenderdevice.h"


class GSRENDERER_EXPORT GsRenderer
{
    public:
        GsRenderer(
			GsLog& log);
        ~GsRenderer();

        GsResult createDevice(
            std::string apiName);
		void releaseDevice();

        GsRenderDevice* getDevice();


    private:
        GsRenderDevice* device;
		
		GsLog& log;
};