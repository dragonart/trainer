#ifndef GSCORE_GLOBAL_H
#define GSCORE_GLOBAL_H

#include <QtCore/qglobal.h>

#ifdef GSCORE_LIB
# define GSCORE_EXPORT Q_DECL_EXPORT
#else
# define GSCORE_EXPORT Q_DECL_IMPORT
#endif

#endif // GSCORE_GLOBAL_H
