#pragma once


#include <../GsTools/gslog.h>

#include <string>


class GsObject
{
public:
	const std::string name;

	GsObject(
		GsLog& log);
	virtual ~GsObject() = 0;

	virtual void do(
		std::string command) = 0;

protected:
	GsLog& log;
};